package n.webinfotech.itcreports.models;

/**
 * Created by Raj on 10-09-2019.
 */

public class BakingTimeRecord {

    public int id;
    public String date;
    public String time;
    public String shift;
    public String ovenNo;
    public String sku;
    public String btOnHmi;
    public String actualBt;
    public int userId;
    public boolean isChecked = false;
    public String username;
    public int verifyStatus;
    public  int verifyId;

    public BakingTimeRecord(int id, String date, String time, String shift, String ovenNo, String sku, String btOnHmi, String actualBt, int userId, String username, int verifyStatus, int verifyId) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.shift = shift;
        this.ovenNo = ovenNo;
        this.sku = sku;
        this.btOnHmi = btOnHmi;
        this.actualBt = actualBt;
        this.userId = userId;
        this.username = username;
        this.verifyStatus = verifyStatus;
        this.verifyId = verifyId;
    }
}
