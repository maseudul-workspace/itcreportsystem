package n.webinfotech.itcreports.models;

/**
 * Created by Raj on 16-09-2019.
 */

public class TankTemp {

    public int id;
    public String date;
    public String time;
    public String shift;
    public String tank;
    public String actTemp;
    public String setTemp;
    public String remarks;
    public int userId;
    public String username;
    public int verifyStatus;
    public  int verifyId;

    public TankTemp(int id, String date, String time, String shift, String tank, String actTemp, String setTemp, String remarks, int userId, String username, int verifyStatus, int verifyId) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.shift = shift;
        this.tank = tank;
        this.actTemp = actTemp;
        this.setTemp = setTemp;
        this.remarks = remarks;
        this.userId = userId;
        this.username = username;
        this.verifyStatus = verifyStatus;
        this.verifyId = verifyId;
    }
}
