package n.webinfotech.itcreports.models;

/**
 * Created by Raj on 16-09-2019.
 */

public class SieveMagnet {

    public int id;
    public String date;
    public String time;
    public String shift;
    public String sieveCondition;
    public String magnetCleaning;
    public String shifter;
    public String remarks;
    public int userId;
    public String username;
    public int verifyStatus;
    public  int verifyId;

    public SieveMagnet(int id, String date, String time, String shift, String sieveCondition, String magnetCleaning, String shifter, String remarks, int userId, String username, int verifyStatus, int verifyId) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.shift = shift;
        this.sieveCondition = sieveCondition;
        this.magnetCleaning = magnetCleaning;
        this.shifter = shifter;
        this.remarks = remarks;
        this.userId = userId;
        this.username = username;
        this.verifyStatus = verifyStatus;
        this.verifyId = verifyId;
    }

}
