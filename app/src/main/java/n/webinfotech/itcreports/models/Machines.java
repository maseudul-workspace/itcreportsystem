package n.webinfotech.itcreports.models;

/**
 * Created by Raj on 09-09-2019.
 */

public class Machines {

    public int id;
    public String machineName;
    public String machineNo;
    public int status;
    public String qrCode;
    public String formSheet;

    public Machines(int id, String machineName, String machineNo, int status, String qrCode, String formSheet) {
        this.id = id;
        this.machineName = machineName;
        this.machineNo = machineNo;
        this.status = status;
        this.qrCode = qrCode;
        this.formSheet = formSheet;
    }
}
