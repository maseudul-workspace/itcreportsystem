package n.webinfotech.itcreports.models;

/**
 * Created by Raj on 12-09-2019.
 */

public class MDRecord {

    public int id;
    public String date;
    public String time;
    public String shift;
    public String mdNo;
    public String probeFe;
    public String probeNfe;
    public String probeSS;
    public String interlock;
    public String sensitivity;
    public String remarks;
    public int userId;
    public String username;
    public int verifyStatus;
    public  int verifyId;

    public MDRecord(int id, String date, String time, String shift, String mdNo, String probeFe, String probeNfe, String probeSS, String interlock, String sensitivity, String remarks, int userId, String username, int verifyStatus, int verifyId) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.shift = shift;
        this.mdNo = mdNo;
        this.probeFe = probeFe;
        this.probeNfe = probeNfe;
        this.probeSS = probeSS;
        this.interlock = interlock;
        this.sensitivity = sensitivity;
        this.userId = userId;
        this.username = username;
        this.verifyStatus = verifyStatus;
        this.verifyId = verifyId;
        this.remarks = remarks;
    }
}
