package n.webinfotech.itcreports.models;

public class Forms {
    public int id;
    public boolean verified;
    public int unverifyCount;

    public Forms(int id, boolean verified, int unverifyCount) {
        this.id = id;
        this.verified = verified;
        this.unverifyCount = unverifyCount;
    }
}
