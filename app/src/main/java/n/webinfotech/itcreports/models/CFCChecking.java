package n.webinfotech.itcreports.models;

/**
 * Created by Raj on 14-09-2019.
 */

public class CFCChecking {

    public int id;
    public String date;
    public String time;
    public String shift;
    public String machineNo;
    public String sku;
    public String torn;
    public String flap;
    public String dent;
    public String codingMono;
    public String qtyPresentMono;
    public String qtyPresentCFC;
    public String tapping;
    public String codingCFC;
    public String remarks;
    public int userId;
    public String username;
    public int verifyStatus;
    public  int verifyId;


    public CFCChecking(int id, String date, String time, String shift, String machineNo, String sku, String torn, String flap, String dent, String codingMono, String qtyPresentMono, String qtyPresentCFC, String tapping, String codingCFC, String remarks, int userId, String username, int verifyStatus, int verifyId) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.shift = shift;
        this.machineNo = machineNo;
        this.sku = sku;
        this.torn = torn;
        this.flap = flap;
        this.dent = dent;
        this.codingMono = codingMono;
        this.qtyPresentMono = qtyPresentMono;
        this.qtyPresentCFC = qtyPresentCFC;
        this.tapping = tapping;
        this.codingCFC = codingCFC;
        this.userId = userId;
        this.username = username;
        this.verifyStatus = verifyStatus;
        this.verifyId = verifyId;
        this.remarks = remarks;
    }
}
