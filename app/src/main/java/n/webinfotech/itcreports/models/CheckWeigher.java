package n.webinfotech.itcreports.models;

/**
 * Created by Raj on 16-09-2019.
 */

public class CheckWeigher {

    public int id;
    public String date;
    public String time;
    public String shift;
    public String sku;
    public String rejPkt;
    public String lessCount;
    public String flapOpen;
    public String misCoding;
    public int userId;
    public String username;
    public int verifyStatus;
    public  int verifyId;

    public CheckWeigher(int id, String date, String time, String shift, String sku, String rejPkt, String lessCount, String flapOpen, String misCoding, int userId, String username, int verifyStatus, int verifyId) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.shift = shift;
        this.sku = sku;
        this.rejPkt = rejPkt;
        this.flapOpen = flapOpen;
        this.misCoding = misCoding;
        this.userId = userId;
        this.username = username;
        this.verifyStatus = verifyStatus;
        this.verifyId = verifyId;
        this.lessCount = lessCount;
    }
}
