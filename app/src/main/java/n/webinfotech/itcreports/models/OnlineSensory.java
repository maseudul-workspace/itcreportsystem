package n.webinfotech.itcreports.models;

/**
 * Created by Raj on 17-09-2019.
 */

public class OnlineSensory {

    public int id;
    public String date;
    public String time;
    public String shift;
    public String module;
    public String sku;
    public String signP1;
    public String nameP1;
    public String signP2;
    public String nameP2;
    public String signP3;
    public String nameP3;
    public OnlineSensoryBatchesData pannelist1Data;
    public OnlineSensoryBatchesData pannelist2Data;
    public OnlineSensoryBatchesData pannelist3Data;
    public int verifyStatus;
    public int verifyId;

    public OnlineSensory(int id, String date, String time, String shift, String module, String sku, String signP1, String nameP1, String signP2, String nameP2, String signP3, String nameP3, OnlineSensoryBatchesData pannelist1Data, OnlineSensoryBatchesData pannelist2Data, OnlineSensoryBatchesData pannelist3Data, int verifyStatus, int verifyId) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.shift = shift;
        this.module = module;
        this.sku = sku;
        this.signP1 = signP1;
        this.nameP1 = nameP1;
        this.signP2 = signP2;
        this.nameP2 = nameP2;
        this.signP3 = signP3;
        this.nameP3 = nameP3;
        this.pannelist1Data = pannelist1Data;
        this.pannelist2Data = pannelist2Data;
        this.pannelist3Data = pannelist3Data;
        this.verifyStatus = verifyStatus;
        this.verifyId = verifyId;
    }
}
