package n.webinfotech.itcreports.models;

public class Module {
    public String moduleName;
    public String lastCheckedDate;
    public String lastCheckedTime;
    public boolean status;

    public Module(String moduleName, String lastCheckedDate, String lastCheckedTime, boolean status) {
        this.moduleName = moduleName;
        this.lastCheckedDate = lastCheckedDate;
        this.lastCheckedTime = lastCheckedTime;
        this.status = status;
    }
}
