package n.webinfotech.itcreports.models;

/**
 * Created by Raj on 07-09-2019.
 */

public class User {
    public int id;
    public int type;
    public String name;
    public String signature;

    public User(int id, int type, String name, String signature) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.signature = signature;
    }
}
