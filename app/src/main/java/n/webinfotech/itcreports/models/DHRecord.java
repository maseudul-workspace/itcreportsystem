package n.webinfotech.itcreports.models;

/**
 * Created by Raj on 16-09-2019.
 */

public class DHRecord {

    public int id;
    public String date;
    public String time;
    public String shift;
    public String tank;
    public String actTemp;
    public String rh;
    public String remarks;
    public int userId;
    public String username;
    public int verifyStatus;
    public  int verifyId;

    public DHRecord(int id, String date, String time, String shift, String tank, String actTemp, String rh, String remarks, int userId, String username, int verifyStatus, int verifyId) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.shift = shift;
        this.tank = tank;
        this.actTemp = actTemp;
        this.rh = rh;
        this.remarks = remarks;
        this.userId = userId;
        this.username = username;
        this.verifyStatus = verifyStatus;
        this.verifyId = verifyId;
    }
}
