package n.webinfotech.itcreports.models;

/**
 * Created by Raj on 12-09-2019.
 */

public class HourlyPQISheet {

    public int id;
    public String date;
    public String time;
    public String shift;
    public String pktTime;
    public String machineNo;
    public String sku;
    public String coding;
    public String loosePckt;
    public String wrinkles;
    public String stack;
    public String diameter1;
    public String diameter2;
    public String sealingQuality;
    public String broken;
    public String color;
    public String aroma;
    public String foreignMatter;
    public String otherDefects;
    public String remarks;
    public int userId;
    public String username;
    public int verifyStatus;
    public  int verifyId;

    public HourlyPQISheet(int id, String date, String time, String shift, String pktTime, String machineNo, String sku, String coding, String loosePckt, String wrinkles, String stack, String diameter1, String diameter2, String sealingQuality, String broken, String color, String aroma, String foreignMatter, String otherDefects, String remarks, int userId, String username, int verifyStatus, int verifyId) {
        this.id = id;
        this.date = date;
        this.time = time;
        this.shift = shift;
        this.pktTime = pktTime;
        this.machineNo = machineNo;
        this.sku = sku;
        this.coding = coding;
        this.loosePckt = loosePckt;
        this.wrinkles = wrinkles;
        this.stack = stack;
        this.diameter1 = diameter1;
        this.diameter2 = diameter2;
        this.sealingQuality = sealingQuality;
        this.broken = broken;
        this.color = color;
        this.aroma = aroma;
        this.foreignMatter = foreignMatter;
        this.otherDefects = otherDefects;
        this.remarks = remarks;
        this.userId = userId;
        this.username = username;
        this.verifyStatus = verifyStatus;
        this.verifyId = verifyId;
    }

}
