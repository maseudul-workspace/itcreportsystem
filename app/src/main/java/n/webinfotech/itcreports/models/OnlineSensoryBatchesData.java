package n.webinfotech.itcreports.models;

import java.util.ArrayList;

public class OnlineSensoryBatchesData {

    public ArrayList<Integer> ids;

    public ArrayList<Integer> batchNos;

    public ArrayList<Integer> appearance;

    public ArrayList<Integer> taste;

    public ArrayList<Integer> aroma;

    public OnlineSensoryBatchesData(ArrayList<Integer> batchNos, ArrayList<Integer> appearance, ArrayList<Integer> taste, ArrayList<Integer> aroma, ArrayList<Integer> ids) {
        this.batchNos = batchNos;
        this.appearance = appearance;
        this.taste = taste;
        this.aroma = aroma;
        this.ids = ids;
    }
}
