package n.webinfotech.itcreports.models;

/**
 * Created by Raj on 07-09-2019.
 */

public class UserList {
    public int id;
    public String userName;
    public String name;
    public String phone;
    public String email;
    public String gender;
    public int status;
    public String password;
    public int userType;

    public UserList(int id, String userName, String name, String phone, String email, String gender, int status, String password, int type) {
        this.userName = userName;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.gender = gender;
        this.status = status;
        this.password = password;
        this.id = id;
        this.userType = type;
    }

}
