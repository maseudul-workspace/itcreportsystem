package n.webinfotech.itcreports.models;

import android.database.Cursor;

public class LastBatch {
    public String dateTime;
    public Cursor c;

    public LastBatch(String dateTime, Cursor c) {
        this.dateTime = dateTime;
        this.c = c;
    }
}
