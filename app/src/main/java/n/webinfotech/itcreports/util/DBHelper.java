package n.webinfotech.itcreports.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import n.webinfotech.itcreports.models.Forms;
import n.webinfotech.itcreports.models.LastBatch;
import n.webinfotech.itcreports.models.Module;
import n.webinfotech.itcreports.models.OnlineSensoryBatchesData;

import static n.webinfotech.itcreports.util.Helper.checkTimeRangPlusHour;
import static n.webinfotech.itcreports.util.Helper.checkTimeRange;
import static n.webinfotech.itcreports.util.Helper.checkTimeRangeShift;
import static n.webinfotech.itcreports.util.Helper.getDate;
import static n.webinfotech.itcreports.util.Helper.getPreviousDate;
import static n.webinfotech.itcreports.util.Helper.getTime;

/**
 * Created by Raj on 07-09-2019.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static String DB_NAME = "itc.db";
    private SQLiteDatabase db;
    private final Context context;
    private String DB_PATH;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.context = context;
        DB_PATH = "/data/data/" + context.getPackageName() + "/" + "databases/";
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();
//        copyDataBase();
        if (dbExist) {

        } else {
            this.getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException e) {
                Log.e("LogMsg", e.getMessage());
//                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkDataBase() {
        File dbFile = new File(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

    private void copyDataBase() throws IOException {

        InputStream myInput = context.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        // Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public Cursor checkLogin(String username, String password) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select * from users where user_name='"+username+"' AND password='"+password+"'", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public boolean createUser(String username, String name, String password, String email, String phone, String gender, int userType) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery( "select * from users where user_name = '" + username + "'", null );
        if (c.getCount() > 0) {
            return false;
        } else {
            ContentValues contentValues = new ContentValues();
            contentValues.put("user_name", username);
            contentValues.put("password", password);
            contentValues.put("name", name);
            contentValues.put("gender", gender);
            contentValues.put("mobile", phone);
            contentValues.put("email", email);
            contentValues.put("user_type", userType);
            contentValues.put("status", 1);
            db.insert("users", null, contentValues);
            return true;
        }
    }

    public Cursor getUsers() {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select * from users where user_type != 1", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public boolean updateUser(String name, String password, String email, String phone, String gender, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("password", password);
        contentValues.put("name", name);
        contentValues.put("gender", gender);
        contentValues.put("mobile", phone);
        contentValues.put("email", email);
        db.update("users", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public boolean changeUserActivation(int status, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("status", status);
        db.update("users", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Cursor getFormNames() {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select * from form_type", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public boolean addMachine(String machineName, String machineNo, String qrCode, int formId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("machine_name", machineName);
        contentValues.put("machine_number", machineNo);
        contentValues.put("qr_text", qrCode);
        contentValues.put("form_type_id", formId);
        contentValues.put("status", 1);
        db.insert("qr_codes", null, contentValues);
        return true;
    }

    public Cursor getMachines() {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select qr_codes.*, form_type.form_name as f_name  from qr_codes INNER JOIN form_type on form_type.id = qr_codes.form_type_id", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        Log.e("LogMsg", "Machine Count: " + c.getCount());
        return c;
    }

    public boolean updateMachine(String machineName, String machineNo, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("machine_name", machineName);
        contentValues.put("machine_number", machineNo);
        db.update("qr_codes", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public boolean changeMachineActivation(int status, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("status", status);
        db.update("qr_codes", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Cursor checkCode(String qrCode) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select * from qr_codes where qr_text='"+qrCode+"'", null);
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public boolean addBakingTimeRecord(String shift, String machineName, String sku, String btOnHMI, String actualBT, int userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String date = "";
        if (checkTimeRange()) {
            date = getPreviousDate();
        } else {
            date = getDate();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);
        contentValues.put("time", getTime());
        contentValues.put("shift", shift);
        contentValues.put("oven_no", machineName);
        contentValues.put("sku", sku);
        contentValues.put("bt_on_hmi", btOnHMI);
        contentValues.put("actual_bt", actualBT);
        contentValues.put("user_id", userId);
        contentValues.put("date_time", date + " " + getTime());
        db.insert("backing_time_monitoring", null, contentValues);
        return true;
    }

    public boolean updateBakingTimeRecord(String shift, String sku, String btOnHMI, String actualBT, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String date = "";
        if (checkTimeRange()) {
            date = getPreviousDate();
        } else {
            date = getDate();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("shift", shift);
        contentValues.put("sku", sku);
        contentValues.put("bt_on_hmi", btOnHMI);
        contentValues.put("actual_bt", actualBT);
        long i = db.update("backing_time_monitoring", contentValues, "id = ? ", new String[] { Integer.toString(id) });
        if (i < 0) {
           return false;
        } else {
            return true;
        }
    }

    public Cursor getBakingTimeRecordsUnverified() {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select backing_time_monitoring.*, users.name as username  from backing_time_monitoring INNER JOIN users on backing_time_monitoring.user_id = users.id WHERE backing_time_monitoring.verify_status = 1", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public long verifyBakingTimeRecords(String name, String signature) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", getDate());
        contentValues.put("name", name);
        contentValues.put("sign", signature);
        long id = db.insert("backing_time_monitoring_sign", null, contentValues);
        return id;
    }

    public boolean updateBakingTimeRecordsVerification(int id, long verifyId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("verify_status", 2);
        contentValues.put("verify_id", verifyId);
        db.update("backing_time_monitoring", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Cursor getSingleBakingTimeRecord(int id) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select * from backing_time_monitoring WHERE id = " + id, null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public boolean deleteBakingTimeRecordData(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int i = db.delete("backing_time_monitoring", "id = ? ", new String[] { Integer.toString(id) });
        if (i < 0) {
            return false;
        } else {
            return true;
        }
    }

    public boolean addHourlyPQISheet(String shift,String pckgTime, String machine, String sku, String coding, String loosePacket, String wrinkles, String stack, String diameter1, String diameter2, String sealingQuality, String broken, String color, String aroma, String foreignMatter, String otherDefects, String remarks, int userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String date = "";
        if (checkTimeRange()) {
            date = getPreviousDate();
        } else {
            date = getDate();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);
        contentValues.put("time", getTime());
        contentValues.put("shift", shift);
        contentValues.put("pkd_time", pckgTime);
        contentValues.put("machine_no", machine);
        contentValues.put("sku", sku);
        contentValues.put("coding", coding);
        contentValues.put("loose_pkd", loosePacket);
        contentValues.put("wrinkles", wrinkles);
        contentValues.put("stack", stack);
        contentValues.put("diameter1", diameter1);
        contentValues.put("diameter2", diameter2);
        contentValues.put("sealing_quality", sealingQuality);
        contentValues.put("broken_chipped", broken);
        contentValues.put("color", color);
        contentValues.put("aroma", aroma);
        contentValues.put("foreign_matter", foreignMatter);
        contentValues.put("other_defects", otherDefects);
        contentValues.put("remarks", remarks);
        contentValues.put("user_id", userId);
        contentValues.put("date_time", date + " " + getTime());
        db.insert("hourly_pqi_sheet", null, contentValues);
        return true;
    }

    public boolean updateHourlyPQISheet(String shift, String pckgTime, String sku, String coding, String loosePacket, String wrinkles, String stack, String diameter1, String diameter2, String sealingQuality, String broken, String color, String aroma, String foreignMatter, String otherDefects, String remarks, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("shift", shift);
        contentValues.put("pkd_time", pckgTime);
        contentValues.put("sku", sku);
        contentValues.put("coding", coding);
        contentValues.put("loose_pkd", loosePacket);
        contentValues.put("wrinkles", wrinkles);
        contentValues.put("stack", stack);
        contentValues.put("diameter1", diameter1);
        contentValues.put("diameter2", diameter2);
        contentValues.put("sealing_quality", sealingQuality);
        contentValues.put("broken_chipped", broken);
        contentValues.put("color", color);
        contentValues.put("aroma", aroma);
        contentValues.put("foreign_matter", foreignMatter);
        contentValues.put("other_defects", otherDefects);
        contentValues.put("remarks", remarks);
        long i = db.update("hourly_pqi_sheet", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        if (i < 0) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getHourlyPQISheetUnverified() {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select hourly_pqi_sheet.*, users.name as username  from hourly_pqi_sheet INNER JOIN users on hourly_pqi_sheet.user_id = users.id WHERE hourly_pqi_sheet.verify_status = 1", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public boolean updateHourlyPQISheetVerification(int id, long verifyId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("verify_status", 2);
        contentValues.put("verify_id", verifyId);
        db.update("hourly_pqi_sheet", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public long verifyHourlyPQISheetRecords(String name, String signature) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", getDate());
        contentValues.put("name", name);
        contentValues.put("sign", signature);
        long id = db.insert("hourly_pqi_sheet_sign", null, contentValues);
        return id;
    }

    public boolean addMDRecord(String shift, String machineName, String probeFe, String provbeNfe, String probeSS, String interLock, String sensitivity, String remarks, int userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String date = "";
        if (checkTimeRange()) {
            date = getPreviousDate();
        } else {
            date = getDate();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);
        contentValues.put("time", getTime());
        contentValues.put("shift", shift);
        contentValues.put("md_no", machineName);
        contentValues.put("prove_fe", probeFe);
        contentValues.put("prove_nfe", provbeNfe);
        contentValues.put("prove_ss", probeSS);
        contentValues.put("inter_lock", interLock);
        contentValues.put("sensitivity", sensitivity);
        contentValues.put("remarks", remarks);
        contentValues.put("user_id", userId);
        contentValues.put("date_time", date + " " + getTime());
        db.insert("ccp_md", null, contentValues);
        return true;
    }

    public boolean updateMDRecord(String shift, String probeFe, String provbeNfe, String probeSS, String interLock, String sensitivity, String remarks, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("shift", shift);
        contentValues.put("prove_fe", probeFe);
        contentValues.put("prove_nfe", provbeNfe);
        contentValues.put("prove_ss", probeSS);
        contentValues.put("inter_lock", interLock);
        contentValues.put("sensitivity", sensitivity);
        contentValues.put("remarks", remarks);
        long i = db.update("ccp_md", contentValues,  "id = ? ", new String[] { Integer.toString(id) });
        if (i < 0) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getMDRecordsUnverified() {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select ccp_md.*, users.name as username  from ccp_md INNER JOIN users on ccp_md.user_id = users.id WHERE ccp_md.verify_status = 1", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public boolean updateMDRecordsVerification(int id, long verifyId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("verify_status", 2);
        contentValues.put("verify_id", verifyId);
        db.update("ccp_md", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public boolean deleteMDRecord(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int i = db.delete("ccp_md", "id = ? ", new String[] { Integer.toString(id) });
        if (i < 0) {
            return false;
        } else {
            return true;
        }
    }

    public long verifyMDRecords(String name, String signature) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", getDate());
        contentValues.put("name", name);
        contentValues.put("sign", signature);
        long id = db.insert("ccp_md_sign", null, contentValues);
        return id;
    }

    public Cursor getBakingTimeData(String fromDate, String toDate) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select backing_time_monitoring.*,users.name as u_name from backing_time_monitoring INNER JOIN users ON users.id = backing_time_monitoring.user_id WHERE backing_time_monitoring.date BETWEEN '" + fromDate + "' AND '" + toDate + "'", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public Cursor getMDRecordsData(String fromDate, String toDate) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select ccp_md.*,users.name as u_name from ccp_md INNER JOIN users ON users.id = ccp_md.user_id WHERE ccp_md.date BETWEEN '" + fromDate + "' AND '" + toDate + "'", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public Cursor getSingleMDRecordsData(int id) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select * FROM ccp_md WHERE id = " + id, null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public Cursor getHourlyPQISheetData(String fromDate, String toDate) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select hourly_pqi_sheet.*,users.name as u_name from hourly_pqi_sheet INNER JOIN users ON users.id = hourly_pqi_sheet.user_id WHERE hourly_pqi_sheet.date BETWEEN '" + fromDate + "' AND '" + toDate + "'", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public Cursor getSingleHourlyPQISheetData(int id) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select * from hourly_pqi_sheet WHERE id = " + id, null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public boolean deleteSingleHourlyPQISheetData(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int i = db.delete("hourly_pqi_sheet", "id = ? ", new String[] { Integer.toString(id) });
        if (i < 0) {
            return false;
        } else {
            return true;
        }
    }

    public String getSignature(String tableName, int id) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select sign from " + tableName + " where id = " + id, null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        Log.e("LogMsg", "Count " + c.getCount());
        c.moveToFirst();
        return c.getString(c.getColumnIndex("sign"));
    }

    public String getSignatureName(String tableName, int id) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select name from " + tableName + " where id = " + id, null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        Log.e("LogMsg", "Count " + c.getCount());
        c.moveToFirst();
        return c.getString(c.getColumnIndex("name"));
    }

    public boolean addCFCChecking(String shift, String machineName, String sku, String torn, String flap, String dent, String codingMono, String qtyMono, String qtyCfc, String tappingQuality, String codingCfc, String remarks, int userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String date = "";
        if (checkTimeRange()) {
            date = getPreviousDate();
        } else {
            date = getDate();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);
        contentValues.put("time", getTime());
        contentValues.put("shift", shift);
        contentValues.put("machine_no", machineName);
        contentValues.put("sku", sku);
        contentValues.put("rhno_of_torn", torn);
        contentValues.put("no_of_flap_open", flap);
        contentValues.put("no_of_dent", dent);
        contentValues.put("coading_mono", codingMono);
        contentValues.put("qtty_present_mono", qtyMono);
        contentValues.put("qtty_present_cfc", qtyCfc);
        contentValues.put("tapping_quality", tappingQuality);
        contentValues.put("coading_cfc", codingCfc);
        contentValues.put("remarks", remarks);
        contentValues.put("user_id", userId);
        contentValues.put("date_time", date + " " + getTime());
        long i = db.insert("cfc_checking", null, contentValues);
        Log.e("LogMsg", "Status: " + i);
        return true;
    }

    public boolean deleteCFCData(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int i = db.delete("cfc_checking", "id = ? ", new String[] { Integer.toString(id) });
        if (i < 0) {
            return false;
        } else {
            return true;
        }
    }

    public boolean updateCFCChecking(String shift, String sku, String torn, String flap, String dent, String codingMono, String qtyMono, String qtyCfc, String tappingQuality, String codingCfc, String remarks, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("shift", shift);
        contentValues.put("sku", sku);
        contentValues.put("rhno_of_torn", torn);
        contentValues.put("no_of_flap_open", flap);
        contentValues.put("no_of_dent", dent);
        contentValues.put("coading_mono", codingMono);
        contentValues.put("qtty_present_mono", qtyMono);
        contentValues.put("qtty_present_cfc", qtyCfc);
        contentValues.put("tapping_quality", tappingQuality);
        contentValues.put("coading_cfc", codingCfc);
        contentValues.put("remarks", remarks);
        long i = db.update("cfc_checking", contentValues, "id = ? ", new String[] { Integer.toString(id) });
        if (i < 0) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getCFCCheckingUnverified() {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select cfc_checking.*, users.name as username  from cfc_checking INNER JOIN users on cfc_checking.user_id = users.id WHERE cfc_checking.verify_status = 1", null );
        Log.e("LogMsg", "Data Count " + c.getCount());
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public long verifyCFCChecking(String name, String signature) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", getDate());
        contentValues.put("name", name);
        contentValues.put("sign", signature);
        long id = db.insert("cfc_checking_sign", null, contentValues);
        return id;
    }

    public boolean updateCFCCheckingVerification(int id, long verifyId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("verify_status", 2);
        contentValues.put("verify_id", verifyId);
        db.update("cfc_checking", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Cursor getCFCSheetData(String fromDate, String toDate) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select cfc_checking.*,users.name as u_name from cfc_checking INNER JOIN users ON users.id = cfc_checking.user_id WHERE cfc_checking.date BETWEEN '" + fromDate + "' AND '" + toDate + "'", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public Cursor getSingleCFCdata(int id) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select * from cfc_checking WHERE id = " + id, null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public boolean addDHRoomRecord(String shift, String machineName, String actTemp, String rh, String remarks, int userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String date = "";
        if (checkTimeRange()) {
            date = getPreviousDate();
        } else {
            date = getDate();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);
        contentValues.put("time", getTime());
        contentValues.put("shift", shift);
        contentValues.put("tank", machineName);
        contentValues.put("act_temp", actTemp);
        contentValues.put("rh", rh);
        contentValues.put("remarks", remarks);
        contentValues.put("user_id", userId);
        contentValues.put("date_time", date + " " + getTime());
        long i = db.insert("dh_room", null, contentValues);
        if (i == -1) {
            return false;
        } else {
            return true;
        }

    }

    public boolean updateDHRoomRecord(String shift, String actTemp, String rh, String remarks, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String date = "";
        ContentValues contentValues = new ContentValues();
        contentValues.put("shift", shift);
        contentValues.put("act_temp", actTemp);
        contentValues.put("rh", rh);
        contentValues.put("remarks", remarks);
        long i = db.update("dh_room",  contentValues, "id = ? ", new String[] { Integer.toString(id) });
        if (i == -1) {
            return false;
        } else {
            return true;
        }

    }

    public boolean deleteDHRoomData(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int i = db.delete("dh_room", "id = ? ", new String[] { Integer.toString(id) });
        if (i < 0) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getDHRoomRecordUnverified() {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select dh_room.*, users.name as username  from dh_room INNER JOIN users on dh_room.user_id = users.id WHERE dh_room.verify_status = 1", null );
        Log.e("LogMsg", "Data Count " + c.getCount());
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public long verifyDHRoomRecord(String name, String signature) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", getDate());
        contentValues.put("name", name);
        contentValues.put("sign", signature);
        long id = db.insert("dh_room_sign", null, contentValues);
        return id;
    }

    public boolean updateDHRoomVerification(int id, long verifyId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("verify_status", 2);
        contentValues.put("verify_id", verifyId);
        db.update("dh_room", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Cursor getDHRoomRecordData(String fromDate, String toDate) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select dh_room.*,users.name as u_name from dh_room INNER JOIN users ON users.id = dh_room.user_id WHERE dh_room.date BETWEEN '" + fromDate + "' AND '" + toDate + "'", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public Cursor getSingleDHRoomRecordData(int id) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select * from dh_room WHERE id = " + id, null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public boolean addSieveMagnetRecord(String shift, String shifter, String sieveCondition, String magnetCleaningStatus, String remarks, int userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String date = "";
        if (checkTimeRange()) {
            date = getPreviousDate();
        } else {
            date = getDate();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);
        contentValues.put("time", getTime());
        contentValues.put("shift", shift);
        contentValues.put("shifter", shifter);
        contentValues.put("sleve_condition", sieveCondition);
        contentValues.put("magnet_cleaning_status", magnetCleaningStatus);
        contentValues.put("remarks", remarks);
        contentValues.put("user_id", userId);
        contentValues.put("date_time", date + " " + getTime());
        long i = db.insert("sleve_magnet_check_list", null, contentValues);
        if (i == -1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean updateSieveMagnetRecord(String shift, String sieveCondition, String magnetCleaningStatus, String remarks, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("shift", shift);
        contentValues.put("sleve_condition", sieveCondition);
        contentValues.put("magnet_cleaning_status", magnetCleaningStatus);
        contentValues.put("remarks", remarks);
        long i = db.update("sleve_magnet_check_list", contentValues, "id = ? ", new String[] { Integer.toString(id)});
        if (i == -1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean deleteSieveMagnetData(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int i = db.delete("sleve_magnet_check_list", "id = ? ", new String[] { Integer.toString(id) });
        if (i < 0) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getSieveMagnetRecordUnverified() {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select sleve_magnet_check_list.*, users.name as username  from sleve_magnet_check_list INNER JOIN users on sleve_magnet_check_list.user_id = users.id WHERE sleve_magnet_check_list.verify_status = 1", null );
        Log.e("LogMsg", "Data Count " + c.getCount());
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public long verifySieveMagnetRecord(String name, String signature) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", getDate());
        contentValues.put("name", name);
        contentValues.put("sign", signature);
        long id = db.insert("sleve_magnet_check_list_sign", null, contentValues);
        return id;
    }

    public boolean updateSieveMagnetVerification(int id, long verifyId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("verify_status", 2);
        contentValues.put("verify_id", verifyId);
        db.update("sleve_magnet_check_list", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Cursor getSieveMagnetData(String fromDate, String toDate) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select sleve_magnet_check_list.*,users.name as u_name from sleve_magnet_check_list INNER JOIN users ON users.id = sleve_magnet_check_list.user_id WHERE sleve_magnet_check_list.date BETWEEN '" + fromDate + "' AND '" + toDate + "'", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public Cursor getSingleSieveMagnetData(int id) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select * FROM sleve_magnet_check_list WHERE id = " + id, null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public boolean addTankTemperatureRecord(String shift, String machineName, String actTemp, String setTemp, String remarks, int userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String date = "";
        if (checkTimeRange()) {
            date = getPreviousDate();
        } else {
            date = getDate();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);
        contentValues.put("time", getTime());
        contentValues.put("shift", shift);
        contentValues.put("tank", machineName);
        contentValues.put("act_temp", actTemp);
        contentValues.put("set_temp", setTemp);
        contentValues.put("remarks", remarks);
        contentValues.put("user_id", userId);
        contentValues.put("date_time", date + " " + getTime());
        long i = db.insert("tank_temprature", null, contentValues);
        if (i == -1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean updateTankTemperatureRecord(String shift, String actTemp, String setTemp, String remarks, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("shift", shift);
        contentValues.put("act_temp", actTemp);
        contentValues.put("set_temp", setTemp);
        contentValues.put("remarks", remarks);
        long i = db.update("tank_temprature", contentValues, "id = ? ", new String[] { Integer.toString(id) });
        if (i == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getTankTempRecordUnverified() {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select tank_temprature.*, users.name as username  from tank_temprature INNER JOIN users on tank_temprature.user_id = users.id WHERE tank_temprature.verify_status = 1", null );
        Log.e("LogMsg", "Data Count " + c.getCount());
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public long verifyTankTempRecord(String name, String signature) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", getDate());
        contentValues.put("name", name);
        contentValues.put("sign", signature);
        long id = db.insert("tank_temprature_sign", null, contentValues);
        return id;
    }

    public boolean updateTankTempVerification(int id, long verifyId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("verify_status", 2);
        contentValues.put("verify_id", verifyId);
        db.update("tank_temprature", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public boolean deleteTankTempVerification(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int i = db.delete("tank_temprature","id = ? ", new String[] { Integer.toString(id) } );
        if (i < 0) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getTankTempData(String fromDate, String toDate) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select tank_temprature.*,users.name as u_name from tank_temprature INNER JOIN users ON users.id = tank_temprature.user_id WHERE tank_temprature.date BETWEEN '" + fromDate + "' AND '" + toDate + "'", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public Cursor getSingleTankTempData(int id) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select * from tank_temprature WHERE id = " + id, null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public boolean addCheckWeigherBinRecord(String shift, String machineName, String lessCountPacket, String sku, String flapOpen, String misCoding, int userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String date = "";
        if (checkTimeRange()) {
            date = getPreviousDate();
        } else {
            date = getDate();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);
        contentValues.put("time", getTime());
        contentValues.put("shift", shift);
        contentValues.put("rej_pkd", machineName);
        contentValues.put("less_count_pkt", lessCountPacket);
        contentValues.put("sku", sku);
        contentValues.put("flap_open", flapOpen);
        contentValues.put("mis_coading", misCoding);
        contentValues.put("user_id", userId);
        contentValues.put("date_time", date + " " + getTime());
        long i = db.insert("check_weigher_bin_record", null, contentValues);
        if (i == -1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean updateCheckWeigherBinRecord(String shift, String lessCountPacket, String sku, String flapOpen, String misCoding, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String date = "";
        if (checkTimeRange()) {
            date = getPreviousDate();
        } else {
            date = getDate();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("shift", shift);
        contentValues.put("less_count_pkt", lessCountPacket);
        contentValues.put("sku", sku);
        contentValues.put("flap_open", flapOpen);
        contentValues.put("mis_coading", misCoding);
        long i = db.update("check_weigher_bin_record", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        if (i == -1) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getCheckWeigherRecordUnverified() {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select check_weigher_bin_record.*, users.name as username  from check_weigher_bin_record INNER JOIN users on check_weigher_bin_record.user_id = users.id WHERE check_weigher_bin_record.verify_status = 1", null );
        Log.e("LogMsg", "Data Count " + c.getCount());
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public Cursor getSingleCheckWeigherRecord(int id) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select *  from check_weigher_bin_record WHERE id = " + id, null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public long verifyCheckWeigherRecord(String name, String signature) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", getDate());
        contentValues.put("name", name);
        contentValues.put("sign", signature);
        long id = db.insert("check_weigher_bin_record_sign", null, contentValues);
        return id;
    }

    public boolean updateCheckWeigherVerification(int id, long verifyId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("verify_status", 2);
        contentValues.put("verify_id", verifyId);
        db.update("check_weigher_bin_record", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public boolean deleteCheckWeigherBinRecord(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int i = db.delete("check_weigher_bin_record", "id = ? ", new String[] { Integer.toString(id) } );
        if (i < 0) {
            return false;
        } else {
            return true;
        }
    }

    public Cursor getCheckWeigherData(String fromDate, String toDate) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select check_weigher_bin_record.*,users.name as u_name from check_weigher_bin_record INNER JOIN users ON users.id = check_weigher_bin_record.user_id WHERE check_weigher_bin_record.date BETWEEN '" + fromDate + "' AND '" + toDate + "'", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public long addOnlineSensoryEvaluationRecord(String shift, String machineNo, String sku, String signP1, String nameP1, String signP2, String nameP2, String signP3, String nameP3, int userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String date = "";
        if (checkTimeRange()) {
            date = getPreviousDate();
        } else {
            date = getDate();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);
        contentValues.put("time", getTime());
        contentValues.put("shift", shift);
        contentValues.put("machine_no", machineNo);
        contentValues.put("sku", sku);
        contentValues.put("user_id", userId);
        contentValues.put("p1_sign", signP1);
        contentValues.put("p1_name", nameP1);
        contentValues.put("p2_sign", signP2);
        contentValues.put("p2_name", nameP2);
        contentValues.put("p3_sign", signP3);
        contentValues.put("p3_name", nameP3);
        contentValues.put("date_time", date + " " + getTime());
        long i = db.insert("online_sansory_evaluation", null, contentValues);
        return i;
    }

    public boolean updateOnlineSensoryEvaluationRecord(String shift, String machineNo, String sku, String signP1, String nameP1, String signP2, String nameP2, String signP3, String nameP3, int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("shift", shift);
        contentValues.put("machine_no", machineNo);
        contentValues.put("sku", sku);
        contentValues.put("p1_sign", signP1);
        contentValues.put("p1_name", nameP1);
        contentValues.put("p2_sign", signP2);
        contentValues.put("p2_name", nameP2);
        contentValues.put("p3_sign", signP3);
        contentValues.put("p3_name", nameP3);
        long i = db.update("online_sansory_evaluation", contentValues,"id = ? ", new String[] { Integer.toString(id) });
        if (i < 0) {
            return false;
        } else {
            return true;
        }
    }

    public long addOnlinePannelistData(long onlineSensoryId, int panelistNo, int batchNo, String appearance, String taste, String aroma ) {
        SQLiteDatabase db = this.getWritableDatabase();
        String date = "";
        if (checkTimeRange()) {
            date = getPreviousDate();
        } else {
            date = getDate();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("online_sensory_id", onlineSensoryId);
        contentValues.put("pannelist_no", panelistNo);
        contentValues.put("batch_no", batchNo);
        contentValues.put("apperance", appearance);
        contentValues.put("taste", taste);
        contentValues.put("aroma", aroma);
        contentValues.put("date_time", date + " " + getTime());
        long i = db.insert("pannelist_data", null, contentValues);
        return i;
    }



    public boolean updateOnlinePannelistData(String appearance, String taste, String aroma, int id ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("apperance", appearance);
        contentValues.put("taste", taste);
        contentValues.put("aroma", aroma);
        long i = db.update("pannelist_data", contentValues, "id = ? ", new String[] { Integer.toString(id) });
        if (i < 0) {
            return false;
        } else {
            return true;
        }
    }

    public boolean deleteOnlineSensoryData(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int i = db.delete("online_sansory_evaluation","id = ? ", new String[] { Integer.toString(id) });
        if (i < 0) {
            return false;
        } else {
            int j = db.delete("pannelist_data","online_sensory_id = ? ", new String[] { Integer.toString(id) });
            if (j < 0) {
                return false;
            } else {
                return true;
            }
        }
    }

    public Cursor getOnlineSensoryUnverified() {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select *  from online_sansory_evaluation WHERE verify_status = 1", null );
        Log.e("LogMsg", "Data Count " + c.getCount());
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public long verifyOnlineSensoryRecord(String name, String signature) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", getDate());
        contentValues.put("name", name);
        contentValues.put("sign", signature);
        long id = db.insert("online_sansory_evaluation_sign", null, contentValues);
        return id;
    }

    public boolean updateOnlineSansoryVerification(int id, int verifyId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("verify_status", 2);
        contentValues.put("verify_id", verifyId);
        db.update("online_sansory_evaluation", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Cursor getOnlineSansoryData(String fromDate, String toDate, String module) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select *  from online_sansory_evaluation WHERE machine_no = '" + module + "' AND date BETWEEN '" + fromDate + "' AND '" + toDate + "'", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public Cursor getSingleOnlineSansoryData(int id) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select *  from online_sansory_evaluation WHERE id  = " + id, null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public ArrayList<Module> getModule1Data() {
        ArrayList<Module> modules = new ArrayList<>();
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);

        Cursor c1 = db.rawQuery("SELECT qr_codes.*, form_type.form_name AS f_name, form_type.table_name AS t_name, form_type.checking_timing AS check_time, form_type.machine_field_name as m_field_name from qr_codes INNER JOIN form_type ON form_type.id = qr_codes.form_type_id WHERE qr_codes.status = 1 AND qr_codes.module != 2", null);
        while (c1.moveToNext()) {
            String tableName = c1.getString(c1.getColumnIndex("t_name"));
            int checkTime = c1.getInt(c1.getColumnIndex("check_time"));
            String moduleName = c1.getString(c1.getColumnIndex("f_name")) + " " + c1.getString(c1.getColumnIndex("machine_name")) ;
            String machineField = c1.getString(c1.getColumnIndex("m_field_name"));
            String machineName = c1.getString(c1.getColumnIndex("machine_name"));
            boolean status;
            Cursor c2 = db.rawQuery("Select * FROM " + tableName + " WHERE "+ machineField +"= '"+ machineName +"' ORDER BY id DESC LIMIT 1", null);
            String lastDataDate = "";
            String lastDataTime = "";
            while (c2.moveToNext()) {
                lastDataDate = c2.getString(c2.getColumnIndex("date"));
                lastDataTime = c2.getString(c2.getColumnIndex("time"));
            }
            Log.e("LogMsg", "Machine: " + machineName + ", Last date: " + lastDataDate);
            if (lastDataDate.equals(getDate())) {
                if (checkTime == 1) {

                    if (checkTimeRangeShift("06:00:00", "14:00:00", getTime()) && checkTimeRangeShift("06:00:00", "14:00:00", lastDataTime)) {
                        status = true;
                    } else if (checkTimeRangeShift("14:00:00", "22:00:00", getTime()) && checkTimeRangeShift("14:00:00", "22:00:00", lastDataTime)) {
                        status = true;
                    } else if (checkTimeRangeShift("22:00:00", "06:00:00", getTime()) && checkTimeRangeShift("22:00:00", "06:00:00", lastDataTime)) {
                        status = true;
                    } else {
                        status = false;
                    }
                } else if (checkTime == 2) {
                    if (checkTimeRangPlusHour(getTime(), lastDataTime, 4)) {
                        status = true;
                    } else {
                        status = false;
                    }
                } else {
                    if (checkTimeRangPlusHour(getTime(), lastDataTime, 1)) {
                        status = true;
                    } else {
                        status = false;
                    }
                }
            } else {
                status = false;
            }
            Module module = new Module(moduleName, lastDataDate, lastDataTime, status);
            modules.add(module);
        }
        return modules;
    }

    public ArrayList<Module> getModule2Data() {
        ArrayList<Module> modules = new ArrayList<>();
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);

        Cursor c1 = db.rawQuery("SELECT qr_codes.*, form_type.form_name AS f_name, form_type.table_name AS t_name, form_type.checking_timing AS check_time, form_type.machine_field_name as m_field_name from qr_codes INNER JOIN form_type ON form_type.id = qr_codes.form_type_id WHERE qr_codes.status = 1 AND qr_codes.module != 1", null);
        while (c1.moveToNext()) {
            String tableName = c1.getString(c1.getColumnIndex("t_name"));
            int checkTime = c1.getInt(c1.getColumnIndex("check_time"));
            String moduleName = c1.getString(c1.getColumnIndex("f_name")) + " " + c1.getString(c1.getColumnIndex("machine_name")) ;
            String machineField = c1.getString(c1.getColumnIndex("m_field_name"));
            String machineName = c1.getString(c1.getColumnIndex("machine_name"));
            boolean status;
            Cursor c2 = db.rawQuery("Select * FROM " + tableName + " WHERE "+ machineField +"= '"+ machineName +"' ORDER BY id DESC LIMIT 1", null);
            String lastDataDate = "";
            String lastDataTime = "";
            while (c2.moveToNext()) {
                lastDataDate = c2.getString(c2.getColumnIndex("date"));
                lastDataTime = c2.getString(c2.getColumnIndex("time"));
                if (lastDataDate == null) {
                    lastDataDate = "";
                }
            }
            Log.e("LogMsg", "Machine: " + machineName + ", Last date: " + lastDataDate);
            if (lastDataDate.equals(getDate())) {
                if (checkTime == 1) {

                    if (checkTimeRangeShift("06:00:00", "14:00:00", getTime()) && checkTimeRangeShift("06:00:00", "14:00:00", lastDataTime)) {
                        status = true;
                    } else if (checkTimeRangeShift("14:00:00", "22:00:00", getTime()) && checkTimeRangeShift("14:00:00", "22:00:00", lastDataTime)) {
                        status = true;
                    } else if (checkTimeRangeShift("22:00:00", "06:00:00", getTime()) && checkTimeRangeShift("22:00:00", "06:00:00", lastDataTime)) {
                        status = true;
                    } else {
                        status = false;
                    }
                } else if (checkTime == 2) {
                    if (checkTimeRangPlusHour(getTime(), lastDataTime, 4)) {
                        status = true;
                    } else {
                        status = false;
                    }
                } else {
                    if (checkTimeRangPlusHour(getTime(), lastDataTime, 1)) {
                        status = true;
                    } else {
                        status = false;
                    }
                }
            } else {
                status = false;
            }
            Module module = new Module(moduleName, lastDataDate, lastDataTime, status);
            modules.add(module);
        }
        return modules;
    }

    public boolean addVerifierSignature(int userId, String signature) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("sign", signature);
        long i = db.update("users", contentValues, "id = ? ", new String[] { Integer.toString(userId) } );
        if (i < 0) {
            return false;
        } else {
            return true;
        }
    }

    public String getVerifierSignature(int userId) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery("SELECT sign from users WHERE id = " + userId, null);
        c.moveToNext();
        return c.getString(c.getColumnIndex("sign"));
    }

    public String getVerifierName(int userId) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery("SELECT name from users WHERE id = " + userId, null);
        c.moveToNext();
        return c.getString(c.getColumnIndex("name"));
    }

    public ArrayList<Forms> getFormsVerficationStatus() {
        ArrayList<Forms> forms = new ArrayList<>();
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c1 = db.rawQuery("SELECT * FROM form_type", null);
        while (c1.moveToNext()) {
            String tableName = c1.getString(c1.getColumnIndex("table_name"));
            Cursor c2 = db.rawQuery("SELECT * FROM "+ tableName + " WHERE verify_status = 1 ", null);
            boolean isVerified;
            Log.e("LogMsg", "Count: " + c2.getCount());
            if (c2.getCount() > 0) {
                isVerified = false;
            } else {
                isVerified = true;
            }
            Forms form = new Forms(c1.getInt(c1.getColumnIndex("id")), isVerified, c2.getCount());
            forms.add(form);
        }
        return forms;
    }

    public OnlineSensoryBatchesData getOnlineSensoryBatchesData(int onlineSensoryId, int pannelistNo) {
        OnlineSensoryBatchesData onlineSensoryBatchesData;
        ArrayList<Forms> forms = new ArrayList<>();
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery("SELECT * FROM pannelist_data WHERE online_sensory_id = " + onlineSensoryId + " AND pannelist_no = " + pannelistNo, null);
        ArrayList<Integer> batchNos = new ArrayList<>();
        ArrayList<Integer> appearance = new ArrayList<>();
        ArrayList<Integer> taste = new ArrayList<>();
        ArrayList<Integer> aroma = new ArrayList<>();
        ArrayList<Integer> ids = new ArrayList<>();
        while (c.moveToNext()) {
            batchNos.add(c.getInt(c.getColumnIndex("batch_no")));
            appearance.add(c.getInt(c.getColumnIndex("apperance")));
            taste.add(c.getInt(c.getColumnIndex("taste")));
            aroma.add(c.getInt(c.getColumnIndex("aroma")));
            ids.add(c.getInt(c.getColumnIndex("id")));
        }
        onlineSensoryBatchesData = new OnlineSensoryBatchesData(batchNos, appearance, taste, aroma, ids);
        return onlineSensoryBatchesData;
    }

    public LastBatch getLastBatches() {
        LastBatch lastBatch = null;
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c1 = db.rawQuery("SELECT * FROM online_sansory_evaluation ORDER BY id DESC LIMIT 1", null);
        while (c1.moveToNext()) {
            Cursor c2 = db.rawQuery("SELECT DISTINCT(batch_no) FROM pannelist_data WHERE online_sensory_id = " + c1.getInt(c1.getColumnIndex("id")), null);
            lastBatch = new LastBatch(c1.getString(c1.getColumnIndex("date_time")), c2);
            return lastBatch;
        }
        return lastBatch;
    }

    public Cursor getDistinctBatches(int id) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery("SELECT DISTINCT(batch_no) FROM pannelist_data WHERE online_sensory_id = " + id, null);
        return c;
    }

    public Cursor getPannelistData(int id, int pannelistNo, int batchNo) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery("SELECT * FROM pannelist_data WHERE online_sensory_id = " + id + " AND pannelist_no = " + pannelistNo + " AND batch_no = " + batchNo, null);
        return c;
    }

    public long insertStartupChecklist(String machineName, String signature, int userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String date = "";
        if (checkTimeRange()) {
            date = getPreviousDate();
        } else {
            date = getDate();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);
        contentValues.put("time", getTime());
        contentValues.put("module", machineName);
        contentValues.put("quality_manager_sign", signature);
        contentValues.put("user_id", userId);
        contentValues.put("date_time", date + " " + getTime());
        long i = db.insert("start_up_checklist", null, contentValues);
        return i;
    }

    public long insertStartupCheckListData(String status, String remarks, String actionTaken, String finalStatus){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("status", status);
        contentValues.put("remarks", remarks);
        contentValues.put("action_taken", actionTaken);
        contentValues.put("final_status", finalStatus);
        long i = db.insert("start_up_checklist_data", null, contentValues);
        return i;
    }

    public long updateStartupCheckListData(long id,
                                            long sieve1Id,
                                           long sieve2Id,
                                           long sieve3Id,
                                           long sieve4Id,
                                           long packing1Id,
                                           long packing2Id,
                                           long packing3Id,
                                           long packing4Id,
                                           long packing5Id,
                                           long packing6Id,
                                           long packing7Id,
                                           long packing8Id,
                                           long packing9Id,
                                           long packing10Id,
                                           long packing11Id,
                                           long packing12Id,
                                           long packing13Id,
                                           long packing14Id,
                                           long packing15Id,
                                           long packing16Id,
                                           long packing17Id,
                                           long packing18Id,
                                           long packing19Id,
                                           long packing20Id,
                                           long packing21Id,
                                           long packing22Id,
                                           long packing23Id,
                                           long packing24Id,
                                           long ovenId1,
                                           long ovenId2,
                                           long ovenId3,
                                           long ovenId4,
                                           long ovenId5,
                                           long vpId1,
                                           long vpId2,
                                           long vpId3,
                                           long vpId4,
                                           long vpId5,
                                           long vpId6,
                                           long vpId7
                                           ) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("seive1", sieve1Id);
        contentValues.put("seive2", sieve2Id);
        contentValues.put("seive3", sieve3Id);
        contentValues.put("seive4", sieve4Id);
        contentValues.put("packing1", packing1Id);
        contentValues.put("packing2", packing2Id);
        contentValues.put("packing3", packing3Id);
        contentValues.put("packing4", packing4Id);
        contentValues.put("packing5", packing5Id);
        contentValues.put("packing6", packing6Id);
        contentValues.put("packing7", packing7Id);
        contentValues.put("packing8", packing8Id);
        contentValues.put("packing9", packing9Id);
        contentValues.put("packing10", packing10Id);
        contentValues.put("packing11", packing11Id);
        contentValues.put("packing12", packing12Id);
        contentValues.put("packing13", packing13Id);
        contentValues.put("packing14", packing14Id);
        contentValues.put("packing15", packing15Id);
        contentValues.put("packing16", packing16Id);
        contentValues.put("packing17", packing17Id);
        contentValues.put("packing18", packing18Id);
        contentValues.put("packing19", packing19Id);
        contentValues.put("packing20", packing20Id);
        contentValues.put("packing21", packing21Id);
        contentValues.put("packing22", packing22Id);
        contentValues.put("packing23", packing23Id);
        contentValues.put("packing24", packing24Id);
        contentValues.put("oven1", ovenId1);
        contentValues.put("oven2", ovenId2);
        contentValues.put("oven3", ovenId3);
        contentValues.put("oven4", ovenId4);
        contentValues.put("oven5", ovenId5);
        contentValues.put("vp1", vpId1);
        contentValues.put("vp2", vpId2);
        contentValues.put("vp3", vpId3);
        contentValues.put("vp4", vpId4);
        contentValues.put("vp5", vpId5);
        contentValues.put("vp6", vpId6);
        contentValues.put("vp7", vpId7);
        long i = db.update("start_up_checklist", contentValues, "id = ? ", new String[] { Long.toString(id) } );
        return i;
    }

    public Cursor getStartUpCheckListUnverified() {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select start_up_checklist.*, users.name as username  from start_up_checklist INNER JOIN users on start_up_checklist.user_id = users.id WHERE start_up_checklist.verify_status = 1", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public Cursor getStartUpCheckListData(int id) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery("SELECT * from start_up_checklist_data WHERE id = " + id, null);
        return c;
    }

    public boolean updateStartUpCheckListVerification(int id, long verifyId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("verify_status", 2);
        contentValues.put("verify_id", verifyId);
        db.update("start_up_checklist", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Cursor getStartChecklistData(String fromDate, String toDate, String module) {
        String myPath = DB_PATH + DB_NAME;
        db = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.OPEN_READONLY);
        Cursor c = db.rawQuery( "select start_up_checklist.*,users.name as username from start_up_checklist INNER JOIN users ON users.id = start_up_checklist.user_id WHERE start_up_checklist.module = '" + module + "' AND start_up_checklist.date BETWEEN '" + fromDate + "' AND '" + toDate + "'", null );
        // Note: Master is the one table in External db. Here we trying to access the records of table from external db.
        return c;
    }

    public boolean deleteStartUpCheckListData(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int i = db.delete("start_up_checklist", "id = ? ", new String[] { Integer.toString(id) } );
        if (i < 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }



}
