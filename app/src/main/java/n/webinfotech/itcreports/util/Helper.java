package n.webinfotech.itcreports.util;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Raj on 14-09-2019.
 */

public class Helper {

    public static byte[] getByteFromDrawable(Drawable drawable) {
        Bitmap logoBitmap = ((BitmapDrawable)drawable).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        logoBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] bytes = stream.toByteArray();
        return bytes;
    }

    public static byte[] getByteFromFile(String signature) {
        Log.e("LogMsg", "Signatuire " + signature);
        File file = new File(getAlbumStorageDir("ITCReports/Signatures"), signature);
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            Log.e("LogMsg", "Error: " + e.getMessage());
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("LogMsg", "Error: " + e.getMessage());
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    public static File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStorageDirectory(), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    public static String changeDateFormat(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-m-d");
        Date sourceDate = null;
        try {
            sourceDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-mm-dd");
        String targetdatevalue= targetFormat.format(sourceDate);
        return targetdatevalue;
    }

    public static String getSearchCurrentDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String currentDateandTime = sdf.format(new Date());
        return currentDateandTime;
    }

    public static String getSearchNextDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, +1);
        String previousDate = dateFormat.format(cal.getTime());
        return previousDate;
    }

    public static String getPreviousDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String previousDate = dateFormat.format(cal.getTime());
        return previousDate;
    }

    public static String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static boolean checkTimeRange() {
        try {
//            Target time
            String string2 = "05:59:59";
            Date time2 = new SimpleDateFormat("HH:mm:ss").parse(string2);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(time2);
            calendar2.add(Calendar.DATE, 1);

//            Current time
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "HH:mm:ss", Locale.getDefault());
            Date date = new Date();
            Date d = new SimpleDateFormat("HH:mm:ss").parse(dateFormat.format(date));
            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(d);
            calendar3.add(Calendar.DATE, 1);
            Date x = calendar3.getTime();

            if (x.before(calendar2.getTime())) {
                //checkes whether the current time is between 14:49:00 and 20:11:13.
                return true;
            } else {
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean checkTimeRangeShift(String shiftTime1, String shiftTime2, String time) {
        try {
            Date time1 = new SimpleDateFormat("HH:mm:ss").parse(shiftTime1);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(time1);
            calendar1.add(Calendar.DATE, 1);


            Date time2 = new SimpleDateFormat("HH:mm:ss").parse(shiftTime2);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(time2);
            calendar2.add(Calendar.DATE, 1);

            Date d = new SimpleDateFormat("HH:mm:ss").parse(time);
            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(d);
            calendar3.add(Calendar.DATE, 1);

            Date x = calendar3.getTime();

            if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
                //checkes whether the current time is between 14:49:00 and 20:11:13.
                return true;
            } else {
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean checkTimeRangPlusHour(String currentTime, String checkTime, int hour) {
        try {
            Date time1 = new SimpleDateFormat("HH:mm:ss").parse(checkTime);
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(time1);
            calendar1.add(Calendar.HOUR, hour);
            Date y = calendar1.getTime();

            SimpleDateFormat checkTimeDate =  new SimpleDateFormat("HH:mm:ss");

            Date time2 = new SimpleDateFormat("HH:mm:ss").parse(currentTime);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(time2);

            Date x = calendar2.getTime();

            SimpleDateFormat checkTimeDate2 =  new SimpleDateFormat("HH:mm:ss");

            if (x.before(calendar1.getTime())) {
                return true;
            } else {
                return false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

}
