package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.Module;

public class ModuleAdapter extends RecyclerView.Adapter<ModuleAdapter.ViewHolder> {

    Context mContext;
    ArrayList<Module> modules;

    public ModuleAdapter(Context mContext, ArrayList<Module> modules) {
        this.mContext = mContext;
        this.modules = modules;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.txtViewModuleName.setText(modules.get(i).moduleName);
        viewHolder.txtViewModuleDate.setText(modules.get(i).lastCheckedDate);
        viewHolder.txtViewModuleTime.setText(modules.get(i).lastCheckedTime);
        if (modules.get(i).status) {
            viewHolder.imgViewModuleCheck.setVisibility(View.VISIBLE);
            viewHolder.imgViewModuleCancel.setVisibility(View.GONE);
        } else {
            viewHolder.imgViewModuleCancel.setVisibility(View.VISIBLE);
            viewHolder.imgViewModuleCheck.setVisibility(View.GONE);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_module, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public int getItemCount() {
        return modules.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_module_name)
        TextView txtViewModuleName;
        @BindView(R.id.txt_view_module_date)
        TextView txtViewModuleDate;
        @BindView(R.id.txt_view_module_time)
        TextView txtViewModuleTime;
        @BindView(R.id.img_view_module_check)
        ImageView imgViewModuleCheck;
        @BindView(R.id.img_view_module_cancel)
        ImageView imgViewModuleCancel;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
