package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.CFCChecking;

/**
 * Created by Raj on 14-09-2019.
 */

public class CFCCheckingUnverifiedAdapter extends RecyclerView.Adapter<CFCCheckingUnverifiedAdapter.ViewHolder> {

    public interface Callback{
        void insertToArray(int id);
        void removeFromArray(int id);
    }

    Context mContext;
    ArrayList<CFCChecking> cfcCheckings;
    Callback mCallback;

    public CFCCheckingUnverifiedAdapter(Context mContext, ArrayList<CFCChecking> cfcCheckings, Callback mCallback) {
        this.mContext = mContext;
        this.cfcCheckings = cfcCheckings;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_cfc_list, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewCodingCFC.setText(cfcCheckings.get(i).codingCFC);
        viewHolder.txtViewCodingMono.setText(cfcCheckings.get(i).codingMono);
        viewHolder.txtViewDate.setText(cfcCheckings.get(i).date);
        viewHolder.txtViewDent.setText(cfcCheckings.get(i).dent);
        viewHolder.txtViewFlap.setText(cfcCheckings.get(i).flap);
        viewHolder.txtViewMachineNo.setText(cfcCheckings.get(i).machineNo);
        viewHolder.txtViewQtyCFC.setText(cfcCheckings.get(i).qtyPresentCFC);
        viewHolder.txtViewQtyMono.setText(cfcCheckings.get(i).qtyPresentMono);
        viewHolder.txtViewShift.setText(cfcCheckings.get(i).shift);
        viewHolder.txtViewSku.setText(cfcCheckings.get(i).sku);
        viewHolder.txtViewTapping.setText(cfcCheckings.get(i).tapping);
        viewHolder.txtViewTime.setText(cfcCheckings.get(i).time);
        viewHolder.txtViewTorn.setText(cfcCheckings.get(i).torn);
        viewHolder.txtViewRemarks.setText(cfcCheckings.get(i).remarks);
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.insertToArray(cfcCheckings.get(i).id);
                } else {
                    mCallback.removeFromArray(cfcCheckings.get(i).id);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cfcCheckings.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_shift)
        TextView txtViewShift;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_machine_no)
        TextView txtViewMachineNo;
        @BindView(R.id.txt_view_sku)
        TextView txtViewSku;
        @BindView(R.id.txt_view_torn)
        TextView txtViewTorn;
        @BindView(R.id.txt_view_flap)
        TextView txtViewFlap;
        @BindView(R.id.txt_view_dent)
        TextView txtViewDent;
        @BindView(R.id.txt_view_coding_mono)
        TextView txtViewCodingMono;
        @BindView(R.id.txt_view_qty_mono)
        TextView txtViewQtyMono;
        @BindView(R.id.txt_view_qty_cfc)
        TextView txtViewQtyCFC;
        @BindView(R.id.txt_view_tapping)
        TextView txtViewTapping;
        @BindView(R.id.txt_view_coding_cfc)
        TextView txtViewCodingCFC;
        @BindView(R.id.txt_view_remarks)
        TextView txtViewRemarks;
        @BindView(R.id.checkbox)
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
