package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.DHRecord;
import n.webinfotech.itcreports.util.DBHelper;

/**
 * Created by Raj on 19-09-2019.
 */

public class DHRoomRecordReportAdapter extends RecyclerView.Adapter<DHRoomRecordReportAdapter.ViewHolder> {

    public interface Callback {
        void onEditClicked(int id);
        void onDeleteClicked(int id);
    }

    Context mContext;
    ArrayList<DHRecord> dhRecords;
    DBHelper dbHelper;
    Callback mCallback;

    public DHRoomRecordReportAdapter(Context mContext, ArrayList<DHRecord> dhRecords, Callback callback) {
        this.mContext = mContext;
        this.dhRecords = dhRecords;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_dh_room_report_adapter, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        dbHelper = new DBHelper(mContext);
        viewHolder.txtViewShift.setText(dhRecords.get(i).shift);
        viewHolder.txtViewActTemp.setText(dhRecords.get(i).actTemp);
        viewHolder.txtViewCheckedBy.setText(dhRecords.get(i).username);
        viewHolder.txtViewDate.setText(dhRecords.get(i).date);
        viewHolder.txtViewRemarks.setText(dhRecords.get(i).remarks);
        viewHolder.txtViewRh.setText(dhRecords.get(i).rh);
        viewHolder.txtViewTank.setText(dhRecords.get(i).tank);
        viewHolder.txtViewTime.setText(dhRecords.get(i).time);
        if (dhRecords.get(i).verifyStatus == 2) {
            viewHolder.txtViewVerifiedBy.setText(dbHelper.getVerifierName(dhRecords.get(i).verifyId));
        }

        viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(dhRecords.get(i).id);
            }
        });

        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirm dialog demo !");
                builder.setMessage("You are about to delete a record. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onDeleteClicked(dhRecords.get(i).id);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
            }
        });
        viewHolder.txtViewSlNo.setText(Integer.toString(i + 1));


    }

    @Override
    public int getItemCount() {
        return dhRecords.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_shift)
        TextView txtViewShift;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_tank)
        TextView txtViewTank;
        @BindView(R.id.txt_view_act_temp)
        TextView txtViewActTemp;
        @BindView(R.id.txt_view_rh)
        TextView txtViewRh;
        @BindView(R.id.txt_view_checked_by)
        TextView txtViewCheckedBy;
        @BindView(R.id.txt_view_remarks)
        TextView txtViewRemarks;
        @BindView(R.id.txt_view_verified_by)
        TextView txtViewVerifiedBy;
        @BindView(R.id.btn_edit)
        Button btnEdit;
        @BindView(R.id.btn_delete)
        Button btnDelete;
        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
