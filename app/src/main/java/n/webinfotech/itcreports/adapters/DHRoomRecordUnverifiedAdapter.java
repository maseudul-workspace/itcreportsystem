package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.DHRecord;

/**
 * Created by Raj on 16-09-2019.
 */

public class DHRoomRecordUnverifiedAdapter extends RecyclerView.Adapter<DHRoomRecordUnverifiedAdapter.ViewHolder> {

    public interface Callback {
        void insertToArray(int id);
        void removeFromArray(int id);
    }

    Context mContext;
    ArrayList<DHRecord> dhRecords;
    Callback mCallback;

    public DHRoomRecordUnverifiedAdapter(Context mContext, ArrayList<DHRecord> dhRecords, Callback mCallback) {
        this.mContext = mContext;
        this.dhRecords = dhRecords;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_dh_room_record, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewShift.setText(dhRecords.get(i).shift);
        viewHolder.txtViewActTemp.setText(dhRecords.get(i).actTemp);
        viewHolder.txtViewCheckedBy.setText(dhRecords.get(i).username);
        viewHolder.txtViewDate.setText(dhRecords.get(i).date);
        viewHolder.txtViewRemarks.setText(dhRecords.get(i).remarks);
        viewHolder.txtViewRh.setText(dhRecords.get(i).rh);
        viewHolder.txtViewTank.setText(dhRecords.get(i).tank);
        viewHolder.txtViewTime.setText(dhRecords.get(i).time);
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.insertToArray(dhRecords.get(i).id);
                } else {
                    mCallback.removeFromArray(dhRecords.get(i).id);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dhRecords.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_shift)
        TextView txtViewShift;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_tank)
        TextView txtViewTank;
        @BindView(R.id.txt_view_act_temp)
        TextView txtViewActTemp;
        @BindView(R.id.txt_view_rh)
        TextView txtViewRh;
        @BindView(R.id.checkbox)
        CheckBox checkBox;
        @BindView(R.id.txt_view_checked_by)
        TextView txtViewCheckedBy;
        @BindView(R.id.txt_view_remarks)
        TextView txtViewRemarks;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
