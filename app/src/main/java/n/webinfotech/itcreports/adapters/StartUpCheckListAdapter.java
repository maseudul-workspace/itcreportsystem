package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.StartCheckList;

import static n.webinfotech.itcreports.util.Helper.getAlbumStorageDir;

public class StartUpCheckListAdapter extends RecyclerView.Adapter<StartUpCheckListAdapter.ViewHolder> {

    public interface Callback {
        void onViewClicked(int id, String name);
        void insertToArray(int id);
        void removeFromArray(int id);
    }

    Context mContext;
    ArrayList<StartCheckList> startCheckLists;
    Callback mCallback;

    public StartUpCheckListAdapter(Context mContext, ArrayList<StartCheckList> startCheckLists, Callback mCallback) {
        this.mContext = mContext;
        this.startCheckLists = startCheckLists;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_start_up_verification, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.btnSieve1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).seive1, "Seiving: Check the condition of  seives and magnets");
            }
        });

        viewHolder.btnSieve2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).seive2, "Seiving: All materials are stored on pallets ");
            }
        });

        viewHolder.btnSieve3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).seive3, "Seiving: Floor Cleaning ");
            }
        });

        viewHolder.btnSieve4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).seive4, "Seiving: Condition of PVC Stripes ");
            }
        });

        viewHolder.btnPacking1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing1, "Packing and Manufacturing: Availability of IPA and Lint Roller ");
            }
        });

        viewHolder.getBtnPacking2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing2, "Packing and Manufacturing: Entrance Pathway Should be Cleaned and Cleared from any stored materials ");
            }
        });

        viewHolder.getBtnPacking3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing3, "Packing and Manufacturing: Cleanliness of process washroom ");
            }
        });

        viewHolder.getBtnPacking4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing4, "Packing and Manufacturing: Availability of Hot Water in Washroom ");
            }
        });

        viewHolder.getBtnPacking5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing5, "Packing and Manufacturing: All persons working are with Lindstorm Uniform ");
            }
        });

        viewHolder.getBtnPacking6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing6, "Packing and Manufacturing: Cleanliness of freezer ");
            }
        });

        viewHolder.getBtnPacking7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing7, "Packing and Manufacturing: Cleanliness of Scapper/Beater/Mixer bowl and overall mixing machine ");
            }
        });

        viewHolder.getBtnPacking8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing8, "Packing and Manufacturing: Cleanliness of Weighing balance and Table ");
            }
        });

        viewHolder.getBtnPacking9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing9, "Packing and Manufacturing: Cleanliness of all SS Containers/LIDS/Trolleys and Scoops ");
            }
        });

        viewHolder.getBtnPacking10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing10, "Packing and Manufacturing: Cleanliness of Dough and Cream Hopper ");
            }
        });

        viewHolder.getBtnPacking11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing11, "Packing and Manufacturing: Cleanliness of Feeding chute and flow controller of cream and dough ");
            }
        });

        viewHolder.getBtnPacking12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing12, "Packing and Manufacturing: Cleanliness of compound nozzles and encruster ");
            }
        });

        viewHolder.getBtnPacking13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing13, "Packing and Manufacturing: Cleanliness of panning belt/pressure roller and overall panning section ");
            }
        });

        viewHolder.getBtnPacking14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing14, "Packing and Manufacturing: Cleanliness of SS trays used ");
            }
        });

        viewHolder.getBtnPacking15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing15, "Packing and Manufacturing: No accumulation of empty Choco Cream Container ");
            }
        });

        viewHolder.getBtnPacking16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing16, "Packing and Manufacturing: No containers / trays / crates are stored on floor ");
            }
        });

        viewHolder.getBtnPacking17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing17, "Packing and Manufacturing: Ensure the instructor working ");
            }
        });

        viewHolder.getBtnPacking18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing18, "Packing and Manufacturing: Floor Cleaning ");
            }
        });
        viewHolder.getBtnPacking19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing19, "Packing and Manufacturing: Only Green Crates to be used ");
            }
        });

        viewHolder.getBtnPacking20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing20, "Packing and Manufacturing: No unwanted substances like loose nuts / plastics etc above pannels and all other area ");
            }
        });

        viewHolder.getBtnPacking21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing21, "Packing and Manufacturing: Cleanliness of taking conveyors ");
            }
        });

        viewHolder.getBtnPacking22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing22, "Packing and Manufacturing: Cleanliness of MD Rejection Bean ");
            }
        });

        viewHolder.getBtnPacking23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing23, "Packing and Manufacturing: Cleanliness of knives of all conveyors ");
            }
        });

        viewHolder.getBtnPacking24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).packing24, "Packing and Manufacturing: Cleanliness of crates trays ");
            }
        });

        viewHolder.btnOven1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).oven1, "Oven Section and DH Room: Floor Cleaning ");
            }
        });

        viewHolder.btnOven2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).oven2, "Oven Section and DH Room: Cleanliness of moisture analyser and weighing balance: ");
            }
        });

        viewHolder.btnOven3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).oven3, "Oven Section and DH Room: Cleanliness of oven ");
            }
        });

        viewHolder.btnOven4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).oven4, "Oven Section and DH Room: Ensure the instructor working ");
            }
        });

        viewHolder.btnOven5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).oven5, "Oven Section and DH Room: RH and DH room is maintained ");
            }
        });

        viewHolder.btnVP1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).vp1, "VP Section: Empty monocartons should be stored in trays / box container ");
            }
        });

        viewHolder.btnVP2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).vp2, "VP Section: Only green crates to be used ");
            }
        });

        viewHolder.btnVP3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).vp3, "VP Section: No unwanted material is stored above machines ");
            }
        });

        viewHolder.btnVP4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).vp4, "VP Section: Cleanliness of glowing unit ");
            }
        });

        viewHolder.btnVP5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).vp5, "VP Section: Ensure the instructor working ");
            }
        });

        viewHolder.btnVP6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).vp6, "VP Section: Checkweigher rejection bin is under lock and key ");
            }
        });

        viewHolder.btnVP7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onViewClicked(startCheckLists.get(i).vp7, "VP Section: Floor Cleaning ");
            }
        });

        viewHolder.txtViewIndex.setText(Integer.toString(i + 1));
        viewHolder.txtViewDate.setText(startCheckLists.get(i).date);

        try {
            File file = new File(getAlbumStorageDir("ITCReports/Signatures"), startCheckLists.get(i).signature);
            if(file.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                viewHolder.imgViewQualityManager.setImageBitmap(myBitmap);
            }
        } catch (Exception e) {

        }
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.insertToArray(startCheckLists.get(i).id);
                } else {
                    mCallback.removeFromArray(startCheckLists.get(i).id);
                }
            }
        });

        viewHolder.txtViewCheckedBy.setText(startCheckLists.get(i).username);

    }

    @Override
    public int getItemCount() {
        return startCheckLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.btn_sieve1)
        Button btnSieve1;
        @BindView(R.id.btn_sieve2_view)
        Button btnSieve2;
        @BindView(R.id.btn_sieve3_view)
        Button btnSieve3;
        @BindView(R.id.btn_sieve4_view)
        Button btnSieve4;
        @BindView(R.id.btn_packing1_view)
        Button btnPacking1;
        @BindView(R.id.btn_packing2)
        Button getBtnPacking2;
        @BindView(R.id.btn_packing3)
        Button getBtnPacking3;
        @BindView(R.id.btn_packing4)
        Button getBtnPacking4;
        @BindView(R.id.btn_packing5)
        Button getBtnPacking5;
        @BindView(R.id.btn_packing6)
        Button getBtnPacking6;
        @BindView(R.id.btn_packing7)
        Button getBtnPacking7;
        @BindView(R.id.btn_packing8)
        Button getBtnPacking8;
        @BindView(R.id.btn_packing9)
        Button getBtnPacking9;
        @BindView(R.id.btn_packing10)
        Button getBtnPacking10;
        @BindView(R.id.btn_packing11)
        Button getBtnPacking11;
        @BindView(R.id.btn_packing12)
        Button getBtnPacking12;
        @BindView(R.id.btn_packing13)
        Button getBtnPacking13;
        @BindView(R.id.btn_packing14)
        Button getBtnPacking14;
        @BindView(R.id.btn_packing15)
        Button getBtnPacking15;
        @BindView(R.id.btn_packing16)
        Button getBtnPacking16;
        @BindView(R.id.btn_packing17)
        Button getBtnPacking17;
        @BindView(R.id.btn_packing18)
        Button getBtnPacking18;
        @BindView(R.id.btn_packing19)
        Button getBtnPacking19;
        @BindView(R.id.btn_packing20)
        Button getBtnPacking20;
        @BindView(R.id.btn_packing21)
        Button getBtnPacking21;
        @BindView(R.id.btn_packing22)
        Button getBtnPacking22;
        @BindView(R.id.btn_packing23)
        Button getBtnPacking23;
        @BindView(R.id.btn_packing24)
        Button getBtnPacking24;
        @BindView(R.id.btn_oven1)
        Button btnOven1;
        @BindView(R.id.btn_oven2)
        Button btnOven2;
        @BindView(R.id.btn_oven3)
        Button btnOven3;
        @BindView(R.id.btn_oven4)
        Button btnOven4;
        @BindView(R.id.btn_oven5)
        Button btnOven5;
        @BindView(R.id.btn_vp1)
        Button btnVP1;
        @BindView(R.id.btn_vp2)
        Button btnVP2;
        @BindView(R.id.btn_vp3)
        Button btnVP3;
        @BindView(R.id.btn_vp4)
        Button btnVP4;
        @BindView(R.id.btn_vp5)
        Button btnVP5;
        @BindView(R.id.btn_vp6)
        Button btnVP6;
        @BindView(R.id.btn_vp7)
        Button btnVP7;
        @BindView(R.id.txt_view_index)
        TextView txtViewIndex;
        @BindView(R.id.img_view_quality_manager_sign)
        ImageView imgViewQualityManager;
        @BindView(R.id.checkbox)
        CheckBox checkBox;
        @BindView(R.id.txt_view_checked_by)
        TextView txtViewCheckedBy;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
