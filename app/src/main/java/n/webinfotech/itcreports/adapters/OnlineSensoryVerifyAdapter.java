package n.webinfotech.itcreports.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.OnlineSensory;

/**
 * Created by Raj on 17-09-2019.
 */

public class OnlineSensoryVerifyAdapter extends RecyclerView.Adapter<OnlineSensoryVerifyAdapter.ViewHolder> {

    public interface Callback {
        void insertToArray(int id);
        void removeFromArray(int id);
    }

    Context mContext;
    ArrayList<OnlineSensory> onlineSensories;
    Callback mCallback;
    Activity mActivity;

    public OnlineSensoryVerifyAdapter(Context mContext, Activity activity, ArrayList<OnlineSensory> onlineSensories, Callback mCallback) {
        this.mContext = mContext;
        this.onlineSensories = onlineSensories;
        this.mCallback = mCallback;
        mActivity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_online_sensory_verify_adapter, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewDate.setText(onlineSensories.get(i).date);
        viewHolder.txtViewPanelist1Name.setText(onlineSensories.get(i).nameP1);
        viewHolder.txtViewPanelist2Name.setText(onlineSensories.get(i).nameP2);
        viewHolder.txtViewShift.setText(onlineSensories.get(i).shift);
        viewHolder.txtViewTime.setText(onlineSensories.get(i).time);
        viewHolder.txtViewPanelist3Name.setText(onlineSensories.get(i).nameP3);
        viewHolder.txtViewSku.setText(onlineSensories.get(i).sku);
        viewHolder.txtViewModule.setText(onlineSensories.get(i).module);
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.insertToArray(onlineSensories.get(i).id);
                } else {
                    mCallback.removeFromArray(onlineSensories.get(i).id);
                }
            }
        });
        try {
            File file1 = new File(getAlbumStorageDir("ITCReports/Signatures"), onlineSensories.get(i).signP1);
            File file2 = new File(getAlbumStorageDir("ITCReports/Signatures"), onlineSensories.get(i).signP2);
            File file3 = new File(getAlbumStorageDir("ITCReports/Signatures"), onlineSensories.get(i).signP3);

            if(file1.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(file1.getAbsolutePath());
                viewHolder.imageViewPannelist1.setImageBitmap(myBitmap);
            }

            if(file2.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(file2.getAbsolutePath());
                viewHolder.imageViewPannelist2.setImageBitmap(myBitmap);
            }

            if(file3.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(file3.getAbsolutePath());
                viewHolder.imageViewPanelist3.setImageBitmap(myBitmap);
            }
        } catch (Exception e) {

        }

//      PANNELIST1 BATCH NOS
        ArrayList<Integer> pannelist1BatchNos = onlineSensories.get(i).pannelist1Data.batchNos;
        for (int j = 0; j < pannelist1BatchNos.size(); j++) {
            View view = mActivity.getLayoutInflater().inflate(R.layout.layout_batch_no, viewHolder.linearLayoutPannelist1Batch, false);
            TextView txtViewBatch = (TextView) view.findViewById(R.id.txt_view_batch_no);
            txtViewBatch.setText(Integer.toString(pannelist1BatchNos.get(j)));
            viewHolder.linearLayoutPannelist1Batch.addView(view);
        }

//        Pannelist1 Appearances
        ArrayList<Integer> pannelist1AppearanceNos = onlineSensories.get(i).pannelist1Data.appearance;
        for (int j = 0; j < pannelist1AppearanceNos.size(); j++) {
            View view = mActivity.getLayoutInflater().inflate(R.layout.layout_blue_bg, viewHolder.linearLayoutPannelist1Appearance, false);
            TextView txtViewBatch = (TextView) view.findViewById(R.id.txt_view_data);
            txtViewBatch.setText(Integer.toString(pannelist1AppearanceNos.get(j)));
            viewHolder.linearLayoutPannelist1Appearance.addView(view);
        }

//        Pannelist1 Taste
        ArrayList<Integer> pannelist1Taste = onlineSensories.get(i).pannelist1Data.taste;
        for (int j = 0; j < pannelist1Taste.size(); j++) {
            View view = mActivity.getLayoutInflater().inflate(R.layout.layout_blue_bg, viewHolder.linearLayoutPannelist1Taste, false);
            TextView txtViewBatch = (TextView) view.findViewById(R.id.txt_view_data);
            txtViewBatch.setText(Integer.toString(pannelist1Taste.get(j)));
            viewHolder.linearLayoutPannelist1Taste.addView(view);
        }

//        Pannelist1 Aroma
        ArrayList<Integer> pannelist1Aroma = onlineSensories.get(i).pannelist1Data.aroma;
        for (int j = 0; j < pannelist1Aroma.size(); j++) {
            View view = mActivity.getLayoutInflater().inflate(R.layout.layout_blue_bg, viewHolder.linearLayoutPannelist1Aroma, false);
            TextView txtViewBatch = (TextView) view.findViewById(R.id.txt_view_data);
            txtViewBatch.setText(Integer.toString(pannelist1Aroma.get(j)));
            viewHolder.linearLayoutPannelist1Aroma.addView(view);
        }

        //      PANNELIST2 BATCH NOS
        ArrayList<Integer> pannelist2BatchNos = onlineSensories.get(i).pannelist2Data.batchNos;
        for (int j = 0; j < pannelist2BatchNos.size(); j++) {
            View view = mActivity.getLayoutInflater().inflate(R.layout.layout_batch_no, viewHolder.linearLayoutPannelist2Batch, false);
            TextView txtViewBatch = (TextView) view.findViewById(R.id.txt_view_batch_no);
            txtViewBatch.setText(Integer.toString(pannelist2BatchNos.get(j)));
            viewHolder.linearLayoutPannelist2Batch.addView(view);
        }

//        Pannelist2 Appearances
        ArrayList<Integer> pannelist2AppearanceNos = onlineSensories.get(i).pannelist2Data.appearance;
        for (int j = 0; j < pannelist2AppearanceNos.size(); j++) {
            View view = mActivity.getLayoutInflater().inflate(R.layout.layout_blue_bg, viewHolder.linearLayoutPannelist2Appearance, false);
            TextView txtViewBatch = (TextView) view.findViewById(R.id.txt_view_data);
            txtViewBatch.setText(Integer.toString(pannelist2AppearanceNos.get(j)));
            viewHolder.linearLayoutPannelist2Appearance.addView(view);
        }

//        Pannelist2 Taste
        ArrayList<Integer> pannelist2Taste = onlineSensories.get(i).pannelist2Data.taste;
        for (int j = 0; j < pannelist2Taste.size(); j++) {
            View view = mActivity.getLayoutInflater().inflate(R.layout.layout_blue_bg, viewHolder.linearLayoutPannelist2Taste, false);
            TextView txtViewBatch = (TextView) view.findViewById(R.id.txt_view_data);
            txtViewBatch.setText(Integer.toString(pannelist2Taste.get(j)));
            viewHolder.linearLayoutPannelist2Taste.addView(view);
        }

//        Pannelist2 Aroma
        ArrayList<Integer> pannelist2Aroma = onlineSensories.get(i).pannelist2Data.aroma;
        for (int j = 0; j < pannelist2Aroma.size(); j++) {
            View view = mActivity.getLayoutInflater().inflate(R.layout.layout_blue_bg, viewHolder.linearLayoutPannelist2Aroma, false);
            TextView txtViewBatch = (TextView) view.findViewById(R.id.txt_view_data);
            txtViewBatch.setText(Integer.toString(pannelist2Aroma.get(j)));
            viewHolder.linearLayoutPannelist2Aroma.addView(view);
        }

        //      PANNELIST3 BATCH NOS
        ArrayList<Integer> pannelist3BatchNos = onlineSensories.get(i).pannelist3Data.batchNos;
        for (int j = 0; j < pannelist3BatchNos.size(); j++) {
            View view = mActivity.getLayoutInflater().inflate(R.layout.layout_batch_no, viewHolder.linearLayoutPannelist3Batch, false);
            TextView txtViewBatch = (TextView) view.findViewById(R.id.txt_view_batch_no);
            txtViewBatch.setText(Integer.toString(pannelist3BatchNos.get(j)));
            viewHolder.linearLayoutPannelist3Batch.addView(view);
        }

//        Pannelist3 Appearances
        ArrayList<Integer> pannelist3AppearanceNos = onlineSensories.get(i).pannelist3Data.appearance;
        for (int j = 0; j < pannelist3AppearanceNos.size(); j++) {
            View view = mActivity.getLayoutInflater().inflate(R.layout.layout_blue_bg, viewHolder.linearLayoutPannelist3Appearance, false);
            TextView txtViewBatch = (TextView) view.findViewById(R.id.txt_view_data);
            txtViewBatch.setText(Integer.toString(pannelist3AppearanceNos.get(j)));
            viewHolder.linearLayoutPannelist3Appearance.addView(view);
        }

//        Pannelist3 Taste
        ArrayList<Integer> pannelist3Taste = onlineSensories.get(i).pannelist3Data.taste;
        for (int j = 0; j < pannelist3Taste.size(); j++) {
            View view = mActivity.getLayoutInflater().inflate(R.layout.layout_blue_bg, viewHolder.linearLayoutPannelist3Taste, false);
            TextView txtViewBatch = (TextView) view.findViewById(R.id.txt_view_data);
            txtViewBatch.setText(Integer.toString(pannelist3Taste.get(j)));
            viewHolder.linearLayoutPannelist3Taste.addView(view);
        }

//        Pannelist3 Aroma
        ArrayList<Integer> pannelist3Aroma = onlineSensories.get(i).pannelist3Data.aroma;
        for (int j = 0; j < pannelist3Aroma.size(); j++) {
            View view = mActivity.getLayoutInflater().inflate(R.layout.layout_blue_bg, viewHolder.linearLayoutPannelist3Aroma, false);
            TextView txtViewBatch = (TextView) view.findViewById(R.id.txt_view_data);
            txtViewBatch.setText(Integer.toString(pannelist3Aroma.get(j)));
            viewHolder.linearLayoutPannelist3Aroma.addView(view);
        }
        viewHolder.txtViewSlNo.setText(Integer.toString(i + 1) + ".");

    }

    @Override
    public int getItemCount() {
        return onlineSensories.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_shift)
        TextView txtViewShift;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_sku)
        TextView txtViewSku;
        @BindView(R.id.img_view_panelist1_sign)
        ImageView imageViewPannelist1;
        @BindView(R.id.img_view_panelist2_sign)
        ImageView imageViewPannelist2;
        @BindView(R.id.img_view_panelist3_sign)
        ImageView imageViewPanelist3;
        @BindView(R.id.txt_view_panelist1_name)
        TextView txtViewPanelist1Name;
        @BindView(R.id.txt_view_panelist2_name)
        TextView txtViewPanelist2Name;
        @BindView(R.id.txt_view_panelist3_name)
        TextView txtViewPanelist3Name;
        @BindView(R.id.txt_view_module)
        TextView txtViewModule;
        @BindView(R.id.checkbox)
        CheckBox checkBox;
        @BindView(R.id.linear_layout_pannelist_1_appearance)
        LinearLayout linearLayoutPannelist1Appearance;
        @BindView(R.id.linear_layout_pannelist_1_aroma)
        LinearLayout linearLayoutPannelist1Aroma;
        @BindView(R.id.linear_layout_pannelist_1_taste)
        LinearLayout linearLayoutPannelist1Taste;
        @BindView(R.id.linear_layout_pannelist_2_appearance)
        LinearLayout linearLayoutPannelist2Appearance;
        @BindView(R.id.linear_layout_pannelist_2_aroma)
        LinearLayout linearLayoutPannelist2Aroma;
        @BindView(R.id.linear_layout_pannelist_2_taste)
        LinearLayout linearLayoutPannelist2Taste;
        @BindView(R.id.linear_layout_pannelist_3_appearance)
        LinearLayout linearLayoutPannelist3Appearance;
        @BindView(R.id.linear_layout_pannelist_3_aroma)
        LinearLayout linearLayoutPannelist3Aroma;
        @BindView(R.id.linear_layout_pannelist_3_taste)
        LinearLayout linearLayoutPannelist3Taste;
        @BindView(R.id.linear_layout_pannelist_1_batch)
        LinearLayout linearLayoutPannelist1Batch;
        @BindView(R.id.linear_layout_pannelist_2_batch)
        LinearLayout linearLayoutPannelist2Batch;
        @BindView(R.id.linear_layout_pannelist_3_batch)
        LinearLayout linearLayoutPannelist3Batch;
        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStorageDirectory(), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
