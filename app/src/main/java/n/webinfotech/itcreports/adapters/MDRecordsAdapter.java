package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.MDRecord;

/**
 * Created by Raj on 12-09-2019.
 */

public class MDRecordsAdapter extends RecyclerView.Adapter<MDRecordsAdapter.ViewHolder> {

    public interface Callback {
        void insertToArray(int id);
        void removeFromArray(int id);
    }

    Context mContext;
    ArrayList<MDRecord> mdRecords;
    Callback mCallback;

    public MDRecordsAdapter(Context mContext, ArrayList<MDRecord> mdRecords, Callback mCallback) {
        this.mContext = mContext;
        this.mdRecords = mdRecords;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_mdrecords, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewCheckedBy.setText(mdRecords.get(i).username);
        viewHolder.txtViewDate.setText(mdRecords.get(i).date);
        viewHolder.txtViewInterlock.setText(mdRecords.get(i).interlock);
        viewHolder.txtViewMDNo.setText(mdRecords.get(i).mdNo);
        viewHolder.txtViewProbeFe.setText(mdRecords.get(i).probeFe);
        viewHolder.txtViewProbeNfe.setText(mdRecords.get(i).probeNfe);
        viewHolder.txtViewProbeSS.setText(mdRecords.get(i).probeSS);
        viewHolder.txtViewRemarks.setText(mdRecords.get(i).remarks);
        viewHolder.txtViewShift.setText(mdRecords.get(i).shift);
        viewHolder.txtViewTime.setText(mdRecords.get(i).time);
        viewHolder.txtViewSensitivity.setText(mdRecords.get(i).sensitivity);
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.insertToArray(mdRecords.get(i).id);
                } else {
                    mCallback.removeFromArray(mdRecords.get(i).id);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mdRecords.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_shift)
        TextView txtViewShift;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_md_no)
        TextView txtViewMDNo;
        @BindView(R.id.txt_view_probe_fe)
        TextView txtViewProbeFe;
        @BindView(R.id.txt_view_probe_nfe)
        TextView txtViewProbeNfe;
        @BindView(R.id.txt_view_probe_ss)
        TextView txtViewProbeSS;
        @BindView(R.id.txt_view_interlock)
        TextView txtViewInterlock;
        @BindView(R.id.checkbox)
        CheckBox checkBox;
        @BindView(R.id.txt_view_checked_by)
        TextView txtViewCheckedBy;
        @BindView(R.id.txt_view_remarks)
        TextView txtViewRemarks;
        @BindView(R.id.txt_view_sensitivity)
        TextView txtViewSensitivity;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
