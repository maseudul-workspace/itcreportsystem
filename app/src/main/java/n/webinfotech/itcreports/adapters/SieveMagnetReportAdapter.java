package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.SieveMagnet;
import n.webinfotech.itcreports.util.DBHelper;

/**
 * Created by Raj on 19-09-2019.
 */

public class SieveMagnetReportAdapter extends RecyclerView.Adapter<SieveMagnetReportAdapter.ViewHolder> {

    public interface Callback {
        void onEditClicked(int id);
        void onDeleteClicked(int id);

    }

    Context mContext;
    ArrayList<SieveMagnet> sieveMagnets;
    DBHelper dbHelper;
    Callback mCallback;

    public SieveMagnetReportAdapter(Context mContext, ArrayList<SieveMagnet> sieveMagnets, Callback callback) {
        this.mContext = mContext;
        this.sieveMagnets = sieveMagnets;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_sieve_magnet_report_adapter, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        dbHelper = new DBHelper(mContext);
        viewHolder.txtViewCheckedBy.setText(sieveMagnets.get(i).username);
        viewHolder.txtViewDate.setText(sieveMagnets.get(i).date);
        viewHolder.txtViewMagnetCleaning.setText(sieveMagnets.get(i).magnetCleaning);
        viewHolder.txtViewRemarks.setText(sieveMagnets.get(i).remarks);
        viewHolder.txtViewShift.setText(sieveMagnets.get(i).shift);
        viewHolder.txtViewShifter.setText(sieveMagnets.get(i).shifter);
        viewHolder.txtViewSieveCondition.setText(sieveMagnets.get(i).sieveCondition);
        viewHolder.txtViewTime.setText(sieveMagnets.get(i).time);
        if (sieveMagnets.get(i).verifyStatus == 2) {
            viewHolder.txtViewVerifiedBy.setText(dbHelper.getVerifierName(sieveMagnets.get(i).verifyId));
        }

        viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(sieveMagnets.get(i).id);
            }
        });

        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirm dialog demo !");
                builder.setMessage("You are about to delete a record. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onDeleteClicked(sieveMagnets.get(i).id);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
            }
        });
        viewHolder.txtViewSlNo.setText(Integer.toString(i + 1));


    }

    @Override
    public int getItemCount() {
        return sieveMagnets.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_shift)
        TextView txtViewShift;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_shifter)
        TextView txtViewShifter;
        @BindView(R.id.txt_view_sieve_condition)
        TextView txtViewSieveCondition;
        @BindView(R.id.txt_view_magnet_cleaning)
        TextView txtViewMagnetCleaning;
        @BindView(R.id.txt_view_checked_by)
        TextView txtViewCheckedBy;
        @BindView(R.id.txt_view_remarks)
        TextView txtViewRemarks;
        @BindView(R.id.txt_view_verified_by)
        TextView txtViewVerifiedBy;
        @BindView(R.id.btn_edit)
        Button btnEdit;
        @BindView(R.id.btn_delete)
        Button btnDelete;
        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
