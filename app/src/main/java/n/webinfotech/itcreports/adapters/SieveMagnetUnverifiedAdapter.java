package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.SieveMagnet;

/**
 * Created by Raj on 16-09-2019.
 */

public class SieveMagnetUnverifiedAdapter extends RecyclerView.Adapter<SieveMagnetUnverifiedAdapter.ViewHolder> {

    public interface Callback {
        void insertToArray(int id);
        void removeFromArray(int id);
    }

    Context mContext;
    ArrayList<SieveMagnet> sieveMagnets;
    Callback mCallback;

    public SieveMagnetUnverifiedAdapter(Context mContext, ArrayList<SieveMagnet> sieveMagnets, Callback mCallback) {
        this.mContext = mContext;
        this.sieveMagnets = sieveMagnets;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_sieve_magnet_verification, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewCheckedBy.setText(sieveMagnets.get(i).username);
        viewHolder.txtViewDate.setText(sieveMagnets.get(i).date);
        viewHolder.txtViewMagnetCleaning.setText(sieveMagnets.get(i).magnetCleaning);
        viewHolder.txtViewRemarks.setText(sieveMagnets.get(i).remarks);
        viewHolder.txtViewShift.setText(sieveMagnets.get(i).shift);
        viewHolder.txtViewShifter.setText(sieveMagnets.get(i).shifter);
        viewHolder.txtViewSieveCondition.setText(sieveMagnets.get(i).sieveCondition);
        viewHolder.txtViewTime.setText(sieveMagnets.get(i).time);
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.insertToArray(sieveMagnets.get(i).id);
                } else {
                    mCallback.removeFromArray(sieveMagnets.get(i).id);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return sieveMagnets.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_shift)
        TextView txtViewShift;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_shifter)
        TextView txtViewShifter;
        @BindView(R.id.txt_view_sieve_condition)
        TextView txtViewSieveCondition;
        @BindView(R.id.txt_view_magnet_cleaning)
        TextView txtViewMagnetCleaning;
        @BindView(R.id.checkbox)
        CheckBox checkBox;
        @BindView(R.id.txt_view_checked_by)
        TextView txtViewCheckedBy;
        @BindView(R.id.txt_view_remarks)
        TextView txtViewRemarks;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
