package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.CFCChecking;
import n.webinfotech.itcreports.util.DBHelper;

/**
 * Created by Raj on 19-09-2019.
 */

public class CFCReportAdapter extends RecyclerView.Adapter<CFCReportAdapter.ViewHolder>{

    public interface Callback {
        void onEditClicked(int id);
        void onDeleteClicked(int id);
    }

    Context mContext;
    ArrayList<CFCChecking> cfcCheckings;
    Callback mCallback;
    DBHelper dbHelper;


    public CFCReportAdapter(Context mContext, ArrayList<CFCChecking> cfcCheckings, Callback callback) {
        this.mContext = mContext;
        this.cfcCheckings = cfcCheckings;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_cfc_report_adapter, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        dbHelper = new DBHelper(mContext);
        viewHolder.txtViewCodingCFC.setText(cfcCheckings.get(i).codingCFC);
        viewHolder.txtViewCodingMono.setText(cfcCheckings.get(i).codingMono);
        viewHolder.txtViewDate.setText(cfcCheckings.get(i).date);
        viewHolder.txtViewDent.setText(cfcCheckings.get(i).dent);
        viewHolder.txtViewFlap.setText(cfcCheckings.get(i).flap);
        viewHolder.txtViewMachineNo.setText(cfcCheckings.get(i).machineNo);
        viewHolder.txtViewQtyCFC.setText(cfcCheckings.get(i).qtyPresentCFC);
        viewHolder.txtViewQtyMono.setText(cfcCheckings.get(i).qtyPresentMono);
        viewHolder.txtViewShift.setText(cfcCheckings.get(i).shift);
        viewHolder.txtViewSku.setText(cfcCheckings.get(i).sku);
        viewHolder.txtViewTapping.setText(cfcCheckings.get(i).tapping);
        viewHolder.txtViewTime.setText(cfcCheckings.get(i).time);
        viewHolder.txtViewTorn.setText(cfcCheckings.get(i).torn);
        viewHolder.txtViewRemarks.setText(cfcCheckings.get(i).remarks);
        if (cfcCheckings.get(i).verifyStatus == 2) {
            viewHolder.txtViewVerifiedBy.setText(dbHelper.getVerifierName(cfcCheckings.get(i).verifyId));
        }
        viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(cfcCheckings.get(i).id);
            }
        });

        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirm dialog demo !");
                builder.setMessage("You are about to delete a record. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onDeleteClicked(cfcCheckings.get(i).id);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
            }
        });
        viewHolder.txtViewSlNo.setText(Integer.toString(i + 1));


    }

    @Override
    public int getItemCount() {
        return cfcCheckings.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_shift)
        TextView txtViewShift;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_machine_no)
        TextView txtViewMachineNo;
        @BindView(R.id.txt_view_sku)
        TextView txtViewSku;
        @BindView(R.id.txt_view_torn)
        TextView txtViewTorn;
        @BindView(R.id.txt_view_flap)
        TextView txtViewFlap;
        @BindView(R.id.txt_view_dent)
        TextView txtViewDent;
        @BindView(R.id.txt_view_coding_mono)
        TextView txtViewCodingMono;
        @BindView(R.id.txt_view_qty_mono)
        TextView txtViewQtyMono;
        @BindView(R.id.txt_view_qty_cfc)
        TextView txtViewQtyCFC;
        @BindView(R.id.txt_view_tapping)
        TextView txtViewTapping;
        @BindView(R.id.txt_view_coding_cfc)
        TextView txtViewCodingCFC;
        @BindView(R.id.txt_view_remarks)
        TextView txtViewRemarks;
        @BindView(R.id.txt_view_verified_by)
        TextView txtViewVerifiedBy;
        @BindView(R.id.btn_edit)
        Button btnEdit;
        @BindView(R.id.btn_delete)
        Button btnDelete;
        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
