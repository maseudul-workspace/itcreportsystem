package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.MDRecord;
import n.webinfotech.itcreports.util.DBHelper;

/**
 * Created by Raj on 19-09-2019.
 */

public class MDRecordsReportAdapter extends RecyclerView.Adapter<MDRecordsReportAdapter.ViewHolder> {

    public interface Callback {
        void onEditClicked(int id);
        void onDeleteClicked(int id);
    }

    Context mContext;
    ArrayList<MDRecord> mdRecords;
    DBHelper dbHelper;
    Callback mCallback;

    public MDRecordsReportAdapter(ArrayList<MDRecord> mdRecords, Context context, Callback callback) {
        this.mdRecords = mdRecords;
        this.mContext = context;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_md_record_report, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        dbHelper = new DBHelper(mContext);
        viewHolder.txtViewCheckedBy.setText(mdRecords.get(i).username);
        viewHolder.txtViewDate.setText(mdRecords.get(i).date);
        viewHolder.txtViewInterlock.setText(mdRecords.get(i).interlock);
        viewHolder.txtViewMDNo.setText(mdRecords.get(i).mdNo);
        viewHolder.txtViewProbeFe.setText(mdRecords.get(i).probeFe);
        viewHolder.txtViewProbeNfe.setText(mdRecords.get(i).probeNfe);
        viewHolder.txtViewProbeSS.setText(mdRecords.get(i).probeSS);
        viewHolder.txtViewRemarks.setText(mdRecords.get(i).remarks);
        viewHolder.txtViewShift.setText(mdRecords.get(i).shift);
        viewHolder.txtViewTime.setText(mdRecords.get(i).time);
        viewHolder.txtViewSensitivity.setText(mdRecords.get(i).sensitivity);
        if (mdRecords.get(i).verifyStatus == 2) {
            viewHolder.txtViewVerifiedBy.setText(dbHelper.getVerifierName(mdRecords.get(i).verifyId));
        }

        viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(mdRecords.get(i).id);
            }
        });

        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirm dialog demo !");
                builder.setMessage("You are about to delete a record. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onDeleteClicked(mdRecords.get(i).id);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
            }
        });
        viewHolder.txtViewSlNo.setText(Integer.toString(i + 1));


    }

    @Override
    public int getItemCount() {
        return mdRecords.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_shift)
        TextView txtViewShift;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_md_no)
        TextView txtViewMDNo;
        @BindView(R.id.txt_view_probe_fe)
        TextView txtViewProbeFe;
        @BindView(R.id.txt_view_probe_nfe)
        TextView txtViewProbeNfe;
        @BindView(R.id.txt_view_probe_ss)
        TextView txtViewProbeSS;
        @BindView(R.id.txt_view_interlock)
        TextView txtViewInterlock;
        @BindView(R.id.txt_view_checked_by)
        TextView txtViewCheckedBy;
        @BindView(R.id.txt_view_remarks)
        TextView txtViewRemarks;
        @BindView(R.id.txt_view_verified_by)
        TextView txtViewVerifiedBy;
        @BindView(R.id.txt_view_sensitivity)
        TextView txtViewSensitivity;
        @BindView(R.id.btn_edit)
        Button btnEdit;
        @BindView(R.id.btn_delete)
        Button btnDelete;
        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
