package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.CheckWeigher;

/**
 * Created by Raj on 16-09-2019.
 */

public class CheckWeigherVerifyAdapter extends RecyclerView.Adapter<CheckWeigherVerifyAdapter.ViewHolder> {

    public interface Callback {
        void insertToArray(int id);
        void removeFromArray(int id);
    }

    Context mContext;
    ArrayList<CheckWeigher> checkWeighers;
    Callback mCallback;

    public CheckWeigherVerifyAdapter(Context mContext, ArrayList<CheckWeigher> checkWeighers, Callback mCallback) {
        this.mContext = mContext;
        this.checkWeighers = checkWeighers;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_check_weigher_verify, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewShift.setText(checkWeighers.get(i).shift);
        viewHolder.txtViewSku.setText(checkWeighers.get(i).sku);
        viewHolder.txtViewCheckedBy.setText(checkWeighers.get(i).username);
        viewHolder.txtViewDate.setText(checkWeighers.get(i).date);
        viewHolder.txtViewMisCoding.setText(checkWeighers.get(i).misCoding);
        viewHolder.txtViewRejPkt.setText(checkWeighers.get(i).rejPkt);
        viewHolder.txtViewFlap.setText(checkWeighers.get(i).flapOpen);
        viewHolder.txtViewTime.setText(checkWeighers.get(i).time);
        viewHolder.txtViewLessCount.setText(checkWeighers.get(i).lessCount);
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.insertToArray(checkWeighers.get(i).id);
                } else {
                    mCallback.removeFromArray(checkWeighers.get(i).id);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return checkWeighers.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_shift)
        TextView txtViewShift;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_sku)
        TextView txtViewSku;
        @BindView(R.id.txt_view_rej_pkt)
        TextView txtViewRejPkt;
        @BindView(R.id.txt_view_flap)
        TextView txtViewFlap;
        @BindView(R.id.txt_view_mis_coding)
        TextView txtViewMisCoding;
        @BindView(R.id.txt_view_less_count)
        TextView txtViewLessCount;
        @BindView(R.id.checkbox)
        CheckBox checkBox;
        @BindView(R.id.txt_view_checked_by)
        TextView txtViewCheckedBy;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
