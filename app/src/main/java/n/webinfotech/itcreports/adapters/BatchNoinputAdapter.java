package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.BatchNo;

public class BatchNoinputAdapter extends RecyclerView.Adapter<BatchNoinputAdapter.ViewHolder> {

    public interface Callback {
        void onChecked(int number);
        void onUnchecked(int number);
    }

    Context mContext;
    ArrayList<BatchNo> batchNos;
    Callback mCallback;

    public BatchNoinputAdapter(Context mContext, ArrayList<BatchNo> batchNos, Callback callback) {
        this.mContext = mContext;
        this.batchNos = batchNos;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_batch_input_adapter, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewNumber.setText(Integer.toString(batchNos.get(i).batchNo));
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.onChecked(batchNos.get(i).batchNo);
                } else {
                    mCallback.onUnchecked(batchNos.get(i).batchNo);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return batchNos.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_number)
        TextView txtViewNumber;
        @BindView(R.id.checkbox)
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
