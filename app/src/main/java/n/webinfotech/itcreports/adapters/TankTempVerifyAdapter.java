package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.TankTemp;

/**
 * Created by Raj on 16-09-2019.
 */

public class TankTempVerifyAdapter extends RecyclerView.Adapter<TankTempVerifyAdapter.ViewHolder> {

    public interface Callback {
        void insertToArray(int id);
        void removeFromArray(int id);
    }

    Context mContext;
    ArrayList<TankTemp> tankTemps;
    Callback mCallback;

    public TankTempVerifyAdapter(Context mContext, ArrayList<TankTemp> tankTemps, Callback mCallback) {
        this.mContext = mContext;
        this.tankTemps = tankTemps;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_tank_temp_verify, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewShift.setText(tankTemps.get(i).shift);
        viewHolder.txtViewActTemp.setText(tankTemps.get(i).actTemp);
        viewHolder.txtViewCheckedBy.setText(tankTemps.get(i).username);
        viewHolder.txtViewDate.setText(tankTemps.get(i).date);
        viewHolder.txtViewRemarks.setText(tankTemps.get(i).remarks);
        viewHolder.txtViewSetTemp.setText(tankTemps.get(i).setTemp);
        viewHolder.txtViewTank.setText(tankTemps.get(i).tank);
        viewHolder.txtViewTime.setText(tankTemps.get(i).time);
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.insertToArray(tankTemps.get(i).id);
                } else {
                    mCallback.removeFromArray(tankTemps.get(i).id);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return tankTemps.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_shift)
        TextView txtViewShift;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_tank)
        TextView txtViewTank;
        @BindView(R.id.txt_view_act_temp)
        TextView txtViewActTemp;
        @BindView(R.id.txt_view_set_temp)
        TextView txtViewSetTemp;
        @BindView(R.id.checkbox)
        CheckBox checkBox;
        @BindView(R.id.txt_view_checked_by)
        TextView txtViewCheckedBy;
        @BindView(R.id.txt_view_remarks)
        TextView txtViewRemarks;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
