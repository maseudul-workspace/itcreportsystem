package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.CheckWeigher;
import n.webinfotech.itcreports.util.DBHelper;

/**
 * Created by Raj on 19-09-2019.
 */

public class CheckWeigherDataAdapter extends RecyclerView.Adapter<CheckWeigherDataAdapter.ViewHolder>{

    public interface Callback {
        void onEditClicked(int id);
        void onDeleteClicked(int id);
    }

    Context mContext;
    ArrayList<CheckWeigher> checkWeighers;
    DBHelper dbHelper;
    Callback mCallback;

    public CheckWeigherDataAdapter(Context mContext, ArrayList<CheckWeigher> checkWeighers, Callback callback) {
        this.mContext = mContext;
        this.checkWeighers = checkWeighers;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_check_weigher_report_data, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        dbHelper = new DBHelper(mContext);
        viewHolder.txtViewShift.setText(checkWeighers.get(i).shift);
        viewHolder.txtViewSku.setText(checkWeighers.get(i).sku);
        viewHolder.txtViewCheckedBy.setText(checkWeighers.get(i).username);
        viewHolder.txtViewDate.setText(checkWeighers.get(i).date);
        viewHolder.txtViewMisCoding.setText(checkWeighers.get(i).misCoding);
        viewHolder.txtViewRejPkt.setText(checkWeighers.get(i).rejPkt);
        viewHolder.txtViewFlap.setText(checkWeighers.get(i).flapOpen);
        viewHolder.txtViewTime.setText(checkWeighers.get(i).time);
        viewHolder.txtViewLessCount.setText(checkWeighers.get(i).lessCount);
        if (checkWeighers.get(i).verifyStatus == 2) {
            viewHolder.txtViewVerifiedBy.setText(dbHelper.getVerifierName(checkWeighers.get(i).verifyId));
        }
        viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(checkWeighers.get(i).id);
            }
        });

        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirm dialog demo !");
                builder.setMessage("You are about to delete a record. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onDeleteClicked(checkWeighers.get(i).id);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
            }
        });
        viewHolder.txtViewSlNo.setText(Integer.toString(i + 1));

    }

    @Override
    public int getItemCount() {
        return checkWeighers.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_shift)
        TextView txtViewShift;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_sku)
        TextView txtViewSku;
        @BindView(R.id.txt_view_rej_pkt)
        TextView txtViewRejPkt;
        @BindView(R.id.txt_view_flap)
        TextView txtViewFlap;
        @BindView(R.id.txt_view_mis_coding)
        TextView txtViewMisCoding;
        @BindView(R.id.txt_view_checked_by)
        TextView txtViewCheckedBy;
        @BindView(R.id.txt_view_verified_by)
        TextView txtViewVerifiedBy;
        @BindView(R.id.txt_view_less_count)
        TextView txtViewLessCount;
        @BindView(R.id.btn_edit)
        Button btnEdit;
        @BindView(R.id.btn_delete)
        Button btnDelete;
        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
