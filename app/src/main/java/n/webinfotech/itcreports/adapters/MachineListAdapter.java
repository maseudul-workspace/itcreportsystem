package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.Machines;

/**
 * Created by Raj on 09-09-2019.
 */

public class MachineListAdapter extends RecyclerView.Adapter<MachineListAdapter.ViewHolder> {

    public interface Callback {
        void onEditClicked(int id);
        void changeMachineActivation(int id, int status);
        void onShowQRcodeClicked(int id, String qrCode);
    }

    Context mContext;
    ArrayList<Machines> machines;
    Callback mCallback;

    public MachineListAdapter(Context mContext, ArrayList<Machines> machines, Callback callback) {
        this.mContext = mContext;
        this.machines = machines;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_machine_list, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewMachineName.setText(machines.get(i).machineName);
        viewHolder.txtViewMachineNo.setText(machines.get(i).machineNo);
        viewHolder.txtViewQrCode.setText(machines.get(i).qrCode);
        viewHolder.txtViewFormSheet.setText(machines.get(i).formSheet);

        if (machines.get(i).status == 1) {
            viewHolder.btnDeactivate.setVisibility(View.VISIBLE);
            viewHolder.btnActivate.setVisibility(View.GONE);
            viewHolder.txtViewStatus.setText("Activated");
            viewHolder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.green5));
        } else {
            viewHolder.btnDeactivate.setVisibility(View.GONE);
            viewHolder.btnActivate.setVisibility(View.VISIBLE);
            viewHolder.txtViewStatus.setText("Deactivated");
            viewHolder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.darkOrange1));
        }

        viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(machines.get(i).id);
            }
        });

        viewHolder.btnActivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.changeMachineActivation(machines.get(i).id, 1);
            }
        });

        viewHolder.btnDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.changeMachineActivation(machines.get(i).id, 2);
            }
        });

        viewHolder.btnQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onShowQRcodeClicked(machines.get(i).id, machines.get(i).qrCode);
            }
        });

    }

    @Override
    public int getItemCount() {
        return machines.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_machine_name)
        TextView txtViewMachineName;
        @BindView(R.id.txt_view_machine_no)
        TextView txtViewMachineNo;
        @BindView(R.id.txt_view_qrcode)
        TextView txtViewQrCode;
        @BindView(R.id.txt_view_form_sheet)
        TextView txtViewFormSheet;
        @BindView(R.id.btn_edit)
        Button btnEdit;
        @BindView(R.id.btn_activate)
        Button btnActivate;
        @BindView(R.id.btn_deactivate)
        Button btnDeactivate;
        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;
        @BindView(R.id.btn_qrcode)
        Button btnQRCode;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
