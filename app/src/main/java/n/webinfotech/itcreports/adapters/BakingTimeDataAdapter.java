package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.BakingTimeRecord;
import n.webinfotech.itcreports.util.DBHelper;

/**
 * Created by Raj on 18-09-2019.
 */

public class BakingTimeDataAdapter extends RecyclerView.Adapter<BakingTimeDataAdapter.ViewHolder> {

    public interface Callback {
        void onEditClicked(int id);
        void onDeleteClicked(int id);
    }

    Context mContext;
    ArrayList<BakingTimeRecord> bakingTimeRecords;
    DBHelper dbHelper;
    Callback mCallback;

    public BakingTimeDataAdapter(Context mContext, ArrayList<BakingTimeRecord> bakingTimeRecords, Callback callback) {
        this.mContext = mContext;
        this.bakingTimeRecords = bakingTimeRecords;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_baking_time_data, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        dbHelper = new DBHelper(mContext);
        viewHolder.txtViewShift.setText(bakingTimeRecords.get(i).shift);
        viewHolder.txtViewActualBT.setText(bakingTimeRecords.get(i).actualBt);
        viewHolder.txtViewBT.setText(bakingTimeRecords.get(i).btOnHmi);
        viewHolder.txtViewOvenNo.setText(bakingTimeRecords.get(i).ovenNo);
        viewHolder.txtViewSku.setText(bakingTimeRecords.get(i).sku);
        viewHolder.txtViewDate.setText(bakingTimeRecords.get(i).date);
        viewHolder.txtViewTime.setText(bakingTimeRecords.get(i).time);
        viewHolder.txtViewCheckedBy.setText(bakingTimeRecords.get(i).username);
        if (bakingTimeRecords.get(i).verifyStatus == 2) {
            viewHolder.txtViewVerifiedBy.setText(dbHelper.getVerifierName(bakingTimeRecords.get(i).verifyId));
        }
        viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(bakingTimeRecords.get(i).id);
            }
        });
        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirm dialog demo !");
                builder.setMessage("You are about to delete a record. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onDeleteClicked(bakingTimeRecords.get(i).id);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
            }
        });
        viewHolder.txtViewSlNo.setText(Integer.toString(i + 1));
    }

    @Override
    public int getItemCount() {
        return bakingTimeRecords.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_shift)
        TextView txtViewShift;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_oven_no)
        TextView txtViewOvenNo;
        @BindView(R.id.txt_view_sku)
        TextView txtViewSku;
        @BindView(R.id.txt_view_bt_on_hmi)
        TextView txtViewBT;
        @BindView(R.id.txt_view_actual_bt)
        TextView txtViewActualBT;
        @BindView(R.id.txt_view_checked_by)
        TextView txtViewCheckedBy;
        @BindView(R.id.txt_view_verified_by)
        TextView txtViewVerifiedBy;
        @BindView(R.id.btn_edit)
        Button btnEdit;
        @BindView(R.id.btn_delete)
        Button btnDelete;
        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
