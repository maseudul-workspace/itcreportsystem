package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.HourlyPQISheet;

/**
 * Created by Raj on 12-09-2019.
 */

public class HourlyPQISheetVerficationAdapter extends RecyclerView.Adapter<HourlyPQISheetVerficationAdapter.ViewHolder> {

    public interface Callback {
        void insertToArray(int id);
        void removeFromArray(int id);
    }

    Context mContext;
    ArrayList<HourlyPQISheet> hourlyPQISheets;
    Callback mCallback;

    public HourlyPQISheetVerficationAdapter(Context mContext, ArrayList<HourlyPQISheet> hourlyPQISheets, Callback mCallback) {
        this.mContext = mContext;
        this.hourlyPQISheets = hourlyPQISheets;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_hourly_pqi_sheet_verification, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewAroma.setText(hourlyPQISheets.get(i).aroma);
        viewHolder.txtViewBroken.setText(hourlyPQISheets.get(i).broken);
        viewHolder.txtViewCoding.setText(hourlyPQISheets.get(i).coding);
        viewHolder.txtViewColor.setText(hourlyPQISheets.get(i).color);
        viewHolder.txtViewDate.setText(hourlyPQISheets.get(i).date);
        viewHolder.txtViewForeignMatter.setText(hourlyPQISheets.get(i).foreignMatter);
        viewHolder.txtViewLoosePacket.setText(hourlyPQISheets.get(i).loosePckt);
        viewHolder.txtViewMachineNo.setText(hourlyPQISheets.get(i).machineNo);
        viewHolder.txtViewOtherDefects.setText(hourlyPQISheets.get(i).otherDefects);
        viewHolder.txtViewPktTime.setText(hourlyPQISheets.get(i).pktTime);
        viewHolder.txtViewRemarks.setText(hourlyPQISheets.get(i).remarks);
        viewHolder.txtViewSealingQuality.setText(hourlyPQISheets.get(i).sealingQuality);
        viewHolder.txtViewShift.setText(hourlyPQISheets.get(i).shift);
        viewHolder.txtViewTime.setText(hourlyPQISheets.get(i).time);
        viewHolder.txtViewUsername.setText(hourlyPQISheets.get(i).username);
        viewHolder.txtViewWrinkles.setText(hourlyPQISheets.get(i).wrinkles);
        viewHolder.txtViewSku.setText(hourlyPQISheets.get(i).sku);
        viewHolder.txtViewStack.setText(hourlyPQISheets.get(i).stack);
        viewHolder.txtViewDiameter1.setText(hourlyPQISheets.get(i).diameter1);
        viewHolder.txtViewDiameter2.setText(hourlyPQISheets.get(i).diameter2);
        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.insertToArray(hourlyPQISheets.get(i).id);
                } else {
                    mCallback.removeFromArray(hourlyPQISheets.get(i).id);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return hourlyPQISheets.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_shift)
        TextView txtViewShift;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_pkt_time)
        TextView txtViewPktTime;
        @BindView(R.id.txt_view_machine_no)
        TextView txtViewMachineNo;
        @BindView(R.id.txt_view_coding)
        TextView txtViewCoding;
        @BindView(R.id.txt_view_loose_packet)
        TextView txtViewLoosePacket;
        @BindView(R.id.txt_view_wrinkles)
        TextView txtViewWrinkles;
        @BindView(R.id.txt_view_sealing_quality)
        TextView txtViewSealingQuality;
        @BindView(R.id.txt_view_broken)
        TextView txtViewBroken;
        @BindView(R.id.txt_view_color)
        TextView txtViewColor;
        @BindView(R.id.txt_view_aroma)
        TextView txtViewAroma;
        @BindView(R.id.txt_view_foreign_matter)
        TextView txtViewForeignMatter;
        @BindView(R.id.txt_view_other_defects)
        TextView txtViewOtherDefects;
        @BindView(R.id.txt_view_checked_by)
        TextView txtViewUsername;
        @BindView(R.id.txt_view_remarks)
        TextView txtViewRemarks;
        @BindView(R.id.checkbox)
        CheckBox checkBox;
        @BindView(R.id.txt_view_sku)
        TextView txtViewSku;
        @BindView(R.id.txt_view_stack)
        TextView txtViewStack;
        @BindView(R.id.txt_view_diameter_1)
        TextView txtViewDiameter1;
        @BindView(R.id.txt_view_diameter_2)
        TextView txtViewDiameter2;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
