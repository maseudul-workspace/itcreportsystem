package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.HourlyPQISheet;
import n.webinfotech.itcreports.util.DBHelper;

/**
 * Created by Raj on 19-09-2019.
 */

public class HourlyPQISheetDataAdapter extends RecyclerView.Adapter<HourlyPQISheetDataAdapter.ViewHolder> {

    public interface Callback {
        void onEditClicked(int id);
        void onDeleteClicked(int id);
    }

    Context mContext;
    ArrayList<HourlyPQISheet> hourlyPQISheets;
    DBHelper dbHelper;
    Callback mCallback;

    public HourlyPQISheetDataAdapter(Context mContext, ArrayList<HourlyPQISheet> hourlyPQISheets, Callback callback) {
        this.mContext = mContext;
        this.hourlyPQISheets = hourlyPQISheets;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_hourly_pqi_report, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        dbHelper = new DBHelper(mContext);
        viewHolder.txtViewAroma.setText(hourlyPQISheets.get(i).aroma);
        viewHolder.txtViewBroken.setText(hourlyPQISheets.get(i).broken);
        viewHolder.txtViewCoding.setText(hourlyPQISheets.get(i).coding);
        viewHolder.txtViewColor.setText(hourlyPQISheets.get(i).color);
        viewHolder.txtViewDate.setText(hourlyPQISheets.get(i).date);
        viewHolder.txtViewForeignMatter.setText(hourlyPQISheets.get(i).foreignMatter);
        viewHolder.txtViewLoosePacket.setText(hourlyPQISheets.get(i).loosePckt);
        viewHolder.txtViewMachineNo.setText(hourlyPQISheets.get(i).machineNo);
        viewHolder.txtViewOtherDefects.setText(hourlyPQISheets.get(i).otherDefects);
        viewHolder.txtViewPktTime.setText(hourlyPQISheets.get(i).pktTime);
        viewHolder.txtViewRemarks.setText(hourlyPQISheets.get(i).remarks);
        viewHolder.txtViewSealingQuality.setText(hourlyPQISheets.get(i).sealingQuality);
        viewHolder.txtViewShift.setText(hourlyPQISheets.get(i).shift);
        viewHolder.txtViewTime.setText(hourlyPQISheets.get(i).time);
        viewHolder.txtViewUsername.setText(hourlyPQISheets.get(i).username);
        viewHolder.txtViewWrinkles.setText(hourlyPQISheets.get(i).wrinkles);
        viewHolder.txtViewSku.setText(hourlyPQISheets.get(i).sku);
        viewHolder.txtViewStack.setText(hourlyPQISheets.get(i).stack);
        viewHolder.txtViewDiameter1.setText(hourlyPQISheets.get(i).diameter1);
        viewHolder.txtViewDiameter2.setText(hourlyPQISheets.get(i).diameter2);
        if (hourlyPQISheets.get(i).verifyStatus == 2) {
            viewHolder.txtViewVerifiedBy.setText(dbHelper.getVerifierName(hourlyPQISheets.get(i).verifyId));
        }

        viewHolder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(hourlyPQISheets.get(i).id);
            }
        });

        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirm dialog demo !");
                builder.setMessage("You are about to delete a record. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onDeleteClicked(hourlyPQISheets.get(i).id);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
            }
        });
        viewHolder.txtViewSlNo.setText(Integer.toString(i + 1));


    }

    @Override
    public int getItemCount() {
        return hourlyPQISheets.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_shift)
        TextView txtViewShift;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_pkt_time)
        TextView txtViewPktTime;
        @BindView(R.id.txt_view_machine_no)
        TextView txtViewMachineNo;
        @BindView(R.id.txt_view_coding)
        TextView txtViewCoding;
        @BindView(R.id.txt_view_loose_packet)
        TextView txtViewLoosePacket;
        @BindView(R.id.txt_view_wrinkles)
        TextView txtViewWrinkles;
        @BindView(R.id.txt_view_sealing_quality)
        TextView txtViewSealingQuality;
        @BindView(R.id.txt_view_broken)
        TextView txtViewBroken;
        @BindView(R.id.txt_view_color)
        TextView txtViewColor;
        @BindView(R.id.txt_view_aroma)
        TextView txtViewAroma;
        @BindView(R.id.txt_view_foreign_matter)
        TextView txtViewForeignMatter;
        @BindView(R.id.txt_view_other_defects)
        TextView txtViewOtherDefects;
        @BindView(R.id.txt_view_checked_by)
        TextView txtViewUsername;
        @BindView(R.id.txt_view_remarks)
        TextView txtViewRemarks;
        @BindView(R.id.txt_view_verified_by)
        TextView txtViewVerifiedBy;
        @BindView(R.id.txt_view_sku)
        TextView txtViewSku;
        @BindView(R.id.txt_view_stack)
        TextView txtViewStack;
        @BindView(R.id.txt_view_diameter_1)
        TextView txtViewDiameter1;
        @BindView(R.id.txt_view_diameter_2)
        TextView txtViewDiameter2;
        @BindView(R.id.btn_edit)
        Button btnEdit;
        @BindView(R.id.btn_delete)
        Button btnDelete;
        @BindView(R.id.txt_view_sl_no)
        TextView txtViewSlNo;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
