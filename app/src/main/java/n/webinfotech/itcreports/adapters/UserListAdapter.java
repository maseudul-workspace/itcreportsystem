package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.UserList;

/**
 * Created by Raj on 07-09-2019.
 */

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder>{

    public interface Callback {
        void onEditClicked(int id);
        void changeUserActivation(int id, int status);
    }

    Context mContext;
    ArrayList<UserList> userLists;
    Callback mCallback;

    public UserListAdapter(Context mContext, ArrayList<UserList> userLists, Callback mCallback) {
        this.mContext = mContext;
        this.userLists = userLists;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_user_list, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewUsername.setText(userLists.get(i).userName);
        viewHolder.txtViewName.setText(userLists.get(i).name);
        viewHolder.txtViewEmail.setText(userLists.get(i).email);
        viewHolder.txtViewGender.setText(userLists.get(i).gender);
        viewHolder.txtViewPhone.setText(userLists.get(i).phone);
        viewHolder.txtViewPassword.setText(userLists.get(i).password);
        Log.e("LogMsg", "Status: " + userLists.get(i).status);
        if (userLists.get(i).status == 1) {
            viewHolder.btnUserDeactivate.setVisibility(View.VISIBLE);
            viewHolder.btnUserActivate.setVisibility(View.GONE);
            viewHolder.txtViewStatus.setText("Activated");
            viewHolder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.green5));
        } else {
            viewHolder.btnUserDeactivate.setVisibility(View.GONE);
            viewHolder.btnUserActivate.setVisibility(View.VISIBLE);
            viewHolder.txtViewStatus.setText("Deactivated");
            viewHolder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.darkOrange1));
        }
        if (userLists.get(i).userType == 2) {
            viewHolder.txtViewUserType.setText("Employee");
        } else {
            viewHolder.txtViewUserType.setText("Verifier");
        }
        viewHolder.btnUserEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(userLists.get(i).id);
            }
        });
        viewHolder.btnUserActivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.changeUserActivation(userLists.get(i).id, 1);
            }
        });
        viewHolder.btnUserDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.changeUserActivation(userLists.get(i).id, 2);
            }
        });
    }

    @Override
    public int getItemCount() {
        return userLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_username)
        TextView txtViewUsername;
        @BindView(R.id.txt_view_name)
        TextView txtViewName;
        @BindView(R.id.txt_view_email)
        TextView txtViewEmail;
        @BindView(R.id.txt_view_gender)
        TextView txtViewGender;
        @BindView(R.id.txt_view_phone)
        TextView txtViewPhone;
        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;
        @BindView(R.id.btn_user_activate)
        Button btnUserActivate;
        @BindView(R.id.btn_user_deactivate)
        Button btnUserDeactivate;
        @BindView(R.id.btn_user_edit)
        Button btnUserEdit;
        @BindView(R.id.txt_view_password)
        TextView txtViewPassword;
        @BindView(R.id.txt_view_usertype)
        TextView txtViewUserType;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
