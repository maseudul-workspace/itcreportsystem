package n.webinfotech.itcreports.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.BakingTimeRecord;

/**
 * Created by Raj on 10-09-2019.
 */

public class BakingTimeRecordVerficationAdapter extends RecyclerView.Adapter<BakingTimeRecordVerficationAdapter.ViewHolder> {

    public interface Callback {
        void insertToArray(int id);
        void removeFromArray(int id);
    }

    Context mContext;
    ArrayList<BakingTimeRecord> bakingTimeRecords;
    Callback mCallback;

    public BakingTimeRecordVerficationAdapter(Context mContext, ArrayList<BakingTimeRecord> bakingTimeRecords, Callback callback) {
        this.mContext = mContext;
        this.bakingTimeRecords = bakingTimeRecords;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_backing_time_records, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewShift.setText(bakingTimeRecords.get(i).shift);
        viewHolder.txtViewActualBT.setText(bakingTimeRecords.get(i).actualBt);
        viewHolder.txtViewBT.setText(bakingTimeRecords.get(i).btOnHmi);
        viewHolder.txtViewOvenNo.setText(bakingTimeRecords.get(i).ovenNo);
        viewHolder.txtViewSku.setText(bakingTimeRecords.get(i).sku);
        viewHolder.txtViewDate.setText(bakingTimeRecords.get(i).date);
        viewHolder.txtViewTime.setText(bakingTimeRecords.get(i).time);
        viewHolder.txtViewCheckedBy.setText(bakingTimeRecords.get(i).username);

        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mCallback.insertToArray(bakingTimeRecords.get(i).id);
                } else {
                    mCallback.removeFromArray(bakingTimeRecords.get(i).id);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return bakingTimeRecords.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_shift)
        TextView txtViewShift;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_oven_no)
        TextView txtViewOvenNo;
        @BindView(R.id.txt_view_sku)
        TextView txtViewSku;
        @BindView(R.id.txt_view_bt_on_hmi)
        TextView txtViewBT;
        @BindView(R.id.txt_view_actual_bt)
        TextView txtViewActualBT;
        @BindView(R.id.txt_view_checked_by)
        TextView txtViewCheckedBy;
        @BindView(R.id.checkbox)
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
