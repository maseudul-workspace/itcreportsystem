package n.webinfotech.itcreports;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import n.webinfotech.itcreports.models.User;

/**
 * Created by Raj on 07-09-2019.
 */

public class AndroidApplication extends Application {

    User user;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setUser(Context context, User user) {
        this.user = user;
        SharedPreferences sharedPref = context.getSharedPreferences(
                "ITC", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(user != null) {
            editor.putInt("USER_ID", user.id);
            editor.putInt("USER_TYPE", user.type);
            editor.putString("USER_NAME", user.name);
            editor.putString("USER_SIGN", user.signature);
        } else {
            editor.putInt("USER_ID", 0);
            editor.putInt("USER_TYPE", 0);
            editor.putString("USER_NAME", "");
            editor.putString("USER_SIGN", "");
        }

        editor.commit();
    }

    public User getUser(Context context) {
        User userInfo;
        if(this.user != null) {
            userInfo = this.user;
        } else {
            SharedPreferences sharedPref = context.getSharedPreferences(
                    "ITC", Context.MODE_PRIVATE);
            int userId = sharedPref.getInt("USER_ID", 0);
            int userType = sharedPref.getInt("USER_TYPE", 0);
            String userName = sharedPref.getString("USER_NAME", "");
            String signature = sharedPref.getString("USER_SIGN", "");
            if(userId == 0 && userType == 0) {
                userInfo = null;
            } else {
                userInfo = new User(userId, userType, userName, signature);
                this.user = userInfo;
            }
        }
        return userInfo;
    }

}
