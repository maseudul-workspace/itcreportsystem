package n.webinfotech.itcreports.dialogs;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;
import n.webinfotech.itcreports.R;

public class SelectModuleDialog {

    public interface Callback {
        void onModuleSelected(int moduleId);
    }

    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Context mContext;
    Callback mCallback;

    public SelectModuleDialog(Activity mActivity, Context mContext, Callback mCallback) {
        this.mActivity = mActivity;
        this.mContext = mContext;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_select_module, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    @OnClick(R.id.btn_module_1) void onModule1Clicked() {
        mCallback.onModuleSelected(1);
    }

    @OnClick(R.id.btn_module_2) void onModule2Clicked() {
        mCallback.onModuleSelected(2);
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

}
