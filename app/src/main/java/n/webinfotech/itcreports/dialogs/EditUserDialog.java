package n.webinfotech.itcreports.dialogs;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import butterknife.BindView;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.R;

/**
 * Created by Raj on 09-09-2019.
 */

public class EditUserDialog {

    public interface Callback {
        void updateUser(String name, String password, String email, String phone, String gender, int id);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    EditText editTextUsername;
    EditText editTextPassword;
    EditText editTextName;
    EditText editTextEmail;
    EditText editTextPhone;
    EditText editTextRepeatPassword;
    Button btnSubmit;
    RadioGroup radioGroupGender;
    String gender;
    RadioButton radioButtonMale;
    RadioButton radioButtonFemale;
    RadioGroup radioGroupUserType;
    int id;

    public EditUserDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.activity_add_user, null);
        editTextUsername = (EditText) dialogContainer.findViewById(R.id.edit_text_username);
        radioGroupUserType = (RadioGroup) dialogContainer.findViewById(R.id.radio_group_user_type);
        radioGroupUserType.setVisibility(View.GONE);
        editTextUsername.setVisibility(View.GONE);
        editTextPassword = (EditText) dialogContainer.findViewById(R.id.edit_text_password);
        editTextName = (EditText) dialogContainer.findViewById(R.id.edit_text_name);
        editTextEmail = (EditText) dialogContainer.findViewById(R.id.edit_text_email);
        editTextPhone = (EditText) dialogContainer.findViewById(R.id.edit_text_phone);
        editTextRepeatPassword = (EditText) dialogContainer.findViewById(R.id.edit_text_repeat_password);
        radioGroupGender = (RadioGroup) dialogContainer.findViewById(R.id.radio_group_gender);
        radioButtonFemale = (RadioButton) dialogContainer.findViewById(R.id.radio_btn_female);
        radioButtonMale = (RadioButton) dialogContainer.findViewById(R.id.radio_btn_male);
        btnSubmit = (Button) dialogContainer.findViewById(R.id.btn_submit);

        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                gender = checkedRadioButton.getText().toString();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextName.getText().toString().trim().isEmpty() || editTextPassword.getText().toString().trim().isEmpty() || editTextRepeatPassword.getText().toString().trim().isEmpty()) {
                    Toasty.error(mContext, "Fields missing", Toast.LENGTH_SHORT, true).show();
                } else if (!editTextPassword.getText().toString().equals(editTextRepeatPassword.getText().toString())) {
                    Toasty.error(mContext, "Password Mismatch", Toast.LENGTH_SHORT, true).show();
                } else {
                    mCallback.updateUser(editTextName.getText().toString(),
                            editTextPassword.getText().toString(),
                            editTextEmail.getText().toString(),
                            editTextPhone.getText().toString(),
                            gender,
                            id);
                }
            }
        });

        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    public void setUpEditTexts(String name, String password, String email, String phone, String gender, int id) {
        editTextName.setText(name);
        editTextPhone.setText(phone);
        editTextEmail.setText(email);
        editTextPassword.setText(password);
        editTextRepeatPassword.setText(password);
        this.gender = gender;

        if(gender.equals("Male")) {
            radioButtonMale.setChecked(true);
            radioButtonFemale.setChecked(false);
        } else {
            radioButtonFemale.setChecked(true);
            radioButtonMale.setChecked(false);
        }
        this.id = id;
    }

    public void showDialog(){
        dialog.show();
    }

    public void hideDialog(){
        dialog.dismiss();
    }

}
