package n.webinfotech.itcreports.dialogs;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.R;

public class StartUpCheckListDataDialog {

    @BindView(R.id.txt_view_status)
    TextView txtViewStatus;
    @BindView(R.id.txt_view_remarks)
    TextView txtViewRemarks;
    @BindView(R.id.txt_view_action_taken)
    TextView txtViewActionTaken;
    @BindView(R.id.txt_view_final_status)
    TextView txtViewFinalStatus;
    @BindView(R.id.txt_view_title)
    TextView txtViewTitle;

    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Context mContext;

    public StartUpCheckListDataDialog(Activity mActivity, Context mContext) {
        this.mActivity = mActivity;
        this.mContext = mContext;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_check_list_data, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setData(Cursor c, String title) {
        c.moveToNext();
        txtViewStatus.setText(c.getString(c.getColumnIndex("status")));
        txtViewRemarks.setText(c.getString(c.getColumnIndex("remarks")));
        txtViewActionTaken.setText(c.getString(c.getColumnIndex("action_taken")));
        txtViewFinalStatus.setText(c.getString(c.getColumnIndex("final_status")));
        txtViewTitle.setText(title);
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

}
