package n.webinfotech.itcreports.dialogs;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;

public class HourlyPQIEditDialog {

    public interface Callback {
        void onUpdateSucces();
    }

    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    int id;
    Context mContext;
    Callback mCallback;

    @BindView(R.id.spinner_shifts)
    Spinner spinnerShifts;
    @BindView(R.id.txt_view_time)
    TextView txtViewTime;
    @BindView(R.id.toggle_btn_coding)
    ToggleButton toggleButtonCoding;
    @BindView(R.id.edit_text_wrinkles)
    EditText editTextWrinkles;
    @BindView(R.id.toggle_btn_sealing_quality)
    ToggleButton toggleButtonSealingQyality;
    @BindView(R.id.edit_text_broken)
    EditText editTextBroken;
    @BindView(R.id.toggle_btn_color)
    ToggleButton toggleButtonColor;
    @BindView(R.id.toggle_btn_aroma)
    ToggleButton toggleButtonAroma;
    @BindView(R.id.toggle_btn_foreign_matter)
    ToggleButton toggleButtonForeignMatter;
    @BindView(R.id.toggle_other_defects)
    ToggleButton toggleButtonOtherDefects;
    @BindView(R.id.edit_text_remarks)
    EditText editTextRemarks;
    @BindView(R.id.spinner_machines)
    Spinner spinnerMachines;
    String time;
    String shift;
    boolean isTimeSelected = false;
    String[] shiftList = {
            "Select Shift",
            "A",
            "B",
            "C"
    };
    String[] machines = {
            "Select Module",
            "Module - 1",
            "Module - 2"
    };
    DBHelper dbHelper;
    @BindView(R.id.edit_text_loose_packet)
    EditText editTextLoosePacket;
    String sku;
    @BindView(R.id.radio_group_sku)
    RadioGroup radioGroupSKU;
    @BindView(R.id.edit_text_stack)
    EditText editTextStack;
    @BindView(R.id.edit_text_diameter_1)
    EditText editTextDiameter1;
    @BindView(R.id.edit_text_diameter_2)
    EditText editTextDiameter2;
    String machineName;
    @BindView(R.id.radio_btn_sku_75)
    RadioButton radioButtonSKU75;
    @BindView(R.id.radio_btn_sku_20)
    RadioButton radioButtonSKU20;

    public HourlyPQIEditDialog(Activity mActivity, Context mContext, Callback mCallback) {
        this.mActivity = mActivity;
        this.mContext = mContext;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.activity_hourly_pqisheet, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
        setSpinnerShifts();
        initializeDBHelper();
        setSpinnerMachines();
        setRadioGroupSKU();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(mContext);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @OnClick(R.id.img_view_clock) void onClockClicked() {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog timePickerDialog = new TimePickerDialog(mContext,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        calendar.set(Calendar.MINUTE,minute);
                        isTimeSelected = true;
                        time = new SimpleDateFormat("hh:mm a").format(calendar.getTime());
                        txtViewTime.setText(time);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    public void setSpinnerShifts() {
        final ArrayAdapter<String> shiftListAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, shiftList){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;            }
        };

        shiftListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerShifts.setAdapter(shiftListAdapter);
        spinnerShifts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                shift = shiftList[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setSpinnerMachines() {
        final ArrayAdapter<String> machineListAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, machines){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;            }
        };

        machineListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMachines.setAdapter(machineListAdapter);
        spinnerMachines.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                machineName = machines[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setRadioGroupSKU() {
        radioGroupSKU.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                sku = checkedRadioButton.getText().toString();
            }
        });

    }

    public void showDialog() {
        dialog.show();
    }

    public void setData(Cursor c) {
        c.moveToNext();
        id = c.getInt(c.getColumnIndex("id"));
        shift = c.getString(c.getColumnIndex("shift"));
        isTimeSelected = true;
        txtViewTime.setText(c.getString(c.getColumnIndex("pkd_time")));
        time = c.getString(c.getColumnIndex("pkd_time"));

        editTextBroken.setText(c.getString(c.getColumnIndex("broken_chipped")));
        editTextDiameter1.setText(c.getString(c.getColumnIndex("diameter1")));
        editTextDiameter2.setText(c.getString(c.getColumnIndex("diameter2")));
        editTextLoosePacket.setText(c.getString(c.getColumnIndex("loose_pkd")));
        editTextRemarks.setText(c.getString(c.getColumnIndex("remarks")));
        editTextStack.setText(c.getString(c.getColumnIndex("stack")));
        editTextWrinkles.setText(c.getString(c.getColumnIndex("wrinkles")));

        sku = c.getString(c.getColumnIndex("sku"));

        if (sku.contains("75 gm")) {
            radioButtonSKU75.setChecked(true);
        } else {
            radioButtonSKU20.setChecked(true);
        }

        if (c.getString(c.getColumnIndex("coding")).equals("Ok")) {
            toggleButtonCoding.setChecked(true);
        } else {
            toggleButtonCoding.setChecked(false);
        }

        if (c.getString(c.getColumnIndex("sealing_quality")).equals("Ok")) {
            toggleButtonSealingQyality.setChecked(true);
        } else {
            toggleButtonSealingQyality.setChecked(false);
        }

        if (c.getString(c.getColumnIndex("color")).equals("Ok")) {
            toggleButtonColor.setChecked(true);
        } else {
            toggleButtonColor.setChecked(false);
        }

        if (c.getString(c.getColumnIndex("aroma")).equals("Ok")) {
            toggleButtonAroma.setChecked(true);
        } else {
            toggleButtonAroma.setChecked(false);
        }

        if (c.getString(c.getColumnIndex("foreign_matter")).equals("Present")) {
            toggleButtonForeignMatter.setChecked(true);
        } else {
            toggleButtonForeignMatter.setChecked(false);
        }

        if (c.getString(c.getColumnIndex("other_defects")).equals("Yes")) {
            toggleButtonOtherDefects.setChecked(true);
        } else {
            toggleButtonOtherDefects.setChecked(false);
        }
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (!isTimeSelected ||
                shift == null ||
                machineName == null ||
                sku == null ||
                editTextBroken.getText().toString().trim().isEmpty() ||
                editTextWrinkles.getText().toString().trim().isEmpty() ||
                editTextLoosePacket.getText().toString().trim().isEmpty() ||
                editTextStack.getText().toString().trim().isEmpty() ||
                editTextDiameter1.getText().toString().trim().isEmpty() ||
                editTextDiameter2.getText().toString().trim().isEmpty()
        ) {
            Toasty.warning(mContext, "Fields missing", Toast.LENGTH_SHORT, true).show();
        } else {
            if (dbHelper.updateHourlyPQISheet(
                    shift,
                    time,
                    sku,
                    toggleButtonCoding.getText().toString(),
                    editTextLoosePacket.getText().toString(),
                    editTextWrinkles.getText().toString(),
                    editTextStack.getText().toString(),
                    editTextDiameter1.getText().toString(),
                    editTextDiameter2.getText().toString(),
                    toggleButtonSealingQyality.getText().toString(),
                    editTextBroken.getText().toString(),
                    toggleButtonColor.getText().toString(),
                    toggleButtonAroma.getText().toString(),
                    toggleButtonForeignMatter.getText().toString(),
                    toggleButtonOtherDefects.getText().toString(),
                    editTextRemarks.getText().toString(),
                    id
            )) {
                Toasty.success(mContext, "Successfully updated", Toast.LENGTH_LONG, true).show();
                dialog.dismiss();
                mCallback.onUpdateSucces();
            } else {
                Toasty.error(mContext, "Something went wrong", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

}
