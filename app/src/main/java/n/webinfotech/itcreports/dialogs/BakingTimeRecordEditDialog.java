package n.webinfotech.itcreports.dialogs;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;

public class BakingTimeRecordEditDialog {

    @BindView(R.id.spinner_shifts)
    Spinner spinnerShifts;
    @BindView(R.id.radio_group_sku)
    RadioGroup radioGroupSKU;
    @BindView(R.id.txt_view_machine_no)
    TextView txtViewMachine;
    String machine;
    String shift;
    String sku;
    String[] shiftList = {
            "Select Shift",
            "A",
            "B",
            "C"
    };
    DBHelper dbHelper;
    AndroidApplication androidApplication;
    @BindView(R.id.edit_text_hmi_hour)
    EditText editTextHMIhour;
    @BindView(R.id.edit_text_hmi_min)
    EditText editTextHMImin;
    @BindView(R.id.edit_text_hmi_sec)
    EditText editTextHMIsec;
    @BindView(R.id.edit_text_bt_hour)
    EditText editTextBThour;
    @BindView(R.id.edit_text_bt_min)
    EditText editTextBTmin;
    @BindView(R.id.edit_text_bt_sec)
    EditText editTextBTsec;
    @BindView(R.id.radio_group_time_hmi)
    RadioGroup radioGroupTimeHMI;
    @BindView(R.id.radio_group_time_bt)
    RadioGroup radioGroupTimeBT;
    @BindView(R.id.radio_btn_sku_75)
    RadioButton radioButtonSKU75;
    @BindView(R.id.radio_btn_sku_20)
    RadioButton radioButtonSKU20;
    @BindView(R.id.radio_btn_bt_am)
    RadioButton radioButtonBTam;
    @BindView(R.id.radio_btn_bt_pm)
    RadioButton radioButtonBTpm;
    @BindView(R.id.radio_btn_hmi_am)
    RadioButton radioButtonHMIam;
    @BindView(R.id.radio_btn_hmi_pm)
    RadioButton radioButtonHMIpm;
    String actualBT;
    String btOnHMI;
    String btTime;
    String hmiTime;
    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    int id;
    Callback mCallback;

    public interface Callback {
        void onUpdateSucces();
    }

    public BakingTimeRecordEditDialog(Context mContext, Activity mActivity, Callback callback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        mCallback = callback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.activity_baking_time_record_form, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
        setUpFields();
        initializeDBHelper();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(mContext);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setUpFields() {

        radioGroupSKU.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                sku = checkedRadioButton.getText().toString();
            }
        });

        radioGroupTimeBT.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                btTime = checkedRadioButton.getText().toString();
            }
        });

        radioGroupTimeHMI.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                hmiTime = checkedRadioButton.getText().toString();
            }
        });

        final ArrayAdapter<String> shiftListAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, shiftList){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;            }
        };

        shiftListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerShifts.setAdapter(shiftListAdapter);
        spinnerShifts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                shift = shiftList[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void showDialog() {
        dialog.show();
    }

    public void setData(Cursor c) {
        c.moveToNext();
        id =  c.getInt(c.getColumnIndex("id"));
        shift = c.getString(c.getColumnIndex("shift"));
        sku = c.getString(c.getColumnIndex("sku"));
        btOnHMI = c.getString(c.getColumnIndex("bt_on_hmi"));
        actualBT = c.getString(c.getColumnIndex("actual_bt"));
        if (sku.contains("75 gm")) {
            radioButtonSKU75.setChecked(true);
        } else {
            radioButtonSKU20.setChecked(true);
        }
        String[] timeDiffHMI = btOnHMI.split(" ");

        if (timeDiffHMI[1].contains("am")) {
            radioButtonHMIam.setChecked(true);
            radioButtonHMIpm.setChecked(false);
        } else {
            radioButtonHMIpm.setChecked(true);
            radioButtonHMIam.setChecked(false);
        }

        String[] timeDiffBT = actualBT.split(" ");

        if (timeDiffBT[1].contains("am")) {
            radioButtonBTam.setChecked(true);
            radioButtonBTpm.setChecked(false);
        } else {
            radioButtonBTpm.setChecked(true);
            radioButtonBTam.setChecked(false);
        }

        String[] timeHMI = timeDiffHMI[0].split(":");
        String[] timeBT = timeDiffBT[0].split(":");

        editTextHMIhour.setText(timeHMI[0]);
        editTextHMImin.setText(timeHMI[1]);
        editTextHMIsec.setText(timeHMI[2]);

        editTextBThour.setText(timeBT[0]);
        editTextBTmin.setText(timeBT[1]);
        editTextBTsec.setText(timeBT[2]);

        txtViewMachine.setText(c.getString(c.getColumnIndex("oven_no")));

    }

    @OnClick(R.id.btn_submit) void onSubmitBtnClicked() {
        if (shift == null ||
                sku == null ||
                editTextBThour.getText().toString().trim().isEmpty() ||
                editTextBTmin.getText().toString().trim().isEmpty() ||
                editTextBTsec.getText().toString().trim().isEmpty() ||
                editTextHMIhour.getText().toString().trim().isEmpty() ||
                editTextHMImin.getText().toString().trim().isEmpty() ||
                editTextHMIsec.getText().toString().trim().isEmpty() ||
                btTime == null ||
                hmiTime == null
        ) {
            Toasty.warning(mContext, "Fields missing", Toast.LENGTH_SHORT, true).show();
        } else {
            actualBT = editTextBThour.getText().toString() + ":" + editTextBTmin.getText().toString() + ":" + editTextBTsec.getText().toString() + " " + btTime;
            btOnHMI = editTextHMIhour.getText().toString() + ":" + editTextHMImin.getText().toString() + ":" + editTextHMIsec.getText().toString() + " " + hmiTime;
            if (dbHelper.updateBakingTimeRecord(
                    shift,
                    sku,
                    btOnHMI,
                    actualBT,
                    id
            )) {
                Toasty.success(mContext, "Data updated successfully", Toast.LENGTH_LONG, true).show();
                dialog.dismiss();
                mCallback.onUpdateSucces();
            } else {
                Toasty.error(mContext, "Failed to insert data", Toast.LENGTH_SHORT, true).show();
            }
        }
    }



}
