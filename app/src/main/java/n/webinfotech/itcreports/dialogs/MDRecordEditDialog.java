package n.webinfotech.itcreports.dialogs;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;

public class MDRecordEditDialog {

    public interface Callback {
        void onUpdateSucces();
    }

    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    int id;
    Context mContext;
    Callback mCallback;

    @BindView(R.id.spinner_shifts)
    Spinner spinnerShifts;
    @BindView(R.id.toggle_btn_probe_fe)
    ToggleButton toggleButtonProbeFe;
    @BindView(R.id.toggle_btn_probe_nfe)
    ToggleButton toggleButtonProbeNFE;
    @BindView(R.id.toggle_btn_probe_ss)
    ToggleButton toggleButtonProbeSS;
    @BindView(R.id.toggle_btn_interlock)
    ToggleButton toggleButtonInterlock;
    @BindView(R.id.txt_view_machine_no)
    TextView txtViewMachineNo;
    @BindView(R.id.edit_text_remarks)
    EditText editTextRemarks;
    @BindView(R.id.edit_text_by_sensitivity)
    EditText editTextSensitivity;
    String shift;
    boolean isTimeSelected = false;
    String[] shiftList = {
            "Select Shift",
            "A",
            "B",
            "C"
    };
    DBHelper dbHelper;

    public MDRecordEditDialog(Activity mActivity, Context mContext, Callback mCallback) {
        this.mActivity = mActivity;
        this.mContext = mContext;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.activity_mdrecord_form, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
        setSpinnerShifts();
        initializeDBHelper();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(mContext);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setSpinnerShifts() {
        final ArrayAdapter<String> shiftListAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, shiftList){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;            }
        };

        shiftListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerShifts.setAdapter(shiftListAdapter);
        spinnerShifts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                shift = shiftList[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void showDialog() {
        dialog.show();
    }

    public void setData(Cursor c) {
        c.moveToNext();
        id = c.getInt(c.getColumnIndex("id"));
        shift = c.getString(c.getColumnIndex("shift"));
        txtViewMachineNo.setText(c.getString(c.getColumnIndex("md_no")));
        if (c.getString(c.getColumnIndex("prove_fe")).equals("Ok")) {
            toggleButtonProbeFe.setChecked(true);
        } else {
            toggleButtonProbeFe.setChecked(false);
        }

        if (c.getString(c.getColumnIndex("prove_nfe")).equals("Ok")) {
            toggleButtonProbeNFE.setChecked(true);
        } else {
            toggleButtonProbeNFE.setChecked(false);
        }

        if (c.getString(c.getColumnIndex("prove_ss")).equals("Ok")) {
            toggleButtonProbeSS.setChecked(true);
        } else {
            toggleButtonProbeSS.setChecked(false);
        }

        if (c.getString(c.getColumnIndex("inter_lock")).equals("Ok")) {
            toggleButtonInterlock.setChecked(true);
        } else {
            toggleButtonInterlock.setChecked(false);
        }

        editTextSensitivity.setText(c.getString(c.getColumnIndex("sensitivity")));
        editTextRemarks.setText(c.getString(c.getColumnIndex("remarks")));

    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (shift == null || editTextSensitivity.getText().toString().trim().isEmpty() ) {
            Toasty.warning(mContext, "Fields empty", Toast.LENGTH_SHORT, true).show();
        } else {
            if (dbHelper.updateMDRecord(
                    shift,
                    toggleButtonProbeFe.getText().toString(),
                    toggleButtonProbeNFE.getText().toString(),
                    toggleButtonProbeSS.getText().toString(),
                    toggleButtonInterlock.getText().toString(),
                    editTextSensitivity.getText().toString(),
                    editTextRemarks.getText().toString(),
                    id
            )) {
                Toasty.success(mContext, "Successfully updated", Toast.LENGTH_LONG, true).show();
                dialog.dismiss();
                mCallback.onUpdateSucces();
            } else {
                Toasty.error(mContext, "Something went wrong", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

}
