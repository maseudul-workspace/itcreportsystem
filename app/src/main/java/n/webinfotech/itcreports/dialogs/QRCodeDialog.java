package n.webinfotech.itcreports.dialogs;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;

import n.webinfotech.itcreports.R;

/**
 * Created by Raj on 10-09-2019.
 */

public class QRCodeDialog {

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    ImageView imgViewQrCode;

    public QRCodeDialog(Context mContext, Activity mActivity) {
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_qr_code, null);
        imgViewQrCode = (ImageView) dialogContainer.findViewById(R.id.img_view_qr_code); builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    public void setImageView(Bitmap bitmap) {
        imgViewQrCode.setImageBitmap(bitmap);
    }

    public void showDialog() {
        dialog.show();
    }

}
