package n.webinfotech.itcreports.dialogs;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;

import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.R;

public class AddSignatureDialog {
    public interface Callback {
        void onSaveClicked(Bitmap signatureBitmap);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    SignaturePad signaturePad;
    Button btnSave;
    Button btnClear;
    int flag = 0;

    public AddSignatureDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_add_signature, null);

        signaturePad = (SignaturePad) dialogContainer.findViewById(R.id.signature_pad);
        btnSave = (Button) dialogContainer.findViewById(R.id.btn_add);
        btnClear = (Button) dialogContainer.findViewById(R.id.btn_clear);

        signaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
                flag = 1;
            }

            @Override
            public void onSigned() {
                flag = 2;
            }

            @Override
            public void onClear() {
                flag = 0;
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flag == 0) {
                    Toasty.warning(mContext, "Please enter your signature", Toast.LENGTH_SHORT, true).show();
                } else {
                    mCallback.onSaveClicked(signaturePad.getSignatureBitmap());
                }
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signaturePad.clear();
            }
        });

        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.hide();
    }

}
