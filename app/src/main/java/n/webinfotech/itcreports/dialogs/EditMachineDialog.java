package n.webinfotech.itcreports.dialogs;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.R;

/**
 * Created by Raj on 09-09-2019.
 */

public class EditMachineDialog {

    public interface Callback {
        void updateMachine(String machineName, String machineNo, int id);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    EditText editTextMachineName;
    EditText editTextMachineNo;
    Button btnSubmit;
    int id;

    public EditMachineDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_machine_edit, null);
        editTextMachineName = (EditText) dialogContainer.findViewById(R.id.edit_text_machine_name);
        editTextMachineNo = (EditText) dialogContainer.findViewById(R.id.edit_text_machine_no);
        btnSubmit = (Button) dialogContainer.findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editTextMachineName.getText().toString().trim().isEmpty() || editTextMachineNo.getText().toString().trim().isEmpty()) {
                    Toasty.warning(mContext, "Fields missing", Toast.LENGTH_SHORT, true).show();
                } else {
                    mCallback.updateMachine(editTextMachineName.getText().toString(), editTextMachineNo.getText().toString(), id);
                }
            }
        });
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    public void setEditTexts(String machineName, String machineNo, int id) {
        this.id = id;
        editTextMachineName.setText(machineName);
        editTextMachineNo.setText(machineNo);
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

}
