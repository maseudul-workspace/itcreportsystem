package n.webinfotech.itcreports.dialogs;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;

public class SieveAndMagnetEditDialog {

    public interface Callback {
        void onUpdateSucces();
    }

    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    int id;
    Context mContext;
    Callback mCallback;

    @BindView(R.id.spinner_shifts)
    Spinner spinnerShifts;
    @BindView(R.id.txt_view_machine_no)
    TextView txtViewMachineNo;
    @BindView(R.id.edit_text_remarks)
    EditText editTextRemarks;
    @BindView(R.id.toggle_btn_sieve_condition)
    ToggleButton toggleButtonSieveCondition;
    @BindView(R.id.toggle_btn_cleaning_status)
    ToggleButton toggleButtonCleaningStatus;
    String shift;
    String[] shiftList = {
            "Select Shift",
            "A",
            "B",
            "C"
    };
    DBHelper dbHelper;

    public SieveAndMagnetEditDialog(Activity mActivity, Context mContext, Callback mCallback) {
        this.mActivity = mActivity;
        this.mContext = mContext;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.activity_sieve_and_magnet_checklist_form, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
        setSpinnerShifts();
        initializeDBHelper();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(mContext);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setSpinnerShifts() {
        final ArrayAdapter<String> shiftListAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, shiftList){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;            }
        };

        shiftListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerShifts.setAdapter(shiftListAdapter);
        spinnerShifts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                shift = shiftList[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void showDialog() {
        dialog.show();
    }

    public void setData(Cursor c) {

        c.moveToNext();
        id = c.getInt(c.getColumnIndex("id"));
        shift = c.getString(c.getColumnIndex("shift"));
        txtViewMachineNo.setText(c.getString(c.getColumnIndex("shifter")));
        editTextRemarks.setText(c.getString(c.getColumnIndex("remarks")));

        if (c.getString(c.getColumnIndex("sleve_condition")).equals("Ok")) {
            toggleButtonSieveCondition.setChecked(true);
        } else {
            toggleButtonSieveCondition.setChecked(false);
        }

        if (c.getString(c.getColumnIndex("magnet_cleaning_status")).equals("Ok")) {
            toggleButtonCleaningStatus.setChecked(true);
        } else {
            toggleButtonCleaningStatus.setChecked(false);
        }
    }

    @OnClick(R.id.btn_submit) void onSubmitBtnClicked() {
        if ( shift == null ) {
            Toasty.warning(mContext, "Fields missing", Toast.LENGTH_SHORT, true).show();
        } else {
            if(dbHelper.updateSieveMagnetRecord(
                    shift,
                    toggleButtonSieveCondition.getText().toString(),
                    toggleButtonCleaningStatus.getText().toString(),
                    editTextRemarks.getText().toString(),
                    id
            )) {
                Toasty.success(mContext, "Data updated successfully", Toast.LENGTH_LONG, true).show();
                dialog.dismiss();
                mCallback.onUpdateSucces();
            } else {
                Toasty.error(mContext, "Failed to update data", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

}
