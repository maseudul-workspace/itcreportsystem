package n.webinfotech.itcreports.dialogs;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;

public class CFCCheckingEditDialog {

    public interface Callback {
        void onUpdateSucces();
    }

    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    int id;
    Context mContext;
    @BindView(R.id.spinner_shifts)
    Spinner spinnerShifts;
    @BindView(R.id.radio_group_sku)
    RadioGroup radioGroupSKU;
    @BindView(R.id.txt_view_machine_no)
    TextView txtViewMachine;
    @BindView(R.id.edit_text_torn)
    EditText editTextTorn;
    @BindView(R.id.edit_text_flap)
    EditText editTextFlap;
    @BindView(R.id.edit_text_dent)
    EditText editTextDent;
    @BindView(R.id.toggle_btn_coding_mono)
    ToggleButton toggleButtonCodingMono;
    @BindView(R.id.toggle_btn_coding_cfc)
    ToggleButton toggleButtonCodingCFC;
    @BindView(R.id.edit_text_qty_mono)
    EditText editTextQtyMono;
    @BindView(R.id.edit_text_qty_cfc)
    EditText editTextQtyCfc;
    @BindView(R.id.toggle_btn_tapping)
    ToggleButton toggleButtonTapping;
    String machine;
    String shift;
    String sku;
    String[] shiftList = {
            "Select Shift",
            "A",
            "B",
            "C"
    };
    DBHelper dbHelper;
    @BindView(R.id.edit_text_remarks)
    EditText editTextRemarks;
    Callback mCallback;
    @BindView(R.id.radio_btn_sku_75)
    RadioButton radioButtonSKU75;
    @BindView(R.id.radio_btn_sku_20)
    RadioButton radioButtonSKU20;

    public CFCCheckingEditDialog(Activity mActivity, Context mContext, Callback mCallback) {
        this.mActivity = mActivity;
        this.mContext = mContext;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.activity_cfcchecking_sheet_form, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
        setUpFields();
        initializeDBHelper();
    }

    public void setUpFields() {
        radioGroupSKU.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                sku = checkedRadioButton.getText().toString();
            }
        });

        final ArrayAdapter<String> shiftListAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, shiftList){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;            }
        };

        shiftListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerShifts.setAdapter(shiftListAdapter);
        spinnerShifts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                shift = shiftList[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(mContext);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void showDialog() {
        dialog.show();
    }

    public void setData(Cursor c) {
        c.moveToNext();
        id =  c.getInt(c.getColumnIndex("id"));
        shift = c.getString(c.getColumnIndex("shift"));
        sku = c.getString(c.getColumnIndex("sku"));
        if (sku.contains("75 gm")) {
            radioButtonSKU75.setChecked(true);
        } else {
            radioButtonSKU20.setChecked(true);
        }

        editTextTorn.setText(c.getString(c.getColumnIndex("rhno_of_torn")));
        editTextDent.setText(c.getString(c.getColumnIndex("no_of_dent")));
        editTextFlap.setText(c.getString(c.getColumnIndex("no_of_flap_open")));
        editTextQtyCfc.setText(c.getString(c.getColumnIndex("qtty_present_cfc")));
        editTextQtyMono.setText(c.getString(c.getColumnIndex("qtty_present_mono")));
        editTextRemarks.setText(c.getString(c.getColumnIndex("remarks")));

        if (c.getString(c.getColumnIndex("coading_cfc")).equals("Ok")) {
            toggleButtonCodingCFC.setChecked(true);
        } else {
            toggleButtonCodingCFC.setChecked(false);
        }

        if (c.getString(c.getColumnIndex("coading_mono")).equals("Ok")) {
            toggleButtonCodingMono.setChecked(true);
        } else {
            toggleButtonCodingMono.setChecked(false);
        }

        if (c.getString(c.getColumnIndex("tapping_quality")).equals("Ok")) {
            toggleButtonTapping.setChecked(true);
        } else {
            toggleButtonTapping.setChecked(false);
        }

        txtViewMachine.setText(c.getString(c.getColumnIndex("machine_no")));

    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (shift ==  null || sku == null || editTextDent.getText().toString().trim().isEmpty() || editTextFlap.getText().toString().trim().isEmpty() || editTextQtyCfc.getText().toString().trim().isEmpty() || editTextQtyMono.getText().toString().trim().isEmpty() || editTextTorn.getText().toString().trim().isEmpty()) {
            Toasty.warning(mContext, "Some fields are empty", Toast.LENGTH_SHORT, true).show();
        } else {

            if (dbHelper.updateCFCChecking(
                    shift,
                    sku,
                    editTextTorn.getText().toString(),
                    editTextFlap.getText().toString(),
                    editTextDent.getText().toString(),
                    toggleButtonCodingMono.getText().toString(),
                    editTextQtyMono.getText().toString(),
                    editTextQtyCfc.getText().toString(),
                    toggleButtonTapping.getText().toString(),
                    toggleButtonCodingCFC.getText().toString(),
                    editTextRemarks.getText().toString(),
                    id
            )) {
                Toasty.success(mContext, "Updated Successfully", Toast.LENGTH_LONG, true).show();
                mCallback.onUpdateSucces();
                dialog.dismiss();
            } else {
                Toasty.error(mContext, "Failed to update", Toast.LENGTH_LONG, true).show();
            }

        }
    }

}
