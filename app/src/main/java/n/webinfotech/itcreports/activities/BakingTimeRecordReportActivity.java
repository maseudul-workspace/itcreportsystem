package n.webinfotech.itcreports.activities;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.BakingTimeDataAdapter;
import n.webinfotech.itcreports.adapters.BakingTimeRecordVerficationAdapter;
import n.webinfotech.itcreports.dialogs.BakingTimeRecordEditDialog;
import n.webinfotech.itcreports.models.BakingTimeRecord;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.Helper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;

public class BakingTimeRecordReportActivity extends AppCompatActivity implements BakingTimeDataAdapter.Callback, BakingTimeRecordEditDialog.Callback {

    DBHelper dbHelper;
    String startDate;
    String endDate;
    @BindView(R.id.txt_view_start_date)
    TextView txtViewStartDate;
    @BindView(R.id.txt_view_end_date)
    TextView txtViewEndDate;
    @BindView(R.id.recycler_view_baking_time_record_data)
    RecyclerView recyclerView;
    Cursor cursor;
    BakingTimeRecordEditDialog bakingTimeRecordEditDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baking_time_record_report);
        ButterKnife.bind(this);
        initializeDBHelper();
        startDate = Helper.getSearchCurrentDate();
        endDate = Helper.getSearchNextDate();
        txtViewStartDate.setText(startDate);
        txtViewEndDate.setText(endDate);
        cursor = dbHelper.getBakingTimeData(startDate, endDate);
        if (cursor.getCount() == 0) {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
        setRecyclerView(cursor);
        setUpEditDialog();
    }

    public void setUpEditDialog() {
        bakingTimeRecordEditDialog = new BakingTimeRecordEditDialog(this, this, this);
        bakingTimeRecordEditDialog.setUpDialog();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public byte[] getByteFromDrawable(Drawable drawable) {
        Bitmap logoBitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        logoBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] bytes = stream.toByteArray();
        return bytes;
    }

    public void setRecyclerView(Cursor c) {
        if (c.getCount() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            ArrayList<BakingTimeRecord> bakingTimeRecords = new ArrayList<>();
            while (c.moveToNext()) {
                BakingTimeRecord bakingTimeRecord = new BakingTimeRecord(
                        c.getInt(c.getColumnIndex("id")),
                        c.getString(c.getColumnIndex("date")),
                        c.getString(c.getColumnIndex("time")),
                        c.getString(c.getColumnIndex("shift")),
                        c.getString(c.getColumnIndex("oven_no")),
                        c.getString(c.getColumnIndex("sku")),
                        c.getString(c.getColumnIndex("bt_on_hmi")),
                        c.getString(c.getColumnIndex("actual_bt")),
                        c.getInt(c.getColumnIndex("user_id")),
                        c.getString(c.getColumnIndex("u_name")),
                        c.getInt(c.getColumnIndex("verify_status")),
                        c.getInt(c.getColumnIndex("verify_id"))
                );
                bakingTimeRecords.add(bakingTimeRecord);
            }
            BakingTimeDataAdapter adapter = new BakingTimeDataAdapter(this, bakingTimeRecords, this);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
            recyclerView.addItemDecoration(itemDecor);
        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }

    public void saveData() {

        File sd = Environment.getExternalStorageDirectory();
        String csvFile = getDate() + "_bakingtime.xls";

        File directory = new File(sd.getAbsolutePath() + "/ITCReports/BakingTimeRecord");
        //create directory if not exist
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
        try {

            //file path
            File file = new File(directory, csvFile);
            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setLocale(new Locale("en", "EN"));
            WritableWorkbook workbook;
            workbook = Workbook.createWorkbook(file, wbSettings);
            //Excel sheet name. 0 represents first sheet
            WritableSheet sheet = workbook.createSheet("BakingTimeRecord", 0);


//            Excel Header
//            Add logo
            byte[] logoBytes = getByteFromDrawable(getDrawable(R.drawable.itc_logo));
            WritableImage writableImageLogo = new WritableImage(0, 0, 2, 3, logoBytes);
            sheet.addImage(writableImageLogo);

//            Add header
            byte[] headerBytes = getByteFromDrawable(getDrawable(R.drawable.bakingtime));
            WritableImage headerLogo = new WritableImage(2, 0, 5, 3, headerBytes);
            sheet.addImage(headerLogo);

            sheet.addCell(new Label(7, 0, "Document No"));

            for (int i = 2; i < 9; i++) {
                CellView cell = sheet.getColumnView(i);
                cell.setAutosize(true);
                sheet.setColumnView(i, cell);
            }

            sheet.addCell(new Label(8, 0, "  "));


            sheet.addCell(new Label(7, 1, "Revision No"));
            sheet.addCell(new Label(8, 1, "  "));

            sheet.addCell(new Label(7, 2, "Effective Date"));
            sheet.addCell(new Label(8, 2, "  "));


            WritableFont arial10pt = new WritableFont(WritableFont.ARIAL, 9);
            arial10pt.setBoldStyle(WritableFont.BOLD);
            WritableCellFormat cellFormat = new WritableCellFormat(arial10pt);
            sheet.mergeCells(0, 3, 8, 3);
            sheet.addCell(new Label(0, 3, "Frequency: Once In a Shift", cellFormat));

            sheet.addCell(new Label(0, 4, "Date", cellFormat));

            sheet.addCell(new Label(1, 4, "Shift", cellFormat));

            sheet.addCell(new Label(2, 4, "Time", cellFormat));

            sheet.addCell(new Label(3, 4, "  Oven No  ", cellFormat));

            sheet.addCell(new Label(4, 4, "  SKU  ", cellFormat));

            sheet.addCell(new Label(5, 4, "  BT On HMI  ", cellFormat));

            sheet.addCell(new Label(6, 4, "  Actual BT  ", cellFormat));

            sheet.addCell(new Label(7, 4, "  Checked By  ", cellFormat));

            sheet.addCell(new Label(8, 4, "  Verified By  ", cellFormat));

            int row_count = 5;
            cursor = dbHelper.getBakingTimeData(startDate, endDate);
            if (cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    if (cursor.getInt(cursor.getColumnIndex("verify_status")) == 2) {
                        String sign = dbHelper.getVerifierSignature(cursor.getInt(cursor.getColumnIndex("verify_id")));
                        if (sign != null) {
                            byte[] signatureBytes = getByteFromFile(sign);
                            WritableImage signature = new WritableImage(8, row_count, 1, 1, signatureBytes);
                            sheet.addImage(signature);
                        }
                    }
                    sheet.addCell(new Label(0, row_count, cursor.getString(cursor.getColumnIndex("date"))));
                    sheet.addCell(new Label(1, row_count, cursor.getString(cursor.getColumnIndex("shift"))));
                    sheet.addCell(new Label(2, row_count, cursor.getString(cursor.getColumnIndex("time"))));
                    sheet.addCell(new Label(3, row_count, cursor.getString(cursor.getColumnIndex("oven_no"))));
                    sheet.addCell(new Label(4, row_count, cursor.getString(cursor.getColumnIndex("sku"))));
                    sheet.addCell(new Label(5, row_count, cursor.getString(cursor.getColumnIndex("bt_on_hmi"))));
                    sheet.addCell(new Label(6, row_count, cursor.getString(cursor.getColumnIndex("actual_bt"))));
                    sheet.addCell(new Label(7, row_count, cursor.getString(cursor.getColumnIndex("u_name"))));
                    row_count++;
                }
            }


            workbook.write();
            workbook.close();
            Toast.makeText(this,
                    "Data Exported in a Excel Sheet", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e("LogMsg", "Workbook error" + e.getMessage());
            e.printStackTrace();
        }
    }

    public byte[] getByteFromFile(String signature) {
        Log.e("LogMsg", "Signatuire " + signature);
        File file = new File(getAlbumStorageDir("ITCReports/Signatures"), signature);
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            Log.e("LogMsg", "Error: " + e.getMessage());
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("LogMsg", "Error: " + e.getMessage());
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    public static File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStorageDirectory(), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    @OnClick(R.id.img_view_calendar_start_date)
    void onCalendarStartDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        startDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        startDate = Helper.changeDateFormat(startDate);
                        txtViewStartDate.setText(startDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.img_view_calendar_end_date)
    void onCalendarEndDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        endDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        endDate = Helper.changeDateFormat(endDate);
                        txtViewEndDate.setText(endDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.btn_export)
    void onExportClicked() {
        saveData();
    }

    @OnClick(R.id.btn_search)
    void onSearchClicked() {
        if (startDate == null || endDate == null) {
            Toasty.warning(this, "Please enter both dates", Toast.LENGTH_SHORT).show();
        } else {
            if (cursor.getCount() == 0) {
                Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
            }
            cursor = dbHelper.getBakingTimeData(startDate, endDate);
            setRecyclerView(cursor);
        }
    }

    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy_MM_dd_hh_mm_a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    public void onEditClicked(int id) {
        bakingTimeRecordEditDialog.setData(dbHelper.getSingleBakingTimeRecord(id));
        bakingTimeRecordEditDialog.showDialog();
    }

    @Override
    public void onDeleteClicked(int id) {
        if (dbHelper.deleteBakingTimeRecordData(id)) {
            Toasty.success(this, "Deleted Successfully", Toast.LENGTH_SHORT, true).show();
            onSearchClicked();
        } else {
            Toasty.error(this, "Unable To Delete", Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void onUpdateSucces() {
        onSearchClicked();
    }
}
