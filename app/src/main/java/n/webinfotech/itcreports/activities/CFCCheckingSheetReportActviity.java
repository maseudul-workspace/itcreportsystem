package n.webinfotech.itcreports.activities;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.CFCCheckingUnverifiedAdapter;
import n.webinfotech.itcreports.adapters.CFCReportAdapter;
import n.webinfotech.itcreports.adapters.HourlyPQISheetDataAdapter;
import n.webinfotech.itcreports.dialogs.CFCCheckingEditDialog;
import n.webinfotech.itcreports.models.CFCChecking;
import n.webinfotech.itcreports.models.HourlyPQISheet;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.Helper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;
import static n.webinfotech.itcreports.util.Helper.getByteFromDrawable;
import static n.webinfotech.itcreports.util.Helper.getByteFromFile;

public class CFCCheckingSheetReportActviity extends AppCompatActivity implements CFCCheckingEditDialog.Callback, CFCReportAdapter.Callback {

    DBHelper dbHelper;
    String startDate;
    String endDate;
    @BindView(R.id.txt_view_start_date)
    TextView txtViewStartDate;
    @BindView(R.id.txt_view_end_date)
    TextView txtViewEndDate;
    @BindView(R.id.recycler_view_cfc_data)
    RecyclerView recyclerView;
    Cursor cursor;
    CFCCheckingEditDialog cfcCheckingEditDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cfcchecking_sheet_report_actviity);
        ButterKnife.bind(this);
        initializeDBHelper();
        startDate = Helper.getSearchCurrentDate();
        endDate = Helper.getSearchNextDate();
        cursor = dbHelper.getCFCSheetData(startDate, endDate);
        txtViewStartDate.setText(startDate);
        txtViewEndDate.setText(endDate);
        if (cursor.getCount() == 0) {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
        setRecyclerView(cursor);
        setUpDialog();
    }

    public void setUpDialog() {
        cfcCheckingEditDialog = new CFCCheckingEditDialog(this, this, this);
        cfcCheckingEditDialog.setUpDialog();
    }

    public void saveData() {

        File sd = Environment.getExternalStorageDirectory();
        String csvFile = getDate() + "cfc_checking_sheeet.xls";

        File directory = new File(sd.getAbsolutePath()+"/ITCReports/CFCCheckingSheet");
        //create directory if not exist
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
        try {

            //file path
            File file = new File(directory, csvFile);
            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setLocale(new Locale("en", "EN"));
            WritableWorkbook workbook;
            workbook = Workbook.createWorkbook(file, wbSettings);
            //Excel sheet name. 0 represents first sheet
            WritableSheet sheet = workbook.createSheet("Hourly CFC Checking", 0);


//            Excel Header
//            Add logo
            byte[] logoBytes = getByteFromDrawable(getDrawable(R.drawable.itc_logo));
            WritableImage writableImageLogo = new WritableImage(0, 0, 2,4, logoBytes);
            sheet.addImage(writableImageLogo);

//            Add header
            byte[] headerBytes = getByteFromDrawable(getDrawable(R.drawable.hourlypqj));
            WritableImage headerLogo = new WritableImage(2, 0, 12,4, headerBytes);
            sheet.addImage(headerLogo);

            sheet.addCell(new Label(14, 1, "Document No"));

            for(int i = 1; i < 16; i++) {
                CellView cell=sheet.getColumnView(i);
                cell.setAutosize(true);
                sheet.setColumnView(i, cell);
            }

            sheet.addCell(new Label(15, 1, "  "));


            sheet.addCell(new Label(14, 2, "Revision No"));
            sheet.addCell(new Label(15, 2, "  "));

            sheet.addCell(new Label(14, 3, "Effective Date"));
            sheet.addCell(new Label(15, 3, "  "));


            WritableFont arial10pt = new WritableFont(WritableFont.ARIAL, 9);
            arial10pt.setBoldStyle(WritableFont.BOLD);
            WritableCellFormat cellFormat = new WritableCellFormat(arial10pt);

            sheet.mergeCells(0,4,4,4);
            sheet.addCell(new Label(0, 4, "Frequency: 1CFC/Machine/Hour", cellFormat));

            sheet.mergeCells(5,4,9,4);
            sheet.addCell(new Label(5, 4, "MonoCartoon", cellFormat));

            sheet.mergeCells(10,4,12,4);
            sheet.addCell(new Label(10, 4, "CFC", cellFormat));

            sheet.addCell(new Label(0, 5, "Date", cellFormat));

            sheet.addCell(new Label(1, 5, "Shift", cellFormat));

            sheet.addCell(new Label(2, 5, "Time", cellFormat));

            sheet.addCell(new Label(3, 5, "Machine No", cellFormat));

            sheet.addCell(new Label(4, 5, "SKU", cellFormat));

            sheet.addCell(new Label(5, 5, "No of Torn", cellFormat));

            sheet.addCell(new Label(6, 5, "No of Flap Open", cellFormat));

            sheet.addCell(new Label(7, 5, "No of Dent", cellFormat));

            sheet.addCell(new Label(8, 5, "Coding", cellFormat));

            sheet.addCell(new Label(9, 5, "Qty Present", cellFormat));

            sheet.addCell(new Label(10, 5, "Qty Present", cellFormat));

            sheet.addCell(new Label(11, 5, "Tapping Quality", cellFormat));

            sheet.addCell(new Label(12, 5, "Coding", cellFormat));


            sheet.mergeCells(13,4,13,5);
            sheet.addCell(new Label(13, 4, "Checked By", cellFormat));

            sheet.mergeCells(14,4,14,5);
            sheet.addCell(new Label(14, 4, "Remarks", cellFormat));

            sheet.mergeCells(15,4,15,5);
            sheet.addCell(new Label(15, 4, "Verfied by", cellFormat));

            int row_count = 6;

            Cursor c = dbHelper.getCFCSheetData(startDate, endDate);
            if (c.getCount() > 0) {
                Log.e("LogMsg", "Data vcount: " + c.getCount());
                while (c.moveToNext()) {
                    if (c.getInt(c.getColumnIndex("verify_status")) == 2) {
                        String sign = dbHelper.getVerifierSignature(c.getInt(c.getColumnIndex("verify_id")));
                        if (sign != null) {
                            byte[] signatureBytes = getByteFromFile(sign);
                            Log.e("LogMsg", "Hello1");
                            WritableImage signature = new WritableImage(15, row_count, 1, 1, signatureBytes);
                            sheet.addImage(signature);
                        }
                    }
                    sheet.addCell(new Label(0, row_count, c.getString(c.getColumnIndex("date"))));
                    sheet.addCell(new Label(1, row_count, c.getString(c.getColumnIndex("shift"))));
                    sheet.addCell(new Label(2, row_count, c.getString(c.getColumnIndex("time"))));
                    sheet.addCell(new Label(3, row_count, c.getString(c.getColumnIndex("machine_no"))));
                    sheet.addCell(new Label(4, row_count, c.getString(c.getColumnIndex("sku"))));
                    sheet.addCell(new Label(5, row_count, c.getString(c.getColumnIndex("rhno_of_torn"))));
                    sheet.addCell(new Label(6, row_count, c.getString(c.getColumnIndex("no_of_flap_open"))));
                    sheet.addCell(new Label(7, row_count, c.getString(c.getColumnIndex("no_of_dent"))));
                    sheet.addCell(new Label(8, row_count, c.getString(c.getColumnIndex("coading_mono"))));
                    sheet.addCell(new Label(9, row_count, c.getString(c.getColumnIndex("qtty_present_mono"))));
                    sheet.addCell(new Label(10, row_count, c.getString(c.getColumnIndex("qtty_present_cfc"))));
                    sheet.addCell(new Label(11, row_count, c.getString(c.getColumnIndex("tapping_quality"))));
                    sheet.addCell(new Label(12, row_count, c.getString(c.getColumnIndex("coading_cfc"))));
                    sheet.addCell(new Label(13, row_count, c.getString(c.getColumnIndex("u_name"))));
                    sheet.addCell(new Label(14 , row_count, c.getString(c.getColumnIndex("remarks"))));
                    row_count++;
                }
            }


            workbook.write();
            workbook.close();
            Toast.makeText(this,
                    "Data Exported in a Excel Sheet", Toast.LENGTH_SHORT).show();
        } catch(Exception e){
            Log.e("LogMsg", "Workbook error" + e.getMessage());
            e.printStackTrace();
        }
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @OnClick(R.id.img_view_calendar_start_date) void onCalendarStartDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        startDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        startDate = Helper.changeDateFormat(startDate);
                        txtViewStartDate.setText(startDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.img_view_calendar_end_date) void onCalendarEndDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        endDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        endDate = Helper.changeDateFormat(endDate);
                        txtViewEndDate.setText(endDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.btn_export) void onExportClicked() {
        saveData();
    }
    @OnClick(R.id.btn_search) void onSearchClicked() {
        if (startDate == null || endDate == null){
            Toasty.warning(this, "Please enter both dates", Toast.LENGTH_SHORT).show();
        } else {
            cursor = dbHelper.getCFCSheetData(startDate, endDate);
            Log.e("LogMsg", "Cursor Count: " + cursor.getCount());
            setRecyclerView(cursor);
            if (cursor.getCount() == 0) {
                Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setRecyclerView(Cursor c) {
        if (c.getCount() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            ArrayList<CFCChecking> cfcCheckings = new ArrayList<>();
            while (c.moveToNext()) {
                CFCChecking cfcChecking = new CFCChecking(
                        c.getInt(c.getColumnIndex("id")),
                        c.getString(c.getColumnIndex("date")),
                        c.getString(c.getColumnIndex("time")),
                        c.getString(c.getColumnIndex("shift")),
                        c.getString(c.getColumnIndex("machine_no")),
                        c.getString(c.getColumnIndex("sku")),
                        c.getString(c.getColumnIndex("rhno_of_torn")),
                        c.getString(c.getColumnIndex("no_of_flap_open")),
                        c.getString(c.getColumnIndex("no_of_dent")),
                        c.getString(c.getColumnIndex("coading_mono")),
                        c.getString(c.getColumnIndex("qtty_present_mono")),
                        c.getString(c.getColumnIndex("qtty_present_cfc")),
                        c.getString(c.getColumnIndex("tapping_quality")),
                        c.getString(c.getColumnIndex("coading_cfc")),
                        c.getString(c.getColumnIndex("remarks")),
                        c.getInt(c.getColumnIndex("user_id")),
                        c.getString(c.getColumnIndex("u_name")),
                        c.getInt(c.getColumnIndex("verify_status")),
                        c.getInt(c.getColumnIndex("verify_id"))
                );
                cfcCheckings.add(cfcChecking);
            }
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            CFCReportAdapter adapter = new CFCReportAdapter(this, cfcCheckings, this);
            recyclerView.setAdapter(adapter);
            DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
            recyclerView.addItemDecoration(itemDecor);
        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }

    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy_MM_dd_hh_mm_a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    public void onUpdateSucces() {
        onSearchClicked();
    }

    @Override
    public void onEditClicked(int id) {
        cfcCheckingEditDialog.setData(dbHelper.getSingleCFCdata(id));
        cfcCheckingEditDialog.showDialog();
    }

    @Override
    public void onDeleteClicked(int id) {
        if (dbHelper.deleteCFCData(id)) {
            Toasty.success(this, "Deleted Successfully", Toast.LENGTH_SHORT, true).show();
            onSearchClicked();
        } else {
            Toasty.error(this, "Unable To Delete", Toast.LENGTH_SHORT, true).show();
        }
    }
}
