package n.webinfotech.itcreports.activities;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.BatchNo;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.SaveSignatureHelper;

public class OnlineSensoryEvaluationFormActivity extends AppCompatActivity {


    @BindView(R.id.spinner_shifts)
    Spinner spinnerShifts;
    @BindView(R.id.radio_group_sku)
    RadioGroup radioGroupSKU;
    String machineName;
    String shift;
    String sku;
    String[] shiftList = {
            "Select Shift",
            "A",
            "B",
            "C"
    };
    DBHelper dbHelper;
    AndroidApplication androidApplication;
    @BindView(R.id.spinner_machines)
    Spinner spinnerMachines;
    String[] machines = {
            "Select Module",
            "M-1",
            "M-2"
    };
    String[] appearance = {
            "Appearance",
            "1",
            "2",
            "3",
            "4",
            "5"
    };
    String[] taste = {
            "Taste",
            "1",
            "2",
            "3",
            "4",
            "5"
    };
    String[] aroma = {
            "Aroma",
            "1",
            "2",
            "3",
            "4",
            "5"
    };
    @BindView(R.id.signature_pad_pannelist1)
    SignaturePad signaturePadPannelist1;
    @BindView(R.id.signature_pad_pannelist2)
    SignaturePad signaturePadPannelist2;
    @BindView(R.id.signature_pad_pannelist3)
    SignaturePad signaturePadPannelist3;
    @BindView(R.id.edit_text_pannelist1_name)
    EditText editTextPannelist1Name;
    @BindView(R.id.edit_text_pannelist2_name)
    EditText editTextPannelist2Name;
    @BindView(R.id.edit_text_pannelist3_name)
    EditText editTextPannelist3Name;
    int signatureCount = 0;
    int pannelistSignatureFlag1 = 0;
    int pannelistSignatureFlag2 = 0;
    int pannelistSignatureFlag3 = 0;
    String pannelist1Signature;
    String pannelist2Signature;
    String pannelist3Signature;
    @BindView(R.id.layout_pannelist1_signature)
    View layoutPannelist1Signature;
    @BindView(R.id.layout_pannelist2_signature)
    View layoutPannelist2Signature;
    @BindView(R.id.layout_pannelist3_signature)
    View layoutPannelist3Signature;
    @BindView(R.id.layout_test)
    LinearLayout layoutTest;
    @BindView(R.id.linear_layout_pannelist_1_appearance)
    LinearLayout linearLayoutPannelist1Appearance;
    @BindView(R.id.linear_layout_pannelist_1_aroma)
    LinearLayout linearLayoutPannelist1Aroma;
    @BindView(R.id.linear_layout_pannelist_1_taste)
    LinearLayout linearLayoutPannelist1Taste;
    @BindView(R.id.linear_layout_pannelist_2_appearance)
    LinearLayout linearLayoutPannelist2Appearance;
    @BindView(R.id.linear_layout_pannelist_2_aroma)
    LinearLayout linearLayoutPannelist2Aroma;
    @BindView(R.id.linear_layout_pannelist_2_taste)
    LinearLayout linearLayoutPannelist2Taste;
    @BindView(R.id.linear_layout_pannelist_3_appearance)
    LinearLayout linearLayoutPannelist3Appearance;
    @BindView(R.id.linear_layout_pannelist_3_aroma)
    LinearLayout linearLayoutPannelist3Aroma;
    @BindView(R.id.linear_layout_pannelist_3_taste)
    LinearLayout linearLayoutPannelist3Taste;
    @BindView(R.id.linear_layout_pannelist_1_batch)
    LinearLayout linearLayoutPannelist1Batch;
    @BindView(R.id.linear_layout_pannelist_2_batch)
    LinearLayout linearLayoutPannelist2Batch;
    @BindView(R.id.linear_layout_pannelist_3_batch)
    LinearLayout linearLayoutPannelist3Batch;
    ArrayList<Integer> batchNos;
    ArrayList<EditText> editTextsPannelist1Appearance;
    ArrayList<EditText> editTextsPannelist2Appearance;
    ArrayList<EditText> editTextsPannelist3Appearance;
    ArrayList<EditText> editTextsPannelist1Taste;
    ArrayList<EditText> editTextsPannelist2Taste;
    ArrayList<EditText> editTextsPannelist3Taste;
    ArrayList<EditText> editTextsPannelist1Aroma;
    ArrayList<EditText> editTextsPannelist2Aroma;
    ArrayList<EditText> editTextsPannelist3Aroma;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_sensory_evaluation_form);
        ButterKnife.bind(this);
        batchNos = getIntent().getIntegerArrayListExtra("batchNos");
        initializeDBHelper();
        setSpinnerMachines();
        setSpinnerShifts();
        setSignaturePads();
        radioGroupSKU.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                sku = checkedRadioButton.getText().toString();
            }
        });
        setUpBatchs();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setUpBatchs() {

        editTextsPannelist1Aroma = new ArrayList<>();
        editTextsPannelist1Appearance = new ArrayList<>();
        editTextsPannelist1Taste = new ArrayList<>();
        editTextsPannelist2Appearance = new ArrayList<>();
        editTextsPannelist2Aroma = new ArrayList<>();
        editTextsPannelist2Taste = new ArrayList<>();
        editTextsPannelist3Appearance = new ArrayList<>();
        editTextsPannelist3Aroma = new ArrayList<>();
        editTextsPannelist3Taste = new ArrayList<>();

        for (int i = 0; i < batchNos.size(); i++) {

            View view = this.getLayoutInflater().inflate(R.layout.layout_edit_text, linearLayoutPannelist1Appearance, false);
            final EditText editText = (EditText) view.findViewById(R.id.edit_text_test);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().trim().isEmpty() && (Integer.parseInt(s.toString()) > 5 || Integer.parseInt(s.toString()) < 1)) {
                        Toasty.warning(getApplicationContext(), "Value should be between 1 to 5", Toast.LENGTH_SHORT, true).show();
                        editText.getText().clear();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            linearLayoutPannelist1Appearance.addView(view);
            editTextsPannelist1Appearance.add(editText);

        }

        for (int i = 0; i < batchNos.size(); i++) {
            View view = this.getLayoutInflater().inflate(R.layout.layout_edit_text, linearLayoutPannelist2Appearance, false);
            final EditText editText = (EditText) view.findViewById(R.id.edit_text_test);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().trim().isEmpty() && (Integer.parseInt(s.toString()) > 5 || Integer.parseInt(s.toString()) < 1)) {
                        Toasty.warning(getApplicationContext(), "Value should be between 1 to 5", Toast.LENGTH_SHORT, true).show();
                        editText.getText().clear();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            linearLayoutPannelist2Appearance.addView(view);
            editTextsPannelist2Appearance.add(editText);
        }

        for (int i = 0; i < batchNos.size(); i++) {
            View view = this.getLayoutInflater().inflate(R.layout.layout_edit_text, linearLayoutPannelist3Appearance, false);
            final EditText editText = (EditText) view.findViewById(R.id.edit_text_test);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().trim().isEmpty() && (Integer.parseInt(s.toString()) > 5 || Integer.parseInt(s.toString()) < 1)) {
                        Toasty.warning(getApplicationContext(), "Value should be between 1 to 5", Toast.LENGTH_SHORT, true).show();
                        editText.getText().clear();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            linearLayoutPannelist3Appearance.addView(view);
            editTextsPannelist3Appearance.add(editText);
        }

        for (int i = 0; i < batchNos.size(); i++) {
            View view = this.getLayoutInflater().inflate(R.layout.layout_edit_text, linearLayoutPannelist1Taste, false);
            final EditText editText = (EditText) view.findViewById(R.id.edit_text_test);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().trim().isEmpty() && (Integer.parseInt(s.toString()) > 5 || Integer.parseInt(s.toString()) < 1)) {
                        Toasty.warning(getApplicationContext(), "Value should be between 1 to 5", Toast.LENGTH_SHORT, true).show();
                        editText.getText().clear();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            linearLayoutPannelist1Taste.addView(view);
            editTextsPannelist1Taste.add(editText);
        }

        for (int i = 0; i < batchNos.size(); i++) {
            View view = this.getLayoutInflater().inflate(R.layout.layout_edit_text, linearLayoutPannelist2Taste, false);
            final EditText editText = (EditText) view.findViewById(R.id.edit_text_test);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().trim().isEmpty() && (Integer.parseInt(s.toString()) > 5 || Integer.parseInt(s.toString()) < 1)) {
                        Toasty.warning(getApplicationContext(), "Value should be between 1 to 5", Toast.LENGTH_SHORT, true).show();
                        editText.getText().clear();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            linearLayoutPannelist2Taste.addView(view);
            editTextsPannelist2Taste.add(editText);
        }

        for (int i = 0; i < batchNos.size(); i++) {
            View view = this.getLayoutInflater().inflate(R.layout.layout_edit_text, linearLayoutPannelist3Taste, false);
            final EditText editText = (EditText) view.findViewById(R.id.edit_text_test);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().trim().isEmpty() && (Integer.parseInt(s.toString()) > 5 || Integer.parseInt(s.toString()) < 1)) {
                        Toasty.warning(getApplicationContext(), "Value should be between 1 to 5", Toast.LENGTH_SHORT, true).show();
                        editText.getText().clear();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            linearLayoutPannelist3Taste.addView(view);
            editTextsPannelist3Taste.add(editText);
        }

        for (int i = 0; i < batchNos.size(); i++) {
            View view = this.getLayoutInflater().inflate(R.layout.layout_edit_text, linearLayoutPannelist1Aroma, false);
            final EditText editText = (EditText) view.findViewById(R.id.edit_text_test);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().trim().isEmpty() && (Integer.parseInt(s.toString()) > 5 || Integer.parseInt(s.toString()) < 1)) {
                        Toasty.warning(getApplicationContext(), "Value should be between 1 to 5", Toast.LENGTH_SHORT, true).show();
                        editText.getText().clear();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            linearLayoutPannelist1Aroma.addView(view);
            editTextsPannelist1Aroma.add(editText);
        }

        for (int i = 0; i < batchNos.size(); i++) {
            View view = this.getLayoutInflater().inflate(R.layout.layout_edit_text, linearLayoutPannelist2Aroma, false);
            final EditText editText = (EditText) view.findViewById(R.id.edit_text_test);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().trim().isEmpty() && (Integer.parseInt(s.toString()) > 5 || Integer.parseInt(s.toString()) < 1)) {
                        Toasty.warning(getApplicationContext(), "Value should be between 1 to 5", Toast.LENGTH_SHORT, true).show();
                        editText.getText().clear();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            linearLayoutPannelist2Aroma.addView(view);
            editTextsPannelist2Aroma.add(editText);
        }

        for (int i = 0; i < batchNos.size(); i++) {
            View view = this.getLayoutInflater().inflate(R.layout.layout_edit_text, linearLayoutPannelist3Aroma, false);
            final EditText editText = (EditText) view.findViewById(R.id.edit_text_test);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().trim().isEmpty() && (Integer.parseInt(s.toString()) > 5 || Integer.parseInt(s.toString()) < 1)) {
                        Toasty.warning(getApplicationContext(), "Value should be between 1 to 5", Toast.LENGTH_SHORT, true).show();
                        editText.getText().clear();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            linearLayoutPannelist3Aroma.addView(view);
            editTextsPannelist3Aroma.add(editText);
        }

        for (int i = 0; i < batchNos.size(); i++) {
            View view = this.getLayoutInflater().inflate(R.layout.layout_batch_no, linearLayoutPannelist1Batch, false);
            TextView txtViewBatch = (TextView) view.findViewById(R.id.txt_view_batch_no);
            txtViewBatch.setText(Integer.toString(batchNos.get(i)));
            linearLayoutPannelist1Batch.addView(view);
        }

        for (int i = 0; i < batchNos.size(); i++) {
            View view = this.getLayoutInflater().inflate(R.layout.layout_batch_no, linearLayoutPannelist2Batch, false);
            TextView txtViewBatch = (TextView) view.findViewById(R.id.txt_view_batch_no);
            txtViewBatch.setText(Integer.toString(batchNos.get(i)));
            linearLayoutPannelist2Batch.addView(view);
        }

        for (int i = 0; i < batchNos.size(); i++) {
            View view = this.getLayoutInflater().inflate(R.layout.layout_batch_no, linearLayoutPannelist3Batch, false);
            TextView txtViewBatch = (TextView) view.findViewById(R.id.txt_view_batch_no);
            txtViewBatch.setText(Integer.toString(batchNos.get(i)));
            linearLayoutPannelist3Batch.addView(view);
        }

    }

    public void setSpinnerShifts() {
        final ArrayAdapter<String> shiftListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, shiftList){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;            }
        };

        shiftListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerShifts.setAdapter(shiftListAdapter);
        spinnerShifts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                shift = shiftList[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setSpinnerMachines() {
        final ArrayAdapter<String> machineListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, machines){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;            }
        };

        machineListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMachines.setAdapter(machineListAdapter);
        spinnerMachines.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                machineName = machines[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setSignaturePads() {

        signaturePadPannelist1.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
            }

            @Override
            public void onSigned() {
                pannelistSignatureFlag1 = 1;
                signatureCount++;
                Log.e("LogMsg", "Signature Count: " + signatureCount);

            }

            @Override
            public void onClear() {
                pannelistSignatureFlag1 = 0;
                signatureCount--;
            }
        });

        signaturePadPannelist2.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
            }

            @Override
            public void onSigned() {
                pannelistSignatureFlag2 = 1;
                signatureCount++;
                Log.e("LogMsg", "Signature Count: " + signatureCount);

            }

            @Override
            public void onClear() {
                pannelistSignatureFlag2 = 0;
                signatureCount--;
            }
        });

        signaturePadPannelist3.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {
            }

            @Override
            public void onSigned() {
                pannelistSignatureFlag3 = 1;
                signatureCount++;
                Log.e("LogMsg", "Signature Count: " + signatureCount);

            }

            @Override
            public void onClear() {
                pannelistSignatureFlag3 = 0;
                signatureCount--;
            }
        });
    }
    public boolean checkEditTexts() {

        for (int i = 0; i < editTextsPannelist1Appearance.size(); i++) {
            if (editTextsPannelist1Appearance.get(i).getText().toString().trim().isEmpty()){
                return false;
            }
        }

        for (int i = 0; i < editTextsPannelist2Appearance.size(); i++) {
            if (editTextsPannelist2Appearance.get(i).getText().toString().trim().isEmpty()){
                return false;
            }
        }

        for (int i = 0; i < editTextsPannelist3Appearance.size(); i++) {
            if (editTextsPannelist3Appearance.get(i).getText().toString().trim().isEmpty()){
                return false;
            }
        }

        for (int i = 0; i < editTextsPannelist1Taste.size(); i++) {
            if (editTextsPannelist1Taste.get(i).getText().toString().trim().isEmpty()){
                return false;
            }
        }

        for (int i = 0; i < editTextsPannelist2Taste.size(); i++) {
            if (editTextsPannelist2Taste.get(i).getText().toString().trim().isEmpty()){
                return false;
            }
        }

        for (int i = 0; i < editTextsPannelist3Taste.size(); i++) {
            if (editTextsPannelist3Taste.get(i).getText().toString().trim().isEmpty()){
                return false;
            }
        }

        for (int i = 0; i < editTextsPannelist1Aroma.size(); i++) {
            if (editTextsPannelist1Aroma.get(i).getText().toString().trim().isEmpty()){
                return false;
            }
        }

        for (int i = 0; i < editTextsPannelist2Aroma.size(); i++) {
            if (editTextsPannelist2Aroma.get(i).getText().toString().trim().isEmpty()){
                return false;
            }
        }

        for (int i = 0; i < editTextsPannelist3Aroma.size(); i++) {
            if (editTextsPannelist3Aroma.get(i).getText().toString().trim().isEmpty()){
                return false;
            }
        }

        return true;
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {

        androidApplication = (AndroidApplication) getApplicationContext();
        User user = androidApplication.getUser(this);

        if (signatureCount < 3) {
            Toasty.warning(this, "All signatures required", Toast.LENGTH_SHORT, true).show();
        } else if (shift ==  null ||
                    machineName == null ||
                    sku == null ||
                    !checkEditTexts()
                    ) {
            Toasty.warning(this, "Some fields are empty", Toast.LENGTH_SHORT, true).show();
        } else if (pannelistSignatureFlag1 == 1 && editTextPannelist1Name.getText().toString().trim().isEmpty()) {
            Toasty.warning(this, "Please insert pannelist 1 name", Toast.LENGTH_SHORT, true).show();
        } else if (pannelistSignatureFlag2 == 1 && editTextPannelist2Name.getText().toString().trim().isEmpty()) {
            Toasty.warning(this, "Please insert pannelist 2 name", Toast.LENGTH_SHORT, true).show();
        } else if (pannelistSignatureFlag3 == 1 && editTextPannelist3Name.getText().toString().trim().isEmpty()) {
            Toasty.warning(this, "Please insert pannelist 3 name", Toast.LENGTH_SHORT, true).show();
        } else {
            if (pannelistSignatureFlag1 == 1) {
                pannelist1Signature = editTextPannelist1Name.getText().toString() + "_" + getDate() + ".png";
                SaveSignatureHelper.addPngSignatureToGallery(signaturePadPannelist1.getSignatureBitmap(), pannelist1Signature);
            }
            if (pannelistSignatureFlag2 == 1) {
                pannelist2Signature = editTextPannelist2Name.getText().toString() + "_" + getDate() + ".png";
                SaveSignatureHelper.addPngSignatureToGallery(signaturePadPannelist2.getSignatureBitmap(), pannelist2Signature);
            }
            if (pannelistSignatureFlag3 == 1) {
                pannelist3Signature = editTextPannelist3Name.getText().toString() + "_" + getDate() + ".png";
                SaveSignatureHelper.addPngSignatureToGallery(signaturePadPannelist3.getSignatureBitmap(), pannelist3Signature);
            }
            long id = dbHelper.addOnlineSensoryEvaluationRecord(
                        shift,
                        machineName,
                        sku,
                        pannelist1Signature,
                        editTextPannelist1Name.getText().toString(),
                        pannelist2Signature,
                        editTextPannelist2Name.getText().toString(),
                        pannelist3Signature,
                        editTextPannelist3Name.getText().toString(),
                        user.id
                        );
            if (id > 0)
            {
                for (int i = 0; i < batchNos.size(); i++) {
                    dbHelper.addOnlinePannelistData(id, 1, batchNos.get(i), editTextsPannelist1Appearance.get(i).getText().toString(), editTextsPannelist1Taste.get(i).getText().toString(), editTextsPannelist1Aroma.get(i).getText().toString());
                }
                for (int i = 0; i < batchNos.size(); i++) {
                    dbHelper.addOnlinePannelistData(id, 2, batchNos.get(i), editTextsPannelist2Appearance.get(i).getText().toString(), editTextsPannelist2Taste.get(i).getText().toString(), editTextsPannelist2Aroma.get(i).getText().toString());
                }
                for (int i = 0; i < batchNos.size(); i++) {
                    dbHelper.addOnlinePannelistData(id, 3, batchNos.get(i), editTextsPannelist3Appearance.get(i).getText().toString(), editTextsPannelist3Taste.get(i).getText().toString(), editTextsPannelist3Aroma.get(i).getText().toString());
                }
                Toasty.success(this, "Successfully added", Toast.LENGTH_SHORT, true).show();
                finish();
            } else {
                Toasty.error(this, "Something went wrong", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

    @OnClick(R.id.btn_pannelist1_sign_clear) void clearPannelist1Sign() {
        signaturePadPannelist1.clear();
    }

    @OnClick(R.id.btn_pannelist2_sign_clear) void clearPannellist2Sign() {
        signaturePadPannelist2.clear();
    }

    @OnClick(R.id.btn_pannelist3_sign_clear) void clearPannelist3Sign() {
        signaturePadPannelist3.clear();
    }

    @OnClick(R.id.layout_pannelist1) void onPannelist1Clicked() {
        if (layoutPannelist1Signature.getVisibility() == View.GONE) {
            layoutPannelist1Signature.setVisibility(View.VISIBLE);
        } else {
            layoutPannelist1Signature.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.layout_pannelist2) void onPannelist2Clicked() {
        if (layoutPannelist2Signature.getVisibility() == View.GONE) {
            layoutPannelist2Signature.setVisibility(View.VISIBLE);
        } else {
            layoutPannelist2Signature.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.layout_pannelist3) void onPannelist3Clicked() {
        if (layoutPannelist3Signature.getVisibility() == View.GONE) {
            layoutPannelist3Signature.setVisibility(View.VISIBLE);
        } else {
            layoutPannelist3Signature.setVisibility(View.GONE);
        }
    }

    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy_MM_dd_hh_mm_a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

}
