package n.webinfotech.itcreports.activities;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;

public class BakingTimeRecordFormActivity extends AppCompatActivity {

    @BindView(R.id.spinner_shifts)
    Spinner spinnerShifts;
    @BindView(R.id.radio_group_sku)
    RadioGroup radioGroupSKU;
    @BindView(R.id.txt_view_machine_no)
    TextView txtViewMachine;
    String machine;
    String shift;
    String sku;
    String[] shiftList = {
            "Select Shift",
            "A",
            "B",
            "C"
    };
    DBHelper dbHelper;
    AndroidApplication androidApplication;
    @BindView(R.id.edit_text_hmi_hour)
    EditText editTextHMIhour;
    @BindView(R.id.edit_text_hmi_min)
    EditText editTextHMImin;
    @BindView(R.id.edit_text_hmi_sec)
    EditText editTextHMIsec;
    @BindView(R.id.edit_text_bt_hour)
    EditText editTextBThour;
    @BindView(R.id.edit_text_bt_min)
    EditText editTextBTmin;
    @BindView(R.id.edit_text_bt_sec)
    EditText editTextBTsec;
    @BindView(R.id.radio_group_time_hmi)
    RadioGroup radioGroupTimeHMI;
    @BindView(R.id.radio_group_time_bt)
    RadioGroup radioGroupTimeBT;
    String actualBT;
    String btOnHMI;
    String btTime;
    String hmiTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baking_time_record_form);
        ButterKnife.bind(this);
        machine = getIntent().getStringExtra("machineName");
        txtViewMachine.setText(machine);
        initializeDBHelper();
        setUpFields();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void setUpFields() {

        radioGroupSKU.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                sku = checkedRadioButton.getText().toString();
            }
        });

        radioGroupTimeBT.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                btTime = checkedRadioButton.getText().toString();
            }
        });

        radioGroupTimeHMI.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                hmiTime = checkedRadioButton.getText().toString();
            }
        });

        final ArrayAdapter<String> shiftListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, shiftList){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;            }
        };

        shiftListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerShifts.setAdapter(shiftListAdapter);
        spinnerShifts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                shift = shiftList[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @OnClick(R.id.btn_submit) void onSubmitBtnClicked() {
        if (shift == null ||
                sku ==  null ||
                editTextBThour.getText().toString().trim().isEmpty() ||
                editTextBTmin.getText().toString().trim().isEmpty() ||
                editTextBTsec.getText().toString().trim().isEmpty() ||
                editTextHMIhour.getText().toString().trim().isEmpty() ||
                editTextHMImin.getText().toString().trim().isEmpty() ||
                editTextHMIsec.getText().toString().trim().isEmpty() ||
                btTime == null ||
                hmiTime == null
        ) {
            Toasty.warning(this, "Fields missing", Toast.LENGTH_SHORT, true).show();
        } else {
            actualBT = editTextBThour.getText().toString() + ":" + editTextBTmin.getText().toString() + ":" + editTextBTsec.getText().toString() + " " + btTime;
            btOnHMI = editTextHMIhour.getText().toString() + ":" + editTextHMImin.getText().toString() + ":" + editTextHMIsec.getText().toString() + " " + hmiTime;
            androidApplication = (AndroidApplication) getApplicationContext();
            User user = androidApplication.getUser(this);
            if (dbHelper.addBakingTimeRecord(
                    shift,
                    machine,
                    sku,
                    btOnHMI,
                   actualBT,
                    user.id
            )) {
                Toasty.success(this, "Data added successfully", Toast.LENGTH_LONG, true).show();
                finish();
            } else {
                Toasty.error(this, "Failed to insert data", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

}
