package n.webinfotech.itcreports.activities;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.SaveSignatureHelper;

import static n.webinfotech.itcreports.util.Helper.getDate;

public class StartUpCheckListActivity extends AppCompatActivity {

    @BindView(R.id.edit_text_final_status_sieve_1)
    EditText editTextFinalStatusSieve1;
    @BindView(R.id.edit_text_final_status_sieve_2)
    EditText editTextFinalStatusSieve2;
    @BindView(R.id.edit_text_final_status_sieve_3)
    EditText editTextFinalStatusSieve3;
    @BindView(R.id.edit_text_final_status_sieve_4)
    EditText editTextFinalStatusSieve4;
    @BindView(R.id.toggle_btn_sieve1)
    ToggleButton toggleButtonSieve1;
    @BindView(R.id.toggle_btn_sieve2)
    ToggleButton toggleButtonSieve2;
    @BindView(R.id.toggle_btn_sieve3)
    ToggleButton toggleButtonSieve3;
    @BindView(R.id.toggle_btn_sieve4)
    ToggleButton toggleButtonSieve4;
    @BindView(R.id.edit_text_remarks_sieve_1)
    EditText editTextRemarksSieve1;
    @BindView(R.id.edit_text_remarks_sieve_2)
    EditText editTextRemarksSieve2;
    @BindView(R.id.edit_text_remarks_sieve_3)
    EditText editTextRemarksSieve3;
    @BindView(R.id.edit_text_remarks_sieve_4)
    EditText editTextRemarksSieve4;
    @BindView(R.id.edit_text_action_taken_sieve_1)
    EditText editTextActionTakenSieve1;
    @BindView(R.id.edit_text_action_taken_sieve_2)
    EditText editTextActionTakenSieve2;
    @BindView(R.id.edit_text_action_taken_sieve_3)
    EditText editTextActionTakenSieve3;
    @BindView(R.id.edit_text_action_taken_sieve_4)
    EditText editTextActionTakenSieve4;
    @BindView(R.id.toggle_btn_packing1)
    ToggleButton toggleButtonPacking1;
    @BindView(R.id.toggle_btn_packing2)
    ToggleButton getToggleButtonPacking2;
    @BindView(R.id.toggle_btn_packing3)
    ToggleButton toggleBtnPacking3;
    @BindView(R.id.toggle_btn_packing4)
    ToggleButton toggleBtnPacking4;
    @BindView(R.id.toggle_btn_packing5)
    ToggleButton toggleBtnPacking5;
    @BindView(R.id.toggle_btn_packing6)
    ToggleButton toggleBtnPacking6;
    @BindView(R.id.toggle_btn_packing7)
    ToggleButton toggleBtnPacking7;
    @BindView(R.id.toggle_btn_packing8)
    ToggleButton toggleBtnPacking8;
    @BindView(R.id.toggle_btn_packing9)
    ToggleButton toggleBtnPacking9;
    @BindView(R.id.toggle_btn_packing10)
    ToggleButton toggleBtnPacking10;
    @BindView(R.id.toggle_btn_packing11)
    ToggleButton toggleBtnPacking11;
    @BindView(R.id.toggle_btn_packing12)
    ToggleButton toggleBtnPacking12;
    @BindView(R.id.toggle_btn_packing13)
    ToggleButton toggleBtnPacking13;
    @BindView(R.id.toggle_btn_packing14)
    ToggleButton toggleBtnPacking14;
    @BindView(R.id.toggle_btn_packing15)
    ToggleButton toggleBtnPacking15;
    @BindView(R.id.toggle_btn_packing16)
    ToggleButton toggleBtnPacking16;
    @BindView(R.id.toggle_btn_packing17)
    ToggleButton toggleBtnPacking17;
    @BindView(R.id.toggle_btn_packing18)
    ToggleButton toggleBtnPacking18;
    @BindView(R.id.toggle_btn_packing19)
    ToggleButton toggleBtnPacking19;
    @BindView(R.id.toggle_btn_packing20)
    ToggleButton toggleBtnPacking20;
    @BindView(R.id.toggle_btn_packing21)
    ToggleButton toggleBtnPacking21;
    @BindView(R.id.toggle_btn_packing22)
    ToggleButton toggleBtnPacking22;
    @BindView(R.id.toggle_btn_packing23)
    ToggleButton toggleBtnPacking23;
    @BindView(R.id.toggle_btn_packing24)
    ToggleButton toggleBtnPacking24;
    @BindView(R.id.edit_text_remarks_packing_1)
    EditText editTextRemarksPacking1;
    @BindView(R.id.edit_text_remarks_packing_2)
    EditText editTextRemarksPacking2;
    @BindView(R.id.edit_text_remarks_packing_3)
    EditText editTextRemarksPacking3;
    @BindView(R.id.edit_text_remarks_packing_4)
    EditText editTextRemarksPacking4;
    @BindView(R.id.edit_text_remarks_packing_5)
    EditText editTextRemarksPacking5;
    @BindView(R.id.edit_text_remarks_packing_6)
    EditText editTextRemarksPacking6;
    @BindView(R.id.edit_text_remarks_packing_7)
    EditText editTextRemarksPacking7;
    @BindView(R.id.edit_text_remarks_packing_8)
    EditText editTextRemarksPacking8;
    @BindView(R.id.edit_text_remarks_packing_9)
    EditText editTextRemarksPacking9;
    @BindView(R.id.edit_text_remarks_packing_10)
    EditText editTextRemarksPacking10;
    @BindView(R.id.edit_text_remarks_packing_11)
    EditText editTextRemarksPacking11;
    @BindView(R.id.edit_text_remarks_packing_12)
    EditText editTextRemarksPacking12;
    @BindView(R.id.edit_text_remarks_packing_13)
    EditText editTextRemarksPacking13;
    @BindView(R.id.edit_text_remarks_packing_14)
    EditText editTextRemarksPacking14;
    @BindView(R.id.edit_text_remarks_packing_15)
    EditText editTextRemarksPacking15;
    @BindView(R.id.edit_text_remarks_packing_16)
    EditText editTextRemarksPacking16;
    @BindView(R.id.edit_text_remarks_packing_17)
    EditText editTextRemarksPacking17;
    @BindView(R.id.edit_text_remarks_packing_18)
    EditText editTextRemarksPacking18;
    @BindView(R.id.edit_text_remarks_packing_19)
    EditText editTextRemarksPacking19;
    @BindView(R.id.edit_text_remarks_packing_20)
    EditText editTextRemarksPacking20;
    @BindView(R.id.edit_text_remarks_packing_21)
    EditText editTextRemarksPacking21;
    @BindView(R.id.edit_text_remarks_packing_22)
    EditText editTextRemarksPacking22;
    @BindView(R.id.edit_text_remarks_packing_23)
    EditText editTextRemarksPacking23;
    @BindView(R.id.edit_text_remarks_packing_24)
    EditText editTextRemarksPacking24;
    @BindView(R.id.edit_text_action_taken_packing_1)
    EditText editTextActionTakenPacking1;
    @BindView(R.id.edit_text_action_taken_packing_2)
    EditText editTextActionTakenPacking2;
    @BindView(R.id.edit_text_action_taken_packing_3)
    EditText editTextActionTakenPacking3;
    @BindView(R.id.edit_text_action_taken_packing_4)
    EditText editTextActionTakenPacking4;
    @BindView(R.id.edit_text_action_taken_packing_5)
    EditText editTextActionTakenPacking5;
    @BindView(R.id.edit_text_action_taken_packing_6)
    EditText editTextActionTakenPacking6;
    @BindView(R.id.edit_text_action_taken_packing_7)
    EditText editTextActionTakenPacking7;
    @BindView(R.id.edit_text_action_taken_packing_8)
    EditText editTextActionTakenPacking8;
    @BindView(R.id.edit_text_action_taken_packing_9)
    EditText editTextActionTakenPacking9;
    @BindView(R.id.edit_text_action_taken_packing_10)
    EditText editTextActionTakenPacking10;
    @BindView(R.id.edit_text_action_taken_packing_11)
    EditText editTextActionTakenPacking11;
    @BindView(R.id.edit_text_action_taken_packing_12)
    EditText editTextActionTakenPacking12;
    @BindView(R.id.edit_text_action_taken_packing_13)
    EditText editTextActionTakenPacking13;
    @BindView(R.id.edit_text_action_taken_packing_14)
    EditText editTextActionTakenPacking14;
    @BindView(R.id.edit_text_action_taken_packing_15)
    EditText editTextActionTakenPacking15;
    @BindView(R.id.edit_text_action_taken_packing_16)
    EditText editTextActionTakenPacking16;
    @BindView(R.id.edit_text_action_taken_packing_17)
    EditText editTextActionTakenPacking17;
    @BindView(R.id.edit_text_action_taken_packing_18)
    EditText editTextActionTakenPacking18;
    @BindView(R.id.edit_text_action_taken_packing_19)
    EditText editTextActionTakenPacking19;
    @BindView(R.id.edit_text_action_taken_packing_20)
    EditText editTextActionTakenPacking20;
    @BindView(R.id.edit_text_action_taken_packing_21)
    EditText editTextActionTakenPacking21;
    @BindView(R.id.edit_text_action_taken_packing_22)
    EditText editTextActionTakenPacking22;
    @BindView(R.id.edit_text_action_taken_packing_23)
    EditText editTextActionTakenPacking23;
    @BindView(R.id.edit_text_action_taken_packing_24)
    EditText editTextActionTakenPacking24;
    @BindView(R.id.edit_text_final_status_packing_1)
    EditText editTextFinalStatusPacking1;
    @BindView(R.id.edit_text_final_status_packing_2)
    EditText editTextFinalStatusPacking2;
    @BindView(R.id.edit_text_final_status_packing_3)
    EditText editTextFinalStatusPacking3;
    @BindView(R.id.edit_text_final_status_packing_4)
    EditText editTextFinalStatusPacking4;
    @BindView(R.id.edit_text_final_status_packing_5)
    EditText editTextFinalStatusPacking5;
    @BindView(R.id.edit_text_final_status_packing_6)
    EditText editTextFinalStatusPacking6;
    @BindView(R.id.edit_text_final_status_packing_7)
    EditText editTextFinalStatusPacking7;
    @BindView(R.id.edit_text_final_status_packing_8)
    EditText editTextFinalStatusPacking8;
    @BindView(R.id.edit_text_final_status_packing_9)
    EditText editTextFinalStatusPacking9;
    @BindView(R.id.edit_text_final_status_packing_10)
    EditText editTextFinalStatusPacking10;
    @BindView(R.id.edit_text_final_status_packing_11)
    EditText editTextFinalStatusPacking11;
    @BindView(R.id.edit_text_final_status_packing_12)
    EditText editTextFinalStatusPacking12;
    @BindView(R.id.edit_text_final_status_packing_13)
    EditText editTextFinalStatusPacking13;
    @BindView(R.id.edit_text_final_status_packing_14)
    EditText editTextFinalStatusPacking14;
    @BindView(R.id.edit_text_final_status_packing_15)
    EditText editTextFinalStatusPacking15;
    @BindView(R.id.edit_text_final_status_packing_16)
    EditText editTextFinalStatusPacking16;
    @BindView(R.id.edit_text_final_status_packing_17)
    EditText editTextFinalStatusPacking17;
    @BindView(R.id.edit_text_final_status_packing_18)
    EditText editTextFinalStatusPacking18;
    @BindView(R.id.edit_text_final_status_packing_19)
    EditText editTextFinalStatusPacking19;
    @BindView(R.id.edit_text_final_status_packing_20)
    EditText editTextFinalStatusPacking20;
    @BindView(R.id.edit_text_final_status_packing_21)
    EditText editTextFinalStatusPacking21;
    @BindView(R.id.edit_text_final_status_packing_22)
    EditText editTextFinalStatusPacking22;
    @BindView(R.id.edit_text_final_status_packing_23)
    EditText editTextFinalStatusPacking23;
    @BindView(R.id.edit_text_final_status_packing_24)
    EditText editTextFinalStatusPacking24;
    @BindView(R.id.toggle_btn_oven1)
    ToggleButton toggleButtonOven1;
    @BindView(R.id.toggle_btn_oven2)
    ToggleButton toggleButtonOven2;
    @BindView(R.id.toggle_btn_oven3)
    ToggleButton toggleButtonOven3;
    @BindView(R.id.toggle_btn_oven4)
    ToggleButton toggleButtonOven4;
    @BindView(R.id.toggle_btn_oven5)
    ToggleButton toggleButtonOven5;
    @BindView(R.id.edit_text_remarks_oven_1)
    EditText editTextRemarksOven1;
    @BindView(R.id.edit_text_remarks_oven_2)
    EditText editTextRemarksOven2;
    @BindView(R.id.edit_text_remarks_oven_3)
    EditText editTextRemarksOven3;
    @BindView(R.id.edit_text_remarks_oven_4)
    EditText editTextRemarksOven4;
    @BindView(R.id.edit_text_remarks_oven_5)
    EditText editTextRemarksOven5;
    @BindView(R.id.edit_text_action_taken_oven_1)
    EditText editTextActionTakenOven1;
    @BindView(R.id.edit_text_action_taken_oven_2)
    EditText editTextActionTakenOven2;
    @BindView(R.id.edit_text_action_taken_oven_3)
    EditText editTextActionTakenOven3;
    @BindView(R.id.edit_text_action_taken_oven_4)
    EditText editTextActionTakenOven4;
    @BindView(R.id.edit_text_action_taken_oven_5)
    EditText editTextActionTakenOven5;
    @BindView(R.id.edit_text_final_status_oven_1)
    EditText editTextFinalStatusOven1;
    @BindView(R.id.edit_text_final_status_oven_2)
    EditText editTextFinalStatusOven2;
    @BindView(R.id.edit_text_final_status_oven_3)
    EditText editTextFinalStatusOven3;
    @BindView(R.id.edit_text_final_status_oven_4)
    EditText editTextFinalStatusOven4;
    @BindView(R.id.edit_text_final_status_oven_5)
    EditText editTextFinalStatusOven5;
    @BindView(R.id.toggle_btn_vp1)
    ToggleButton toggleButtonVP1;
    @BindView(R.id.toggle_btn_vp2)
    ToggleButton toggleButtonVP2;
    @BindView(R.id.toggle_btn_vp3)
    ToggleButton toggleButtonVP3;
    @BindView(R.id.toggle_btn_vp4)
    ToggleButton toggleButtonVP4;
    @BindView(R.id.toggle_btn_vp5)
    ToggleButton toggleButtonVP5;
    @BindView(R.id.toggle_btn_vp6)
    ToggleButton toggleButtonVP6;
    @BindView(R.id.toggle_btn_vp7)
    ToggleButton toggleButtonVP7;
    @BindView(R.id.edit_text_remarks_vp_1)
    EditText editTextRemarksVP1;
    @BindView(R.id.edit_text_remarks_vp_2)
    EditText editTextRemarksVP2;
    @BindView(R.id.edit_text_remarks_vp_3)
    EditText editTextRemarksVP3;
    @BindView(R.id.edit_text_remarks_vp_4)
    EditText editTextRemarksVP4;
    @BindView(R.id.edit_text_remarks_vp_5)
    EditText editTextRemarksVP5;
    @BindView(R.id.edit_text_remarks_vp_6)
    EditText editTextRemarksVP6;
    @BindView(R.id.edit_text_remarks_vp_7)
    EditText editTextRemarksVP7;
    @BindView(R.id.edit_text_action_taken_vp_1)
    EditText editTextActionTakenVP1;
    @BindView(R.id.edit_text_action_taken_vp_2)
    EditText editTextActionTakenVP2;
    @BindView(R.id.edit_text_action_taken_vp_3)
    EditText editTextActionTakenVP3;
    @BindView(R.id.edit_text_action_taken_vp_4)
    EditText editTextActionTakenVP4;
    @BindView(R.id.edit_text_action_taken_vp_5)
    EditText editTextActionTakenVP5;
    @BindView(R.id.edit_text_action_taken_vp_6)
    EditText editTextActionTakenVP6;
    @BindView(R.id.edit_text_action_taken_vp_7)
    EditText editTextActionTakenVP7;
    @BindView(R.id.edit_text_final_status_vp_1)
    EditText editTextFinalStatusVP1;
    @BindView(R.id.edit_text_final_status_vp_2)
    EditText editTextFinalStatusVP2;
    @BindView(R.id.edit_text_final_status_vp_3)
    EditText editTextFinalStatusVP3;
    @BindView(R.id.edit_text_final_status_vp_4)
    EditText editTextFinalStatusVP4;
    @BindView(R.id.edit_text_final_status_vp_5)
    EditText editTextFinalStatusVP5;
    @BindView(R.id.edit_text_final_status_vp_6)
    EditText editTextFinalStatusVP6;
    @BindView(R.id.edit_text_final_status_vp_7)
    EditText editTextFinalStatusVP7;
    @BindView(R.id.spinner_machines)
    Spinner spinnerMachines;
    String[] machines = {
            "Select Module",
            "M - 1",
            "M - 2"
    };
    String machineName;
    DBHelper dbHelper;
    AndroidApplication androidApplication;
    @BindView(R.id.signature_pad_quality_manager)
    SignaturePad qualityManagerSiganturePad;
    @BindView(R.id.layout_quality_manager_signature)
    View layoutQualityManager;
    @BindView(R.id.btn_quality_manager_sign_clear)
    Button btnQualityManagerClear;
    int signatureFlag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_up_check_list);
        ButterKnife.bind(this);
        setSpinnerMachines();
        initializeDBHelper();
        setUpQualityManager();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setSpinnerMachines() {
        final ArrayAdapter<String> machineListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, machines){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;            }
        };

        machineListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMachines.setAdapter(machineListAdapter);
        spinnerMachines.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                machineName = machines[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setUpQualityManager() {
        qualityManagerSiganturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                signatureFlag = 1;
            }

            @Override
            public void onClear() {
                signatureFlag = 0;
            }
        });
    }

    @OnClick(R.id.btn_submit) void submitClicked() {

        if (signatureFlag == 0) {
            Toasty.warning(this, "Please add quality manager signature", Toast.LENGTH_SHORT, true).show();
        } else {
            String qualityManagerSignature = "quality_manager_signature_" + getDate() + ".png";
            SaveSignatureHelper.addPngSignatureToGallery(qualityManagerSiganturePad.getSignatureBitmap(), qualityManagerSignature);
            androidApplication = (AndroidApplication) getApplicationContext();
            User user = androidApplication.getUser(this);
            long mainId = dbHelper.insertStartupChecklist(machineName, qualityManagerSignature, user.id);

//        Check the condition of seives aand magnets
            long sieve1DataId = dbHelper.insertStartupCheckListData(toggleButtonSieve1.getText().toString(), editTextRemarksSieve1.getText().toString(), editTextActionTakenSieve1.getText().toString(), editTextFinalStatusSieve1.getText().toString());

//       All materials are stored on pallets
            long sieve2DataId = dbHelper.insertStartupCheckListData(toggleButtonSieve2.getText().toString(), editTextRemarksSieve2.getText().toString(), editTextActionTakenSieve2.getText().toString(), editTextFinalStatusSieve2.getText().toString());

//        Floor Cleaning
            long sieve3DataId = dbHelper.insertStartupCheckListData(toggleButtonSieve3.getText().toString(), editTextRemarksSieve3.getText().toString(), editTextActionTakenSieve3.getText().toString(), editTextFinalStatusSieve3.getText().toString());

//        Condition of PVC Stripes
            long sieve4DataId = dbHelper.insertStartupCheckListData(toggleButtonSieve4.getText().toString(), editTextRemarksSieve4.getText().toString(), editTextActionTakenSieve4.getText().toString(), editTextFinalStatusSieve4.getText().toString());

//        Availability of IPA & Lint Roller
            long packing1DataId = dbHelper.insertStartupCheckListData(toggleButtonPacking1.getText().toString(), editTextRemarksPacking1.getText().toString(), editTextActionTakenPacking1.getText().toString(), editTextFinalStatusPacking1.getText().toString());

            long packing2DataId = dbHelper.insertStartupCheckListData(getToggleButtonPacking2.getText().toString(), editTextRemarksPacking2.getText().toString(), editTextActionTakenPacking2.getText().toString(), editTextFinalStatusPacking2.getText().toString());

            long packing3DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking3.getText().toString(), editTextRemarksPacking3.getText().toString(), editTextActionTakenPacking3.getText().toString(), editTextFinalStatusPacking3.getText().toString());

            long packing4DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking4.getText().toString(), editTextRemarksPacking4.getText().toString(), editTextActionTakenPacking4.getText().toString(), editTextFinalStatusPacking4.getText().toString());

            long packing5DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking5.getText().toString(), editTextRemarksPacking5.getText().toString(), editTextActionTakenPacking5.getText().toString(), editTextFinalStatusPacking5.getText().toString());

            long packing6DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking6.getText().toString(), editTextRemarksPacking6.getText().toString(), editTextActionTakenPacking6.getText().toString(), editTextFinalStatusPacking6.getText().toString());

            long packing7DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking7.getText().toString(), editTextRemarksPacking7.getText().toString(), editTextActionTakenPacking7.getText().toString(), editTextFinalStatusPacking7.getText().toString());

            long packing8DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking8.getText().toString(), editTextRemarksPacking8.getText().toString(), editTextActionTakenPacking8.getText().toString(), editTextFinalStatusPacking8.getText().toString());

            long packing9DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking9.getText().toString(), editTextRemarksPacking9.getText().toString(), editTextActionTakenPacking9.getText().toString(), editTextFinalStatusPacking9.getText().toString());

            long packing10DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking10.getText().toString(), editTextRemarksPacking10.getText().toString(), editTextActionTakenPacking10.getText().toString(), editTextFinalStatusPacking10.getText().toString());

            long packing11DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking11.getText().toString(), editTextRemarksPacking11.getText().toString(), editTextActionTakenPacking11.getText().toString(), editTextFinalStatusPacking11.getText().toString());

            long packing12DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking12.getText().toString(), editTextRemarksPacking12.getText().toString(), editTextActionTakenPacking12.getText().toString(), editTextFinalStatusPacking12.getText().toString());

            long packing13DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking13.getText().toString(), editTextRemarksPacking13.getText().toString(), editTextActionTakenPacking13.getText().toString(), editTextFinalStatusPacking13.getText().toString());

            long packing14DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking14.getText().toString(), editTextRemarksPacking14.getText().toString(), editTextActionTakenPacking14.getText().toString(), editTextFinalStatusPacking14.getText().toString());

            long packing15DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking15.getText().toString(), editTextRemarksPacking15.getText().toString(), editTextActionTakenPacking15.getText().toString(), editTextFinalStatusPacking15.getText().toString());

            long packing16DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking16.getText().toString(), editTextRemarksPacking16.getText().toString(), editTextActionTakenPacking16.getText().toString(), editTextFinalStatusPacking16.getText().toString());

            long packing17DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking17.getText().toString(), editTextRemarksPacking17.getText().toString(), editTextActionTakenPacking17.getText().toString(), editTextFinalStatusPacking17.getText().toString());

            long packing18DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking18.getText().toString(), editTextRemarksPacking18.getText().toString(), editTextActionTakenPacking18.getText().toString(), editTextFinalStatusPacking18.getText().toString());

            long packing19DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking19.getText().toString(), editTextRemarksPacking19.getText().toString(), editTextActionTakenPacking19.getText().toString(), editTextFinalStatusPacking19.getText().toString());

            long packing20DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking20.getText().toString(), editTextRemarksPacking20.getText().toString(), editTextActionTakenPacking20.getText().toString(), editTextFinalStatusPacking20.getText().toString());

            long packing21DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking21.getText().toString(), editTextRemarksPacking21.getText().toString(), editTextActionTakenPacking21.getText().toString(), editTextFinalStatusPacking21.getText().toString());

            long packing22DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking22.getText().toString(), editTextRemarksPacking22.getText().toString(), editTextActionTakenPacking22.getText().toString(), editTextFinalStatusPacking22.getText().toString());

            long packing23DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking23.getText().toString(), editTextRemarksPacking23.getText().toString(), editTextActionTakenPacking23.getText().toString(), editTextFinalStatusPacking23.getText().toString());

            long packing24DataId = dbHelper.insertStartupCheckListData(toggleBtnPacking24.getText().toString(), editTextRemarksPacking24.getText().toString(), editTextActionTakenPacking24.getText().toString(), editTextFinalStatusPacking24.getText().toString());

            long oven1DataId = dbHelper.insertStartupCheckListData(toggleButtonOven1.getText().toString(), editTextRemarksOven1.getText().toString(), editTextActionTakenOven1.getText().toString(), editTextFinalStatusOven1.getText().toString());

            long oven2DataId = dbHelper.insertStartupCheckListData(toggleButtonOven2.getText().toString(), editTextRemarksOven2.getText().toString(), editTextActionTakenOven2.getText().toString(), editTextFinalStatusOven2.getText().toString());

            long oven3DataId = dbHelper.insertStartupCheckListData(toggleButtonOven3.getText().toString(), editTextRemarksOven3.getText().toString(), editTextActionTakenOven3.getText().toString(), editTextFinalStatusOven3.getText().toString());

            long oven4DataId = dbHelper.insertStartupCheckListData(toggleButtonOven4.getText().toString(), editTextRemarksOven4.getText().toString(), editTextActionTakenOven4.getText().toString(), editTextFinalStatusOven4.getText().toString());

            long oven5DataId = dbHelper.insertStartupCheckListData(toggleButtonOven5.getText().toString(), editTextRemarksOven5.getText().toString(), editTextActionTakenOven5.getText().toString(), editTextFinalStatusOven5.getText().toString());

            long vp1DataId = dbHelper.insertStartupCheckListData(toggleButtonVP1.getText().toString(), editTextRemarksVP1.getText().toString(), editTextActionTakenVP1.getText().toString(), editTextFinalStatusVP1.getText().toString());

            long vp2DataId = dbHelper.insertStartupCheckListData(toggleButtonVP2.getText().toString(), editTextRemarksVP2.getText().toString(), editTextActionTakenVP2.getText().toString(), editTextFinalStatusVP2.getText().toString());

            long vp3DataId = dbHelper.insertStartupCheckListData(toggleButtonVP3.getText().toString(), editTextRemarksVP3.getText().toString(), editTextActionTakenVP3.getText().toString(), editTextFinalStatusVP3.getText().toString());

            long vp4DataId = dbHelper.insertStartupCheckListData(toggleButtonVP4.getText().toString(), editTextRemarksVP4.getText().toString(), editTextActionTakenVP4.getText().toString(), editTextFinalStatusVP4.getText().toString());

            long vp5DataId = dbHelper.insertStartupCheckListData(toggleButtonVP5.getText().toString(), editTextRemarksVP5.getText().toString(), editTextActionTakenVP5.getText().toString(), editTextFinalStatusVP5.getText().toString());

            long vp6DataId = dbHelper.insertStartupCheckListData(toggleButtonVP6.getText().toString(), editTextRemarksVP6.getText().toString(), editTextActionTakenVP6.getText().toString(), editTextFinalStatusVP6.getText().toString());

            long vp7DataId = dbHelper.insertStartupCheckListData(toggleButtonVP7.getText().toString(), editTextRemarksVP7.getText().toString(), editTextActionTakenVP7.getText().toString(), editTextFinalStatusVP7.getText().toString());

            long updateDataId = dbHelper.updateStartupCheckListData(mainId,
                    sieve1DataId,
                    sieve2DataId,
                    sieve3DataId,
                    sieve4DataId,
                    packing1DataId,
                    packing2DataId,
                    packing3DataId,
                    packing4DataId,
                    packing5DataId,
                    packing6DataId,
                    packing7DataId,
                    packing8DataId,
                    packing9DataId,
                    packing10DataId,
                    packing11DataId,
                    packing12DataId,
                    packing13DataId,
                    packing14DataId,
                    packing15DataId,
                    packing16DataId,
                    packing17DataId,
                    packing18DataId,
                    packing19DataId,
                    packing20DataId,
                    packing21DataId,
                    packing22DataId,
                    packing23DataId,
                    packing24DataId,
                    oven1DataId,
                    oven2DataId,
                    oven3DataId,
                    oven4DataId,
                    oven5DataId,
                    vp1DataId,
                    vp2DataId,
                    vp3DataId,
                    vp4DataId,
                    vp5DataId,
                    vp6DataId,
                    vp7DataId
            );
            if (updateDataId > 0) {
                Toasty.success(this, "Successfully added", Toast.LENGTH_SHORT).show();
                finish();
            } else {
                Toasty.error(this, "Failed to add data", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.layout_quality_manager) void onQualityManagerClicked() {
        if (layoutQualityManager.getVisibility() == View.VISIBLE) {
            layoutQualityManager.setVisibility(View.GONE);
            btnQualityManagerClear.setVisibility(View.GONE);
        } else {
            layoutQualityManager.setVisibility(View.VISIBLE);
            btnQualityManagerClear.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btn_quality_manager_sign_clear) void onQualityMangerSignClearClicked() {
        qualityManagerSiganturePad.clear();
    }

}
