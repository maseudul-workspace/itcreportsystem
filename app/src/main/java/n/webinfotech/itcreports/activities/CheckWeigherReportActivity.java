package n.webinfotech.itcreports.activities;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.CheckWeigherDataAdapter;
import n.webinfotech.itcreports.adapters.CheckWeigherVerifyAdapter;
import n.webinfotech.itcreports.dialogs.CheckWeigherEditDialog;
import n.webinfotech.itcreports.models.CheckWeigher;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.Helper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;
import static n.webinfotech.itcreports.util.Helper.getByteFromFile;

public class CheckWeigherReportActivity extends AppCompatActivity implements CheckWeigherEditDialog.Callback, CheckWeigherDataAdapter.Callback {

    DBHelper dbHelper;String startDate;
    String endDate;
    @BindView(R.id.txt_view_start_date)
    TextView txtViewStartDate;
    @BindView(R.id.txt_view_end_date)
    TextView txtViewEndDate;
    @BindView(R.id.recycler_view_checkweigher_data)
    RecyclerView recyclerView;
    Cursor cursor;
    CheckWeigherEditDialog checkWeigherEditDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_weigher_report);
        ButterKnife.bind(this);
        initializeDBHelper();
        startDate = Helper.getSearchCurrentDate();
        endDate = Helper.getSearchNextDate();
        cursor = dbHelper.getCheckWeigherData(startDate, endDate);
        txtViewStartDate.setText(startDate);
        txtViewEndDate.setText(endDate);
        if (cursor.getCount() == 0) {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
        setRecyclerView(cursor);
        setUpDialog();
    }

    public void setUpDialog() {
        checkWeigherEditDialog = new CheckWeigherEditDialog(this, this, this);
        checkWeigherEditDialog.setUpDialog();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void saveData() {
        File sd = Environment.getExternalStorageDirectory();
        String csvFile = getDate() + "check_weigher_bin_record.xls";

        File directory = new File(sd.getAbsolutePath()+"/ITCReports/CheckWeigherBinRecord");
        //create directory if not exist
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
        //create directory if not exist
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
        try {

            //file path
            File file = new File(directory, csvFile);
            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setLocale(new Locale("en", "EN"));
            WritableWorkbook workbook;
            workbook = Workbook.createWorkbook(file, wbSettings);
            //Excel sheet name. 0 represents first sheet
            WritableSheet sheet = workbook.createSheet("Check Weigher Report", 0);


//            Excel Header
//            Add logo
            byte[] logoBytes = Helper.getByteFromDrawable(getDrawable(R.drawable.itc_logo));
            WritableImage writableImageLogo = new WritableImage(0, 0, 2,4, logoBytes);
            sheet.addImage(writableImageLogo);

//            Add header
            byte[] headerBytes = Helper.getByteFromDrawable(getDrawable(R.drawable.checkweigher));
            WritableImage headerLogo = new WritableImage(2, 0, 6,4, headerBytes);
            sheet.addImage(headerLogo);

            sheet.addCell(new Label(8, 1, "Document No"));

            for(int i = 2; i < 10; i++) {
                CellView cell=sheet.getColumnView(i);
                cell.setAutosize(true);
                sheet.setColumnView(i, cell);
            }

            sheet.addCell(new Label(9, 1, "  "));


            sheet.addCell(new Label(8, 2, "Revision No"));
            sheet.addCell(new Label(9, 2, "  "));

            sheet.addCell(new Label(8, 3, "Effective Date"));
            sheet.addCell(new Label(9, 3, "  "));


            WritableFont arial10pt = new WritableFont(WritableFont.ARIAL, 9);
            arial10pt.setBoldStyle(WritableFont.BOLD);
            WritableCellFormat cellFormat = new WritableCellFormat(arial10pt);
            sheet.mergeCells(0,4,9,4);
            sheet.addCell(new Label(0, 4, "Frequency: Once In a Shift", cellFormat));

            sheet.addCell(new Label(0, 5, "Date", cellFormat));

            sheet.addCell(new Label(1, 5, "Shift", cellFormat));

            sheet.addCell(new Label(2, 5, "SKU", cellFormat));

            sheet.addCell(new Label(3, 5, " Time ", cellFormat));

            sheet.addCell(new Label(4, 5, " Module ", cellFormat));

            sheet.addCell(new Label(5, 5, " Less Count ", cellFormat));

            sheet.addCell(new Label(6, 5, " Flap Open ", cellFormat));

            sheet.addCell(new Label(7, 5, "  Mis Coding  ", cellFormat));

            sheet.addCell(new Label(8, 5, "  Checked By  ", cellFormat));

            sheet.addCell(new Label(9, 5, "  Verified By  ", cellFormat));

            int row_count = 6;

            Cursor c = dbHelper.getCheckWeigherData(startDate, endDate);
            if (c.getCount() > 0) {
                Log.e("LogMsg", "Data vcount: " + c.getCount());
                while (c.moveToNext()) {
                    if (c.getInt(c.getColumnIndex("verify_status")) == 2) {
                        String sign = dbHelper.getVerifierSignature(c.getInt(c.getColumnIndex("verify_id")));
                        if (sign != null) {
                            byte[] signatureBytes = getByteFromFile(sign);
                            WritableImage signature = new WritableImage(9, row_count, 1, 1, signatureBytes);
                            sheet.addImage(signature);
                        }
                    }
                    sheet.addCell(new Label(0, row_count, c.getString(c.getColumnIndex("date"))));
                    sheet.addCell(new Label(1, row_count, c.getString(c.getColumnIndex("shift"))));
                    sheet.addCell(new Label(2, row_count, c.getString(c.getColumnIndex("sku"))));
                    sheet.addCell(new Label(3, row_count, c.getString(c.getColumnIndex("time"))));
                    sheet.addCell(new Label(4, row_count, c.getString(c.getColumnIndex("rej_pkd"))));
                    sheet.addCell(new Label(5, row_count, c.getString(c.getColumnIndex("less_count_pkt"))));
                    sheet.addCell(new Label(6, row_count, c.getString(c.getColumnIndex("flap_open"))));
                    sheet.addCell(new Label(7, row_count, c.getString(c.getColumnIndex("mis_coading"))));
                    sheet.addCell(new Label(8, row_count, c.getString(c.getColumnIndex("u_name"))));
                    row_count++;
                }
            }
            workbook.write();
            workbook.close();
            Toast.makeText(this,
                    "Data Exported in a Excel Sheet", Toast.LENGTH_SHORT).show();
        } catch(Exception e){
            Log.e("LogMsg", "Workbook error" + e.getMessage());
            e.printStackTrace();
        }
    }

    @OnClick(R.id.img_view_calendar_start_date) void onCalendarStartDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        startDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        startDate = Helper.changeDateFormat(startDate);
                        txtViewStartDate.setText(startDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.img_view_calendar_end_date) void onCalendarEndDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        endDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        endDate = Helper.changeDateFormat(endDate);
                        txtViewEndDate.setText(endDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.btn_export) void onExportClicked() {
        saveData();
    }
    @OnClick(R.id.btn_search) void onSearchClicked() {
        if (startDate == null || endDate == null){
            Toasty.warning(this, "Please enter both dates", Toast.LENGTH_SHORT).show();
        } else {
            cursor = dbHelper.getCheckWeigherData(startDate, endDate);
            Log.e("LogMsg", "Cursor Count: " + cursor.getCount());
            setRecyclerView(cursor);
            if (cursor.getCount() == 0) {
                Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy_MM_dd_hh_mm_a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void setRecyclerView(Cursor c) {
        if (c.getCount() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            ArrayList<CheckWeigher> checkWeighers = new ArrayList<>();
            while (c.moveToNext()) {
                CheckWeigher checkWeigher = new CheckWeigher(
                        c.getInt(c.getColumnIndex("id")),
                        c.getString(c.getColumnIndex("date")),
                        c.getString(c.getColumnIndex("time")),
                        c.getString(c.getColumnIndex("shift")),
                        c.getString(c.getColumnIndex("sku")),
                        c.getString(c.getColumnIndex("rej_pkd")),
                        c.getString(c.getColumnIndex("less_count_pkt")),
                        c.getString(c.getColumnIndex("flap_open")),
                        c.getString(c.getColumnIndex("mis_coading")),
                        c.getInt(c.getColumnIndex("user_id")),
                        c.getString(c.getColumnIndex("u_name")),
                        c.getInt(c.getColumnIndex("verify_status")),
                        c.getInt(c.getColumnIndex("verify_id"))
                );
                checkWeighers.add(checkWeigher);
            }
            CheckWeigherDataAdapter adapter = new CheckWeigherDataAdapter(this, checkWeighers, this);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
            recyclerView.addItemDecoration(itemDecor);
        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onEditClicked(int id) {
        checkWeigherEditDialog.setData(dbHelper.getSingleCheckWeigherRecord(id));
        checkWeigherEditDialog.showDialog();
    }

    @Override
    public void onDeleteClicked(int id) {
        if (dbHelper.deleteCheckWeigherBinRecord(id)) {
            Toasty.success(this, "Deleted successfully", Toast.LENGTH_SHORT, true).show();
            onSearchClicked();
        } else {
            Toasty.error(this, "Unable To Delete", Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void onUpdateSucces() {
        onSearchClicked();
    }
}
