package n.webinfotech.itcreports.activities;

import android.content.Intent;
import android.os.Environment;
import android.support.annotation.LayoutRes;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;

public class AdminBaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    android.support.v7.widget.Toolbar toolbar;
    Drawer drawer;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_base);
    }

    protected void inflateContent(@LayoutRes int inflatedResID) {
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflatedResID, contentFrameLayout);
        ButterKnife.bind(this);
        setDrawer();
    }

    public void setDrawer() {
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withHeaderBackground(R.drawable.itc_enduring  )
                .withActivity(this)
                .build();

        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .withHasStableIds(true)
                .withDisplayBelowStatusBar(false)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        new ExpandableDrawerItem().withName("Manage Users").withSelectable(true)
                                .withSubItems(
                                        new SecondaryDrawerItem().withName("Show Users").withLevel(2).withIdentifier(1).withSelectable(false),
                                        new SecondaryDrawerItem().withName("Add User").withLevel(2).withIdentifier(2).withSelectable(false)
                                ),
                        new ExpandableDrawerItem().withName("Manage Machines").withSelectable(true)
                                .withSubItems(
                                        new SecondaryDrawerItem().withName("Show Machines").withLevel(2).withIdentifier(3).withSelectable(false),
                                        new SecondaryDrawerItem().withName("Add Machines").withLevel(2).withIdentifier(4).withSelectable(false)
                                ),
                        new ExpandableDrawerItem().withName("Reports").withSelectable(true)
                                .withSubItems(
                                        new SecondaryDrawerItem().withName("Baking time record reporting").withSelectable(false).withIdentifier(6).withLevel(2),
                                        new SecondaryDrawerItem().withName("MD Records reporting").withSelectable(false).withIdentifier(7).withLevel(2),
                                        new SecondaryDrawerItem().withName("Hourly PQI Sheet").withSelectable(false).withIdentifier(8).withLevel(2),
                                        new SecondaryDrawerItem().withName("CFC Sheet").withSelectable(false).withIdentifier(9).withLevel(2),
                                        new SecondaryDrawerItem().withName("DH Room").withSelectable(false).withIdentifier(10).withLevel(2),
                                        new SecondaryDrawerItem().withName("Sieve Magnet").withSelectable(false).withIdentifier(11).withLevel(2),
                                        new SecondaryDrawerItem().withName("Tank Temp").withSelectable(false).withIdentifier(12).withLevel(2),
                                        new SecondaryDrawerItem().withName("CheckWeigher").withSelectable(false).withIdentifier(13).withLevel(2),
                                        new SecondaryDrawerItem().withName("Online Sensory").withSelectable(false).withIdentifier(14).withLevel(2),
                                        new SecondaryDrawerItem().withName("Start Up Checklist (Chocofill)").withSelectable(false).withIdentifier(16).withLevel(2)
                                ),
                        new PrimaryDrawerItem().withName("Save Database").withSelectable(false).withIdentifier(15),
                        new PrimaryDrawerItem().withName("Log Out").withSelectable(false).withIdentifier(5)
                ).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem != null) {
                            switch (((int) drawerItem.getIdentifier())) {
                                case 16:
                                    Intent startUpCheckListIntent = new Intent(getApplicationContext(), StartCheckUpListReportActivity.class);
                                    startActivity(startUpCheckListIntent);
                                    break;
                                case 15:
                                    saveDatabaseBackup();
                                    break;
                                case 14:
                                    Intent onlineSensoryIntent = new Intent(getApplicationContext(), OnlineSensoryEvaluationReportActivity.class);
                                    startActivity(onlineSensoryIntent);
                                    break;
                                case 13:
                                    Intent checkWeigherIntent = new Intent(getApplicationContext(), CheckWeigherReportActivity.class);
                                    startActivity(checkWeigherIntent);
                                    break;
                                case 12:
                                    Intent tabkTempIntent = new Intent(getApplicationContext(), TankTempReportActivity.class);
                                    startActivity(tabkTempIntent);
                                    break;
                                case 11:
                                    Intent sieveMagnetIntent = new Intent(getApplicationContext(), SieveAndMagnetChecklistReportActivity.class);
                                    startActivity(sieveMagnetIntent);
                                    break;
                                case 10:
                                    Intent dhRoomIntent = new Intent(getApplicationContext(), DHRoomRecordReportActivity.class);
                                    startActivity(dhRoomIntent);
                                    break;
                                case 9:
                                    Intent cfcSheet = new Intent(getApplicationContext(), CFCCheckingSheetReportActviity.class);
                                    startActivity(cfcSheet);
                                    break;
                                case 8:
                                    Intent hourlyPQISheet = new Intent(getApplicationContext(), HourlyPQISheetReportActivity.class);
                                    startActivity(hourlyPQISheet);
                                    break;
                                case 7:
                                    Intent mdIntent = new Intent(getApplicationContext(), MDRecordReportActivity.class);
                                    startActivity(mdIntent);
                                    break;
                                case 6:
                                    Intent bakingTimeRecordsReport = new Intent(getApplicationContext(), BakingTimeRecordReportActivity.class);
                                    startActivity(bakingTimeRecordsReport);
                                    break;
                                case 5:
                                    androidApplication = (AndroidApplication) getApplicationContext();
                                    androidApplication.setUser(getApplicationContext(), null);
                                    Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
                                    startActivity(loginIntent);
                                    finish();
                                    break;
                                case 4:
                                    Intent addMachineActivity = new Intent(getApplicationContext(), AddMachineActivity.class);
                                    startActivity(addMachineActivity);
                                    break;
                                case 3:
                                    Intent showMachinesActivity = new Intent(getApplicationContext(), MachineListActivity.class);
                                    startActivity(showMachinesActivity);
                                    break;
                                case 2:
                                    Intent addUserIntent = new Intent(getApplicationContext(), AddUserActivity.class);
                                    startActivity(addUserIntent);
                                    break;
                                case 1:
                                    Intent userListIntent = new Intent(getApplicationContext(), UserListActivity.class);
                                    startActivity(userListIntent);
                                    break;
                            }
                        }
                        return false;
                    }
                })
                .build();
    }

    public void saveDatabaseBackup() {
        try {
            File sd = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/ITCReports/DatabaseBackup");
            //create directory if not exist
            if (!sd.isDirectory()) {
                sd.mkdirs();
            }

            if (sd.canWrite()) {
                String currentDBPath = "/data/data/" + getPackageName() + "/databases/itc.db";
                String backupDBPath = getDate() + "_db_backup.db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                    Toast.makeText(this, "Database Transfered!", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception e) {
            Log.e("LogMsg", e.toString());
        }
    }

    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy_MM_dd_hh_mm_a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

}
