package n.webinfotech.itcreports.activities;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.zxing.Result;

import java.io.IOException;

import es.dmoral.toasty.Toasty;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.util.DBHelper;

public class QRScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);
        initializeDBHelper();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void handleResult(Result rawResult) {
        Cursor c = dbHelper.checkCode(rawResult.getText());
        if (c.getCount() > 0) {
            c.moveToFirst();
            checkForm(c.getInt(c.getColumnIndex("form_type_id")), c.getString(c.getColumnIndex("machine_name")));
        } else {
            Toasty.error(this, "No machine found for this code", Toast.LENGTH_SHORT, true).show();
            mScannerView.resumeCameraPreview(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    public void checkForm(int formId, String machineName) {
        switch (formId) {
            case 9:
                Intent checkWeigher = new Intent(this, CheckweigherBinRecordFormActivity.class);
                checkWeigher.putExtra("machineName", machineName);
                startActivity(checkWeigher);
                break;
            case 2:
                Intent tankTempIntent = new Intent(this, TankTemperatureFormActivity.class);
                tankTempIntent.putExtra("machineName", machineName);
                startActivity(tankTempIntent);
                break;
            case 6:
                Intent sieveMagnetIntent = new Intent(this, SieveAndMagnetChecklistFormActivity.class);
                sieveMagnetIntent.putExtra("machineName", machineName);
                startActivity(sieveMagnetIntent);
                break;
            case 3:
                Intent dhroomIntent = new Intent(this, DHRoomRecordFormActivity.class);
                dhroomIntent.putExtra("machineName", machineName);
                startActivity(dhroomIntent);
                break;
            case 4:
                Intent cfcCheckingIntent = new Intent(this, CFCCheckingSheetFormActivity.class);
                cfcCheckingIntent.putExtra("machineName", machineName);
                startActivity(cfcCheckingIntent);
                break;
            case 5:
                Intent mdRecordIntent = new Intent(this, MDRecordFormActivity.class);
                mdRecordIntent.putExtra("machineName", machineName);
                startActivity(mdRecordIntent);
                break;
            case 8:
                Intent bakingTimeIntent = new Intent(this, BakingTimeRecordFormActivity.class);
                bakingTimeIntent.putExtra("machineName", machineName);
                startActivity(bakingTimeIntent);
                break;
        }
    }

}
