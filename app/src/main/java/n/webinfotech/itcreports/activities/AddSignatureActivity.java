package n.webinfotech.itcreports.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.dialogs.AddSignatureDialog;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.SaveSignatureHelper;

import static n.webinfotech.itcreports.util.Helper.getAlbumStorageDir;

public class AddSignatureActivity extends AppCompatActivity implements AddSignatureDialog.Callback {

    @BindView(R.id.img_view_signature)
    ImageView imgViewSignature;
    AddSignatureDialog addSignatureDialog;
    User user;
    DBHelper dbHelper;
    File file;
    String signature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_signature);
        ButterKnife.bind(this);
        initialiseDialog();
        addSignatureDialog.setUpDialog();
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        user = androidApplication.getUser(this);
        initializeDBHelper();
        checkSignature();
    }

    public void initialiseDialog() {
        addSignatureDialog = new AddSignatureDialog(this, this, this);
    }

    public void checkSignature() {
        signature = dbHelper.getVerifierSignature(user.id);
        if (signature == null) {
            Toasty.warning(this, "You have no signature", Toast.LENGTH_SHORT, true).show();
        } else {
            file = new File(getAlbumStorageDir("ITCReports/Signatures"), signature);

            if(file.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                imgViewSignature.setImageBitmap(myBitmap);
            }
        }
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void onSaveClicked(Bitmap signatureBitmap) {
        String fileName = user.name.trim() + "_" + getDate() + ".png";
        if (signature != null) {
            file.delete();
        }
        if (SaveSignatureHelper.addPngSignatureToGallery(signatureBitmap, fileName)) {
            if (dbHelper.addVerifierSignature(user.id, fileName)) {
                checkSignature();
                addSignatureDialog.hideDialog();
                Toasty.success(this, "Signature saved successfully", Toast.LENGTH_SHORT, true).show();
            } else {
                addSignatureDialog.hideDialog();
                Toasty.error(this, "Something went wrong", Toast.LENGTH_SHORT, true).show();
            }
        } else {
            addSignatureDialog.hideDialog();
            Toasty.error(this, "Failed to save signature", Toast.LENGTH_SHORT, true).show();
        }
    }

    @OnClick(R.id.btn_add) void onBtnAddClicked() {
        addSignatureDialog.showDialog();
    }

    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy_MM_dd_hh_mm_a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

}
