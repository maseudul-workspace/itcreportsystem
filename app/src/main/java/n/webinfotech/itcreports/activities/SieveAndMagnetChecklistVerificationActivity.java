package n.webinfotech.itcreports.activities;

import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.MDRecordsAdapter;
import n.webinfotech.itcreports.adapters.SieveMagnetUnverifiedAdapter;
import n.webinfotech.itcreports.dialogs.VerificationDialog;
import n.webinfotech.itcreports.models.MDRecord;
import n.webinfotech.itcreports.models.SieveMagnet;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.SaveSignatureHelper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;

public class SieveAndMagnetChecklistVerificationActivity extends AppCompatActivity implements SieveMagnetUnverifiedAdapter.Callback {


    DBHelper dbHelper;
    ArrayList<Integer> magnetIds;
    @BindView(R.id.recycler_view_sieve_magnet_verification)
    RecyclerView recyclerView;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sieve_and_magnet_checklist_verification);
        ButterKnife.bind(this);
        magnetIds = new ArrayList<>();
        initializeDBHelper();
        setRecyclerView();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setRecyclerView() {
        Cursor c = dbHelper.getSieveMagnetRecordUnverified();
        if (c.getCount() > 0) {
            ArrayList<SieveMagnet> sieveMagnets = new ArrayList<>();
            while (c.moveToNext()) {
                SieveMagnet sieveMagnet = new SieveMagnet(
                        c.getInt(c.getColumnIndex("id")),
                        c.getString(c.getColumnIndex("date")),
                        c.getString(c.getColumnIndex("time")),
                        c.getString(c.getColumnIndex("shift")),
                        c.getString(c.getColumnIndex("sleve_condition")),
                        c.getString(c.getColumnIndex("magnet_cleaning_status")),
                        c.getString(c.getColumnIndex("shifter")),
                        c.getString(c.getColumnIndex("remarks")),
                        c.getInt(c.getColumnIndex("user_id")),
                        c.getString(c.getColumnIndex("username")),
                        c.getInt(c.getColumnIndex("verify_status")),
                        c.getInt(c.getColumnIndex("verify_id"))
                );
                sieveMagnets.add(sieveMagnet);
            }
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            SieveMagnetUnverifiedAdapter adapter = new SieveMagnetUnverifiedAdapter(this, sieveMagnets, this);
            recyclerView.setAdapter(adapter);
            DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
            recyclerView.addItemDecoration(itemDecor);
        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void insertToArray(int id) {
        magnetIds.add(id);

    }

    @Override
    public void removeFromArray(int id) {
        for (int i = 0; i < magnetIds.size(); i++) {
            if (magnetIds.get(i) == id) {
                magnetIds.remove(i);
                break;
            }
        }
    }

    @OnClick(R.id.btn_verify) void onVerificationClicked() {
        if (magnetIds.size() > 0 ) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Are you sure !");
            builder.setMessage("You are about to verify " + magnetIds.size() +" records. Do you really want to proceed ?");
            builder.setCancelable(false);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    androidApplication = (AndroidApplication) getApplicationContext();
                    User user = androidApplication.getUser(getApplicationContext());
                    for (int  i = 0; i < magnetIds.size(); i++) {
                        dbHelper.updateSieveMagnetVerification(magnetIds.get(i), user.id);
                    }
                    magnetIds.clear();
                    setRecyclerView();
                    Toasty.success(getApplicationContext(), "Successfully verified", Toast.LENGTH_SHORT, true).show();
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            builder.show();
        } else {
            Toasty.warning(this, "No data selected", Toast.LENGTH_SHORT, true).show();
        }
    }


    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy_MM_dd_hh_mm_a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

}
