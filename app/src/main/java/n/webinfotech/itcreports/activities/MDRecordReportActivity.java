package n.webinfotech.itcreports.activities;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.MDRecordsAdapter;
import n.webinfotech.itcreports.adapters.MDRecordsReportAdapter;
import n.webinfotech.itcreports.dialogs.MDRecordEditDialog;
import n.webinfotech.itcreports.models.MDRecord;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.Helper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;

public class MDRecordReportActivity extends AppCompatActivity implements MDRecordsReportAdapter.Callback, MDRecordEditDialog.Callback {

    DBHelper dbHelper;
    String startDate;
    String endDate;
    @BindView(R.id.txt_view_start_date)
    TextView txtViewStartDate;
    @BindView(R.id.txt_view_end_date)
    TextView txtViewEndDate;
    @BindView(R.id.recycler_view_md_record_data)
    RecyclerView recyclerView;
    Cursor cursor;
    MDRecordEditDialog mdRecordEditDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mdrecord_report);
        ButterKnife.bind(this);
        initializeDBHelper();
        startDate = Helper.getSearchCurrentDate();
        endDate = Helper.getSearchNextDate();
        txtViewStartDate.setText(startDate);
        txtViewEndDate.setText(endDate);
        cursor = dbHelper.getMDRecordsData(startDate, endDate);
        if (cursor.getCount() == 0) {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
        setRecyclerView(cursor);
        setUpDialog();

    }

    public void setUpDialog() {
        mdRecordEditDialog = new MDRecordEditDialog(this, this, this);
        mdRecordEditDialog.setUpDialog();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void saveData() {

        File sd = Environment.getExternalStorageDirectory();
        String csvFile = getDate() + "_mdrecord.xls";

        File directory = new File(sd.getAbsolutePath()+"/ITCReports/MDRecord");
        //create directory if not exist
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
        try {

            //file path
            File file = new File(directory, csvFile);
            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setLocale(new Locale("en", "EN"));
            WritableWorkbook workbook;
            workbook = Workbook.createWorkbook(file, wbSettings);
            //Excel sheet name. 0 represents first sheet
            WritableSheet sheet = workbook.createSheet("MD Record", 0);


//            Excel Header
//            Add logo
            byte[] logoBytes = getByteFromDrawable(getDrawable(R.drawable.itc_logo));
            WritableImage writableImageLogo = new WritableImage(0, 0, 2,4, logoBytes);
            sheet.addImage(writableImageLogo);

//            Add header
            byte[] headerBytes = getByteFromDrawable(getDrawable(R.drawable.ccpchecklist));
            WritableImage headerLogo = new WritableImage(2, 0, 8,4, headerBytes);
            sheet.addImage(headerLogo);

            sheet.addCell(new Label(10, 1, "Document No"));

            for(int i = 2; i < 12; i++) {
                CellView cell=sheet.getColumnView(i);
                cell.setAutosize(true);
                sheet.setColumnView(i, cell);
            }

            sheet.addCell(new Label(11, 1, "  "));


            sheet.addCell(new Label(10, 2, "Revision No"));
            sheet.addCell(new Label(11, 2, "  "));

            sheet.addCell(new Label(10, 3, "Effective Date"));
            sheet.addCell(new Label(11, 3, "  "));


            WritableFont arial10pt = new WritableFont(WritableFont.ARIAL, 9);
            arial10pt.setBoldStyle(WritableFont.BOLD);
            WritableCellFormat cellFormat = new WritableCellFormat(arial10pt);
            sheet.mergeCells(0,4,11,4);
            sheet.addCell(new Label(0, 4, "Frequency: Once In a Shift", cellFormat));

            sheet.addCell(new Label(0, 5, "Date", cellFormat));

            sheet.addCell(new Label(1, 5, "Shift", cellFormat));

            sheet.addCell(new Label(2, 5, "Time", cellFormat));

            sheet.addCell(new Label(3, 5, "  MD No  ", cellFormat));

            sheet.addCell(new Label(4, 5, "  Probe-FE 1 mm  ", cellFormat));

            sheet.addCell(new Label(5, 5, "  Probe-NFE 1.2 mm  ", cellFormat));

            sheet.addCell(new Label(6, 5, "  Probe-SS  1.8mm  ", cellFormat));

            sheet.addCell(new Label(7, 5, "  Interlock  ", cellFormat));

            sheet.addCell(new Label(8, 5, "  Sensitivity  ", cellFormat));

            sheet.addCell(new Label(9, 5, "  Checked By  ", cellFormat));

            sheet.addCell(new Label(10, 5, "  Remarks  ", cellFormat));

            sheet.addCell(new Label(11, 5, "  Verified By  ", cellFormat));

            int row_count = 6;


            Cursor c = dbHelper.getMDRecordsData(startDate, endDate);
            if (c.getCount() > 0) {
                while (c.moveToNext()) {
                    if (c.getInt(c.getColumnIndex("verify_status")) == 2) {
                        String sign = dbHelper.getVerifierSignature(c.getInt(c.getColumnIndex("verify_id")));
                        if (sign != null) {
                            byte[] signatureBytes = getByteFromFile(sign);
                            Log.e("LogMsg", "Hello1");
                            WritableImage signature = new WritableImage(11, row_count, 1, 1, signatureBytes);
                            sheet.addImage(signature);
                        }
                    }
                    sheet.addCell(new Label(0, row_count, c.getString(c.getColumnIndex("date"))));
                    sheet.addCell(new Label(1, row_count, c.getString(c.getColumnIndex("shift"))));
                    sheet.addCell(new Label(2, row_count, c.getString(c.getColumnIndex("time"))));
                    sheet.addCell(new Label(3, row_count, c.getString(c.getColumnIndex("md_no"))));
                    sheet.addCell(new Label(4, row_count, c.getString(c.getColumnIndex("prove_fe"))));
                    sheet.addCell(new Label(5, row_count, c.getString(c.getColumnIndex("prove_nfe"))));
                    sheet.addCell(new Label(6, row_count, c.getString(c.getColumnIndex("prove_ss"))));
                    sheet.addCell(new Label(7, row_count, c.getString(c.getColumnIndex("inter_lock"))));
                    sheet.addCell(new Label(8, row_count, c.getString(c.getColumnIndex("sensitivity"))));
                    sheet.addCell(new Label(9, row_count, c.getString(c.getColumnIndex("u_name"))));
                    sheet.addCell(new Label(10 , row_count, c.getString(c.getColumnIndex("remarks"))));
                    row_count++;
                }
            }


            workbook.write();
            workbook.close();
            Toast.makeText(this,
                    "Data Exported in a Excel Sheet", Toast.LENGTH_SHORT).show();
        } catch(Exception e){
            Log.e("LogMsg", "Workbook error" + e.getMessage());
            e.printStackTrace();
        }
    }

    public byte[] getByteFromDrawable(Drawable drawable) {
        Bitmap logoBitmap = ((BitmapDrawable)drawable).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        logoBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] bytes = stream.toByteArray();
        return bytes;
    }

    public byte[] getByteFromFile(String signature) {
        Log.e("LogMsg", "Signatuire " + signature);
        File file = new File(getAlbumStorageDir("ITCReports/Signatures"), signature);
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            Log.e("LogMsg", "Error: " + e.getMessage());
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("LogMsg", "Error: " + e.getMessage());
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    public static File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStorageDirectory(), albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    @OnClick(R.id.img_view_calendar_start_date) void onCalendarStartDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        startDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        startDate = Helper.changeDateFormat(startDate);
                        txtViewStartDate.setText(startDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.img_view_calendar_end_date) void onCalendarEndDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        endDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        endDate = Helper.changeDateFormat(endDate);
                        txtViewEndDate.setText(endDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.btn_export) void onExportClicked() {
            saveData();
    }
    @OnClick(R.id.btn_search) void onSearchClicked() {
        if (startDate == null || endDate == null){
            Toasty.warning(this, "Please enter both dates", Toast.LENGTH_SHORT).show();
        } else {
            cursor = dbHelper.getMDRecordsData(startDate, endDate);
            if (cursor.getCount() == 0) {
                Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
            }
            setRecyclerView(cursor);
        }
    }

    public void setRecyclerView(Cursor c) {
        if (c.getCount() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            ArrayList<MDRecord> mdRecords = new ArrayList<>();
            while (c.moveToNext()) {
                MDRecord mdRecord = new MDRecord(
                        c.getInt(c.getColumnIndex("id")),
                        c.getString(c.getColumnIndex("date")),
                        c.getString(c.getColumnIndex("time")),
                        c.getString(c.getColumnIndex("shift")),
                        c.getString(c.getColumnIndex("md_no")),
                        c.getString(c.getColumnIndex("prove_fe")),
                        c.getString(c.getColumnIndex("prove_nfe")),
                        c.getString(c.getColumnIndex("prove_ss")),
                        c.getString(c.getColumnIndex("inter_lock")),
                        c.getString(c.getColumnIndex("sensitivity")),
                        c.getString(c.getColumnIndex("remarks")),
                        c.getInt(c.getColumnIndex("user_id")),
                        c.getString(c.getColumnIndex("u_name")),
                        c.getInt(c.getColumnIndex("verify_status")),
                        c.getInt(c.getColumnIndex("verify_id"))
                );
                mdRecords.add(mdRecord);
            }
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            MDRecordsReportAdapter adapter = new MDRecordsReportAdapter(mdRecords, this, this);
            recyclerView.setAdapter(adapter);
            DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
            recyclerView.addItemDecoration(itemDecor);
        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }

    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy_MM_dd_hh_mm_a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }


    @Override
    public void onEditClicked(int id) {
        mdRecordEditDialog.setData(dbHelper.getSingleMDRecordsData(id));
        mdRecordEditDialog.showDialog();
    }

    @Override
    public void onDeleteClicked(int id) {
        if (dbHelper.deleteMDRecord(id)) {
            Toasty.success(this, "Deleted Successfully", Toast.LENGTH_SHORT, true).show();
            onSearchClicked();
        } else {
            Toasty.error(this, "Unable To Delete", Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void onUpdateSucces() {
        onSearchClicked();
    }
}
