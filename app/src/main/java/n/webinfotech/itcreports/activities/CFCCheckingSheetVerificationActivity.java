package n.webinfotech.itcreports.activities;

import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.CFCCheckingUnverifiedAdapter;
import n.webinfotech.itcreports.adapters.MDRecordsAdapter;
import n.webinfotech.itcreports.dialogs.VerificationDialog;
import n.webinfotech.itcreports.models.CFCChecking;
import n.webinfotech.itcreports.models.MDRecord;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.SaveSignatureHelper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;

public class CFCCheckingSheetVerificationActivity extends AppCompatActivity implements CFCCheckingUnverifiedAdapter.Callback {

    DBHelper dbHelper;
    ArrayList<Integer> cfcRecordIds;
    @BindView(R.id.recycler_view_cfc_checking)
    RecyclerView recyclerViewCFCChecking;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cfcchecking_sheet_verification);
        ButterKnife.bind(this);
        cfcRecordIds = new ArrayList<>();
        initializeDBHelper();
        setRecyclerView();

    }


    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void setRecyclerView() {
        Cursor c = dbHelper.getCFCCheckingUnverified();
        if (c.getCount() > 0) {
            recyclerViewCFCChecking.setVisibility(View.VISIBLE);
            ArrayList<CFCChecking> cfcCheckings = new ArrayList<>();
            while (c.moveToNext()) {
                CFCChecking cfcChecking = new CFCChecking(
                        c.getInt(c.getColumnIndex("id")),
                        c.getString(c.getColumnIndex("date")),
                        c.getString(c.getColumnIndex("time")),
                        c.getString(c.getColumnIndex("shift")),
                        c.getString(c.getColumnIndex("machine_no")),
                        c.getString(c.getColumnIndex("sku")),
                        c.getString(c.getColumnIndex("rhno_of_torn")),
                        c.getString(c.getColumnIndex("no_of_flap_open")),
                        c.getString(c.getColumnIndex("no_of_dent")),
                        c.getString(c.getColumnIndex("coading_mono")),
                        c.getString(c.getColumnIndex("qtty_present_mono")),
                        c.getString(c.getColumnIndex("qtty_present_cfc")),
                        c.getString(c.getColumnIndex("tapping_quality")),
                        c.getString(c.getColumnIndex("coading_cfc")),
                        c.getString(c.getColumnIndex("remarks")),
                        c.getInt(c.getColumnIndex("user_id")),
                        c.getString(c.getColumnIndex("username")),
                        c.getInt(c.getColumnIndex("verify_status")),
                        c.getInt(c.getColumnIndex("verify_id"))
                );
                cfcCheckings.add(cfcChecking);
            }
            recyclerViewCFCChecking.setLayoutManager(new LinearLayoutManager(this));
            CFCCheckingUnverifiedAdapter adapter = new CFCCheckingUnverifiedAdapter(this, cfcCheckings, this);
            recyclerViewCFCChecking.setAdapter(adapter);
            DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
            recyclerViewCFCChecking.addItemDecoration(itemDecor);
        } else {
            recyclerViewCFCChecking.setVisibility(View.GONE);
        }
    }

    @Override
    public void insertToArray(int id) {
        cfcRecordIds.add(id);

    }

    @Override
    public void removeFromArray(int id) {
        for (int i = 0; i < cfcRecordIds.size(); i++) {
            if (cfcRecordIds.get(i) == id) {
                cfcRecordIds.remove(i);
                break;
            }
        }
    }

    @OnClick(R.id.btn_verify) void onVerificationClicked() {
        if (cfcRecordIds.size() > 0 ) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Are you sure !");
            builder.setMessage("You are about to verify " + cfcRecordIds.size() + " records. Do you really want to proceed ?");
            builder.setCancelable(false);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    androidApplication = (AndroidApplication) getApplicationContext();
                    User user = androidApplication.getUser(getApplicationContext());
                    for (int  i = 0; i < cfcRecordIds.size(); i++) {
                        dbHelper.updateCFCCheckingVerification(cfcRecordIds.get(i), user.id);
                    }
                    cfcRecordIds.clear();
                    setRecyclerView();
                    Toasty.success(getApplicationContext(), "Successfully verified", Toast.LENGTH_SHORT, true).show();

                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            builder.show();

        } else {
            Toasty.warning(this, "No data selected", Toast.LENGTH_SHORT, true).show();
        }
    }


    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy_MM_dd_hh_mm_a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
}
