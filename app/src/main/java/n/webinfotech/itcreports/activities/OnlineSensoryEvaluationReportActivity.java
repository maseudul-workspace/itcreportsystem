package n.webinfotech.itcreports.activities;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.OnlineSensoryDataAdapter;
import n.webinfotech.itcreports.adapters.OnlineSensoryVerifyAdapter;
import n.webinfotech.itcreports.dialogs.OnlineSensoryEditDialog;
import n.webinfotech.itcreports.dialogs.SelectModuleDialog;
import n.webinfotech.itcreports.models.OnlineSensory;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.Helper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;
import static n.webinfotech.itcreports.util.Helper.getByteFromDrawable;
import static n.webinfotech.itcreports.util.Helper.getByteFromFile;

public class OnlineSensoryEvaluationReportActivity extends AppCompatActivity implements OnlineSensoryEditDialog.Callback, OnlineSensoryDataAdapter.Callback, SelectModuleDialog.Callback {

    DBHelper dbHelper;
    String startDate;
    String endDate;
    @BindView(R.id.txt_view_start_date)
    TextView txtViewStartDate;
    @BindView(R.id.txt_view_end_date)
    TextView txtViewEndDate;
    @BindView(R.id.recycler_view_online_sensory_data)
    RecyclerView recyclerView;
    Cursor cursor;
    OnlineSensoryEditDialog onlineSensoryEditDialog;
    SelectModuleDialog selectModuleDialog;
    String module;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_sensory_evaluation_report);
        ButterKnife.bind(this);
        setSelectModuleDialog();
        initializeDBHelper();
        setUpInitialDates();
        setUpDialog();
        selectModuleDialog.showDialog();
    }

    public void setUpInitialDates() {
        startDate = Helper.getSearchCurrentDate();
        endDate = Helper.getSearchNextDate();
        txtViewStartDate.setText(startDate);
        txtViewEndDate.setText(endDate);
    }

    public void setUpDialog() {
        onlineSensoryEditDialog = new OnlineSensoryEditDialog(this, this, this);
    }

    public void setSelectModuleDialog() {
        selectModuleDialog = new SelectModuleDialog(this, this, this);
        selectModuleDialog.setUpDialog();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void saveData() {

        File sd = Environment.getExternalStorageDirectory();
        String csvFile = getDate() + "_online_sensory_evaluation.xls";

        File directory = new File(sd.getAbsolutePath()+"/ITCReports/OnlineSensoryEvaluation");
        //create directory if not exist
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
        try {

            //file path
            File file = new File(directory, csvFile);
            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setLocale(new Locale("en", "EN"));
            WritableWorkbook workbook;
            workbook = Workbook.createWorkbook(file, wbSettings);
            //Excel sheet name. 0 represents first sheet
            WritableSheet sheet = workbook.createSheet("Online Sensory Evaluation", 0);


//            Excel Header
//            Add logo
            byte[] logoBytes = getByteFromDrawable(getDrawable(R.drawable.itc_logo));
            WritableImage writableImageLogo = new WritableImage(0, 0, 2,4, logoBytes);
            sheet.addImage(writableImageLogo);

//            Add header
            byte[] headerBytes = getByteFromDrawable(getDrawable(R.drawable.onlinesensory));
            WritableImage headerLogo = new WritableImage(2, 0, 15,4, headerBytes);
            sheet.addImage(headerLogo);

            sheet.addCell(new Label(17, 1, "Document No"));

            for(int i = 1; i < 19; i++) {
                CellView cell=sheet.getColumnView(i);
                cell.setAutosize(true);
                sheet.setColumnView(i, cell);
            }

            sheet.addCell(new Label(18, 1, "  "));

            sheet.addCell(new Label(17, 2, "Revision No"));
            sheet.addCell(new Label(18, 2, "  "));

            sheet.addCell(new Label(17, 3, "Effective Date"));
            sheet.addCell(new Label(18, 3, "  "));

            WritableFont arial10pt = new WritableFont(WritableFont.ARIAL, 9);
            arial10pt.setBoldStyle(WritableFont.BOLD);
            WritableCellFormat cellFormat = new WritableCellFormat(arial10pt);

            sheet.mergeCells(0,4,3,4);
            sheet.addCell(new Label(0, 4, "Frequency: Once In a Shift", cellFormat));
//
            sheet.mergeCells(6,4,9,4);
            sheet.addCell(new Label(6, 4, "Pannelist 1", cellFormat));
//
            sheet.mergeCells(10,4,13,4);
            sheet.addCell(new Label(10, 4, "Pannelist 2", cellFormat));

            sheet.mergeCells(14,4,17,4);
            sheet.addCell(new Label(14, 4, "Pannelist 3", cellFormat));
//
            sheet.addCell(new Label(0, 5, "Date", cellFormat));

            sheet.addCell(new Label(1, 5, "Shift", cellFormat));

            sheet.addCell(new Label(2, 5, "Time", cellFormat));

            sheet.addCell(new Label(3, 5, "Module", cellFormat));

            sheet.addCell(new Label(4, 5, "Batch No", cellFormat));

            sheet.addCell(new Label(5, 5, "SKU", cellFormat));

            sheet.addCell(new Label(6, 5, "Apperance", cellFormat));

            sheet.addCell(new Label(7, 5, "Taste", cellFormat));

            sheet.addCell(new Label(8, 5, "Aroma", cellFormat));

            sheet.addCell(new Label(9, 5, "Sign", cellFormat));

            sheet.addCell(new Label(10, 5, "Apperance", cellFormat));

            sheet.addCell(new Label(11, 5, "Taste", cellFormat));

            sheet.addCell(new Label(12, 5, "Aroma", cellFormat));

            sheet.addCell(new Label(13, 5, "Sign", cellFormat));

            sheet.addCell(new Label(14, 5, "Apperance", cellFormat));

            sheet.addCell(new Label(15, 5, "Taste", cellFormat));

            sheet.addCell(new Label(16, 5, "Aroma", cellFormat));

            sheet.addCell(new Label(17, 5, "Sign", cellFormat));

            sheet.mergeCells(18,4,18,5);

            sheet.addCell(new Label(18, 4, "Verified By", cellFormat));

//
            int row_count = 6;

            Cursor c = dbHelper.getOnlineSansoryData(startDate, endDate, module);
            if (c.getCount() > 0) {
                while (c.moveToNext()) {
                    Cursor batchesCursor = dbHelper.getDistinctBatches(c.getInt(c.getColumnIndex("id")));
                    while (batchesCursor.moveToNext()) {
                        Cursor cursorPannelist1Data = dbHelper.getPannelistData(c.getInt(c.getColumnIndex("id")), 1, batchesCursor.getInt(batchesCursor.getColumnIndex("batch_no")));
                        Cursor cursorPannelist2Data = dbHelper.getPannelistData(c.getInt(c.getColumnIndex("id")), 2, batchesCursor.getInt(batchesCursor.getColumnIndex("batch_no")));
                        Cursor cursorPannelist3Data = dbHelper.getPannelistData(c.getInt(c.getColumnIndex("id")), 3, batchesCursor.getInt(batchesCursor.getColumnIndex("batch_no")));
                        cursorPannelist1Data.moveToNext();
                        cursorPannelist2Data.moveToNext();
                        cursorPannelist3Data.moveToNext();

                        if (c.getInt(c.getColumnIndex("verify_status")) == 2) {
                            String sign = dbHelper.getVerifierSignature(c.getInt(c.getColumnIndex("verify_id")));
                            if (sign != null) {
                                byte[] signatureBytes = getByteFromFile(sign);
                                Log.e("LogMsg", "Hello1");
                                WritableImage signature = new WritableImage(18, row_count, 1, 1, signatureBytes);
                                sheet.addImage(signature);
                            }
                        }


                        sheet.addCell(new Label(0, row_count, c.getString(c.getColumnIndex("date"))));
                        sheet.addCell(new Label(1, row_count, c.getString(c.getColumnIndex("shift"))));

                        sheet.addCell(new Label(2, row_count, c.getString(c.getColumnIndex("time"))));

                        sheet.addCell(new Label(3, row_count, c.getString(c.getColumnIndex("machine_no"))));

                        sheet.addCell(new Label(4, row_count, Integer.toString(batchesCursor.getInt(batchesCursor.getColumnIndex("batch_no")))));

                        sheet.addCell(new Label(5, row_count, c.getString(c.getColumnIndex("sku"))));

                        sheet.addCell(new Label(6, row_count, cursorPannelist1Data.getString(cursorPannelist1Data.getColumnIndex("apperance"))));
                        sheet.addCell(new Label(7, row_count, cursorPannelist1Data.getString(cursorPannelist1Data.getColumnIndex("taste"))));
                        sheet.addCell(new Label(8, row_count, cursorPannelist1Data.getString(cursorPannelist1Data.getColumnIndex("aroma"))));

                        try{
                            byte[] signBytes1 = getByteFromFile(c.getString(c.getColumnIndex("p1_sign")));
                            WritableImage signature1 = new WritableImage(9, row_count, 1,1, signBytes1);
                            sheet.addImage(signature1);
                        } catch (Exception e) {
                            sheet.addCell(new Label(9, row_count, "No Sign"));

                        }

                        sheet.addCell(new Label(10, row_count, cursorPannelist2Data.getString(cursorPannelist2Data.getColumnIndex("apperance"))));
                        sheet.addCell(new Label(11, row_count, cursorPannelist2Data.getString(cursorPannelist2Data.getColumnIndex("taste"))));
                        sheet.addCell(new Label(12, row_count, cursorPannelist2Data.getString(cursorPannelist2Data.getColumnIndex("aroma"))));

                        try{
                            byte[] signBytes2 = getByteFromFile(c.getString(c.getColumnIndex("p2_sign")));
                            WritableImage signature2 = new WritableImage(13, row_count, 1,1, signBytes2);
                            sheet.addImage(signature2);
                        } catch (Exception e) {
                            sheet.addCell(new Label(13, row_count, "No Sign"));
                        }

                        Log.e("LogMsg", "Hello5");

                        sheet.addCell(new Label(14, row_count, cursorPannelist3Data.getString(cursorPannelist3Data.getColumnIndex("apperance"))));
                        sheet.addCell(new Label(15, row_count, cursorPannelist3Data.getString(cursorPannelist3Data.getColumnIndex("taste"))));
                        sheet.addCell(new Label(16, row_count, cursorPannelist3Data.getString(cursorPannelist3Data.getColumnIndex("aroma"))));


                        try{
                            byte[] signBytes3 = getByteFromFile(c.getString(c.getColumnIndex("p3_sign")));
                            WritableImage signature3 = new WritableImage(17, row_count, 1,1, signBytes3);
                            sheet.addImage(signature3);
                        } catch (Exception e) {
                            sheet.addCell(new Label(17, row_count, "No Sign"));
                        }

                        row_count++;

                    }
                }
            }

            workbook.write();
            workbook.close();
            Toast.makeText(this,
                    "Data Exported in a Excel Sheet", Toast.LENGTH_SHORT).show();
        } catch(Exception e){
            Log.e("LogMsg", "Workbook error" + e.getMessage());
            e.printStackTrace();
        }
    }

    @OnClick(R.id.img_view_calendar_start_date) void onCalendarStartDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        startDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        startDate = Helper.changeDateFormat(startDate);
                        txtViewStartDate.setText(startDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.img_view_calendar_end_date) void onCalendarEndDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        endDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        endDate = Helper.changeDateFormat(endDate);
                        txtViewEndDate.setText(endDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.btn_export) void onExportClicked() {
        saveData();
    }
    @OnClick(R.id.btn_search) void onSearchClicked() {
        if (startDate == null || endDate == null){
            Toasty.warning(this, "Please enter both dates", Toast.LENGTH_SHORT).show();
        } else {
           selectModuleDialog.showDialog();
        }
    }

    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy_MM_dd_hh_mm_a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void setRecyclerView(Cursor c) {
        if (c.getCount() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            ArrayList<OnlineSensory> onlineSensories = new ArrayList<>();
            while (c.moveToNext()) {
                        OnlineSensory onlineSensory = new OnlineSensory(
                        c.getInt(c.getColumnIndex("id")),
                        c.getString(c.getColumnIndex("date")),
                        c.getString(c.getColumnIndex("time")),
                        c.getString(c.getColumnIndex("shift")),
                        c.getString(c.getColumnIndex("machine_no")),
                        c.getString(c.getColumnIndex("sku")),
                        c.getString(c.getColumnIndex("p1_sign")),
                        c.getString(c.getColumnIndex("p1_name")),
                        c.getString(c.getColumnIndex("p2_sign")),
                        c.getString(c.getColumnIndex("p2_name")),
                        c.getString(c.getColumnIndex("p3_sign")),
                        c.getString(c.getColumnIndex("p3_name")),
                        dbHelper.getOnlineSensoryBatchesData(c.getInt(c.getColumnIndex("id")), 1),
                        dbHelper.getOnlineSensoryBatchesData(c.getInt(c.getColumnIndex("id")), 2),
                        dbHelper.getOnlineSensoryBatchesData(c.getInt(c.getColumnIndex("id")), 3),
                        c.getInt(c.getColumnIndex("verify_status")),
                        c.getInt(c.getColumnIndex("verify_id"))
                );
                onlineSensories.add(onlineSensory);
            }
            OnlineSensoryDataAdapter adapter = new OnlineSensoryDataAdapter(this, onlineSensories, this, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);
            DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
            recyclerView.addItemDecoration(itemDecor);
        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onEditClicked(int id) {
        onlineSensoryEditDialog.setUpDialog();
        onlineSensoryEditDialog.setData(dbHelper.getSingleOnlineSansoryData(id));
        onlineSensoryEditDialog.showDialog();
    }

    @Override
    public void onDeleteClicked(int id) {
        if (dbHelper.deleteOnlineSensoryData(id)) {
            Toasty.success(this, "Deleted Successfully", Toast.LENGTH_SHORT, true).show();

        } else {
            Toasty.error(this, "Unable To Delete", Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void onUpdateSucces() {
        onSearchClicked();
    }

    @Override
    public void onModuleSelected(int moduleId) {
        if (moduleId == 1) {
            module = "M-1";
        } else {
            module = "M-2";
        }
        setData();
        selectModuleDialog.hideDialog();
    }

    public void setData() {
        cursor = dbHelper.getOnlineSansoryData(startDate, endDate, module);
        if (cursor.getCount() == 0) {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
            recyclerView.setVisibility(View.GONE);
        } else {
            setRecyclerView(cursor);
        }
    }

}
