package n.webinfotech.itcreports.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.ModuleAdapter;
import n.webinfotech.itcreports.models.Module;
import n.webinfotech.itcreports.util.DBHelper;

public class UserActivity extends AppCompatActivity {

    @BindView(R.id.txt_view_username)
    TextView txtViewUsername;
    @BindView(R.id.recycler_view_module_1)
    RecyclerView recyclerViewModule1;
    @BindView(R.id.recycler_view_module_2)
    RecyclerView recyclerViewModule2;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        txtViewUsername.setText("Welcome " + androidApplication.getUser(this).name);
        initializeDBHelper();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setRecyclerViewModule1() {
        ArrayList<Module> moduleArrayList = dbHelper.getModule1Data();
        for (int i = 0; i < moduleArrayList.size(); i++) {
            Log.e("LogMsg", "Module name: " + moduleArrayList.get(i).moduleName + " Status:" + moduleArrayList.get(i).status);
        }
        ModuleAdapter moduleAdapter = new ModuleAdapter(this, moduleArrayList);
        recyclerViewModule1.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewModule1.setAdapter(moduleAdapter);
        recyclerViewModule1.setNestedScrollingEnabled(false);
    }

    public void setRecyclerViewModule2() {
        ArrayList<Module> moduleArrayList = dbHelper.getModule2Data();
        for (int i = 0; i < moduleArrayList.size(); i++) {
            Log.e("LogMsg", "Module name: " + moduleArrayList.get(i).moduleName + " Status:" + moduleArrayList.get(i).status);
        }
        ModuleAdapter moduleAdapter = new ModuleAdapter(this, moduleArrayList);
        recyclerViewModule2.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewModule2.setAdapter(moduleAdapter);
        recyclerViewModule2.setNestedScrollingEnabled(false);
    }

    @OnClick(R.id.btn_hourly_pqi_sheet_form) void viewHourlyPQISheetForm() {
        Intent hourlyPQI = new Intent(this, HourlyPQISheetActivity.class);
        startActivity(hourlyPQI);
    }

    @OnClick(R.id.btn_online_sensory_view) void onOnlineSensoryViewClicked() {
        Intent onlineSensoryIntent = new Intent(this, OnlineSensoryBatchActivity.class);
        startActivity(onlineSensoryIntent);
    }

    @OnClick(R.id.btn_start_up_check_list) void onStartUpCheckListClicked() {
        Intent startUpCheckListIntent = new Intent(this, StartUpCheckListActivity.class);
        startActivity(startUpCheckListIntent);
    }

    @OnClick(R.id.scan_card_view) void onScanClicked() {
        Intent intent  = new Intent(this, QRScanActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_logout) void onLogoutClicked() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setUser(this, null);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRecyclerViewModule1();
        setRecyclerViewModule2();
    }
}
