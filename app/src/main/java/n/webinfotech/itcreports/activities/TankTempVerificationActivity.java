package n.webinfotech.itcreports.activities;

import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.DHRoomRecordUnverifiedAdapter;
import n.webinfotech.itcreports.adapters.TankTempVerifyAdapter;
import n.webinfotech.itcreports.dialogs.VerificationDialog;
import n.webinfotech.itcreports.models.DHRecord;
import n.webinfotech.itcreports.models.TankTemp;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.SaveSignatureHelper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;

public class TankTempVerificationActivity extends AppCompatActivity implements TankTempVerifyAdapter.Callback {

    DBHelper dbHelper;
    ArrayList<Integer> tankRecordIds;
    @BindView(R.id.recycler_view_tank_temp_verification)
    RecyclerView recyclerView;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tank_temp_verification);
        ButterKnife.bind(this);
        tankRecordIds = new ArrayList<>();
        initializeDBHelper();
        setRecyclerView();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setRecyclerView() {
        Cursor c = dbHelper.getTankTempRecordUnverified();
        if (c.getCount() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            ArrayList<TankTemp> tankTemps = new ArrayList<>();
            while (c.moveToNext()) {
                TankTemp tankTemp = new TankTemp(
                        c.getInt(c.getColumnIndex("id")),
                        c.getString(c.getColumnIndex("date")),
                        c.getString(c.getColumnIndex("time")),
                        c.getString(c.getColumnIndex("shift")),
                        c.getString(c.getColumnIndex("tank")),
                        c.getString(c.getColumnIndex("act_temp")),
                        c.getString(c.getColumnIndex("set_temp")),
                        c.getString(c.getColumnIndex("remarks")),
                        c.getInt(c.getColumnIndex("user_id")),
                        c.getString(c.getColumnIndex("username")),
                        c.getInt(c.getColumnIndex("verify_status")),
                        c.getInt(c.getColumnIndex("verify_id"))
                );
                tankTemps.add(tankTemp);
            }
            TankTempVerifyAdapter tankTempVerifyAdapter = new TankTempVerifyAdapter(this, tankTemps, this);
            recyclerView.setAdapter(tankTempVerifyAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
            recyclerView.addItemDecoration(itemDecor);
        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void insertToArray(int id) {
        tankRecordIds.add(id);

    }

    @Override
    public void removeFromArray(int id) {
        for (int i = 0; i < tankRecordIds.size(); i++) {
            if (tankRecordIds.get(i) == id) {
                tankRecordIds.remove(i);
                break;
            }
        }
    }

    @OnClick(R.id.btn_verify) void onVerificationClicked() {
        if (tankRecordIds.size() > 0 ) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Are you sure ??");
            builder.setMessage("You are about to verify " + tankRecordIds.size() + " records. Do you really want to proceed ?");
            builder.setCancelable(false);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    androidApplication = (AndroidApplication) getApplicationContext();
                    User user = androidApplication.getUser(getApplicationContext());
                    for (int  i = 0; i < tankRecordIds.size(); i++) {
                        dbHelper.updateTankTempVerification(tankRecordIds.get(i), user.id);
                    }
                    tankRecordIds.clear();
                    setRecyclerView();
                    Toasty.success(getApplicationContext(), "Successfully verified", Toast.LENGTH_SHORT, true).show();
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            builder.show();
        } else {
            Toasty.warning(this, "No data selected", Toast.LENGTH_SHORT, true).show();
        }
    }


    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy_MM_dd_hh_mm_a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }


}
