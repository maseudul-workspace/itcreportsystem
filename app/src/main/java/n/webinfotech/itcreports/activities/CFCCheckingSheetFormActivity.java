package n.webinfotech.itcreports.activities;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;

public class CFCCheckingSheetFormActivity extends AppCompatActivity {

    @BindView(R.id.spinner_shifts)
    Spinner spinnerShifts;
    @BindView(R.id.radio_group_sku)
    RadioGroup radioGroupSKU;
    @BindView(R.id.txt_view_machine_no)
    TextView txtViewMachine;
    @BindView(R.id.edit_text_torn)
    EditText editTextTorn;
    @BindView(R.id.edit_text_flap)
    EditText editTextFlap;
    @BindView(R.id.edit_text_dent)
    EditText editTextDent;
    @BindView(R.id.toggle_btn_coding_mono)
    ToggleButton toggleButtonCodingMono;
    @BindView(R.id.toggle_btn_coding_cfc)
    ToggleButton toggleButtonCodingCFC;
    @BindView(R.id.edit_text_qty_mono)
    EditText editTextQtyMono;
    @BindView(R.id.edit_text_qty_cfc)
    EditText editTextQtyCfc;
    @BindView(R.id.toggle_btn_tapping)
    ToggleButton toggleButtonTapping;
    String machine;
    String shift;
    String sku;
    String[] shiftList = {
            "Select Shift",
            "A",
            "B",
            "C"
    };
    DBHelper dbHelper;
    AndroidApplication androidApplication;
    @BindView(R.id.edit_text_remarks)
    EditText editTextRemarks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cfcchecking_sheet_form);
        ButterKnife.bind(this);
        machine = getIntent().getStringExtra("machineName");
        txtViewMachine.setText(machine);
        setUpFields();
        initializeDBHelper();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setUpFields() {
        radioGroupSKU.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                sku = checkedRadioButton.getText().toString();
            }
        });

        final ArrayAdapter<String> shiftListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, shiftList){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0)
                {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                }
                else
                {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;            }
        };

        shiftListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerShifts.setAdapter(shiftListAdapter);
        spinnerShifts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                shift = shiftList[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (shift ==  null || sku == null || editTextDent.getText().toString().trim().isEmpty() || editTextFlap.getText().toString().trim().isEmpty() || editTextQtyCfc.getText().toString().trim().isEmpty() || editTextQtyMono.getText().toString().trim().isEmpty() || editTextTorn.getText().toString().trim().isEmpty()) {
            Toasty.warning(this, "Some fields are empty", Toast.LENGTH_SHORT, true).show();
        } else {
            androidApplication = (AndroidApplication) getApplicationContext();
            User user = androidApplication.getUser(this);
            if (dbHelper.addCFCChecking(
                    shift,
                    machine,
                    sku,
                    editTextTorn.getText().toString(),
                    editTextFlap.getText().toString(),
                    editTextDent.getText().toString(),
                    toggleButtonCodingMono.getText().toString(),
                    editTextQtyMono.getText().toString(),
                    editTextQtyCfc.getText().toString(),
                    toggleButtonTapping.getText().toString(),
                    toggleButtonCodingCFC.getText().toString(),
                    editTextRemarks.getText().toString(),
                    user.id
            )) {
                Toasty.success(this, "Added Successfully", Toast.LENGTH_LONG, true).show();
                finish();
            } else {
                Toasty.error(this, "Failed to add", Toast.LENGTH_LONG, true).show();
            }

        }
    }

}
