package n.webinfotech.itcreports.activities;

import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.BakingTimeRecordVerficationAdapter;
import n.webinfotech.itcreports.dialogs.VerificationDialog;
import n.webinfotech.itcreports.models.BakingTimeRecord;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;

public class BakingTimeRecordsVerificationActivity extends AppCompatActivity implements BakingTimeRecordVerficationAdapter.Callback {

    @BindView(R.id.recycler_view_baking_time_record_verification)
    RecyclerView recyclerView;
    ArrayList<BakingTimeRecord> bakingTimeRecords;
    DBHelper dbHelper;
    ArrayList<Integer> bakingIds;
    BakingTimeRecordVerficationAdapter adapter;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baking_time_records_verification);
        ButterKnife.bind(this);
        initializeDBHelper();
        setRecyclerView();
        bakingIds = new ArrayList<>();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setRecyclerView() {
        bakingTimeRecords = new ArrayList<>();
        Cursor c = dbHelper.getBakingTimeRecordsUnverified();
        if (c.getCount() > 0) {
            while (c.moveToNext()) {
                BakingTimeRecord bakingTimeRecord = new BakingTimeRecord(
                  c.getInt(c.getColumnIndex("id")),
                        c.getString(c.getColumnIndex("date")),
                        c.getString(c.getColumnIndex("time")),
                        c.getString(c.getColumnIndex("shift")),
                        c.getString(c.getColumnIndex("oven_no")),
                        c.getString(c.getColumnIndex("sku")),
                        c.getString(c.getColumnIndex("bt_on_hmi")),
                        c.getString(c.getColumnIndex("actual_bt")),
                        c.getInt(c.getColumnIndex("user_id")),
                        c.getString(c.getColumnIndex("username")),
                        c.getInt(c.getColumnIndex("verify_status")),
                        c.getInt(c.getColumnIndex("verify_id"))
                );
                bakingTimeRecords.add(bakingTimeRecord);
            }
            adapter = new BakingTimeRecordVerficationAdapter(this, bakingTimeRecords,this);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
            recyclerView.addItemDecoration(itemDecor);
        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void insertToArray(int id) {
        bakingIds.add(id);
}

    @Override
    public void removeFromArray(int id) {
        for (int i = 0; i < bakingIds.size(); i++) {
            if (bakingIds.get(i) == id) {
                bakingIds.remove(i);
                break;
            }
        }
    }

    @OnClick(R.id.btn_verify) void onVerificationClicked() {
        if (bakingIds.size() > 0 ) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Are you sure !");
            builder.setMessage("You are about verify "+ bakingIds.size() +" records. Do you really want to proceed ?");
            builder.setCancelable(false);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    androidApplication = (AndroidApplication) getApplicationContext();
                    User user = androidApplication.getUser(getApplicationContext());
                    for (int  i = 0; i < bakingIds.size(); i++) {
                        dbHelper.updateBakingTimeRecordsVerification(bakingIds.get(i), user.id);
                    }
                    bakingIds.clear();
                    setRecyclerView();
                    Toasty.success(getApplicationContext(), "Successfully verified", Toast.LENGTH_SHORT, true).show();
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            builder.show();
        } else {
            Toasty.warning(this, "No data selected", Toast.LENGTH_SHORT, true).show();
        }
    }

}
