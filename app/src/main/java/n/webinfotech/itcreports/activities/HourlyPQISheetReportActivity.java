package n.webinfotech.itcreports.activities;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.HourlyPQISheetDataAdapter;
import n.webinfotech.itcreports.adapters.HourlyPQISheetVerficationAdapter;
import n.webinfotech.itcreports.dialogs.HourlyPQIEditDialog;
import n.webinfotech.itcreports.models.HourlyPQISheet;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.Helper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;
import static n.webinfotech.itcreports.util.Helper.getByteFromDrawable;
import static n.webinfotech.itcreports.util.Helper.getByteFromFile;

public class HourlyPQISheetReportActivity extends AppCompatActivity implements HourlyPQISheetDataAdapter.Callback, HourlyPQIEditDialog.Callback {

    DBHelper dbHelper;
    String startDate;
    String endDate;
    @BindView(R.id.txt_view_start_date)
    TextView txtViewStartDate;
    @BindView(R.id.txt_view_end_date)
    TextView txtViewEndDate;
    @BindView(R.id.recycler_view_hourly_pqi_sheet_data)
    RecyclerView recyclerView;
    Cursor cursor;
    HourlyPQIEditDialog hourlyPQIEditDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hourly_pqisheet_report);
        ButterKnife.bind(this);
        initializeDBHelper();
        startDate = Helper.getSearchCurrentDate();
        endDate = Helper.getSearchNextDate();
        cursor = dbHelper.getHourlyPQISheetData(startDate, endDate);
        txtViewStartDate.setText(startDate);
        txtViewEndDate.setText(endDate);
        if (cursor.getCount() == 0) {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
        setRecyclerView(cursor);
        setUpDialog();
    }

    public void setUpDialog() {
        hourlyPQIEditDialog = new HourlyPQIEditDialog(this, this, this);
        hourlyPQIEditDialog.setUpDialog();
    }

    public void saveData() {

        File sd = Environment.getExternalStorageDirectory();
        String csvFile = getDate() + "_hourly_pqi_sheet.xls";

        File directory = new File(sd.getAbsolutePath()+"/ITCReports/HourlyPQISheet");
        //create directory if not exist
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
        try {

            //file path
            File file = new File(directory, csvFile);
            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setLocale(new Locale("en", "EN"));
            WritableWorkbook workbook;
            workbook = Workbook.createWorkbook(file, wbSettings);
            //Excel sheet name. 0 represents first sheet
            WritableSheet sheet = workbook.createSheet("Hourly PQI Sheet", 0);


//            Excel Header
//            Add logo
            byte[] logoBytes = getByteFromDrawable(getDrawable(R.drawable.itc_logo));
            WritableImage writableImageLogo = new WritableImage(0, 0, 2,4, logoBytes);
            sheet.addImage(writableImageLogo);

//            Add header
            byte[] headerBytes = getByteFromDrawable(getDrawable(R.drawable.hourlypqj));
            WritableImage headerLogo = new WritableImage(2, 0, 17,4, headerBytes);
            sheet.addImage(headerLogo);

            sheet.addCell(new Label(19, 1, "Document No"));

            for(int i = 1; i < 21; i++) {
                CellView cell=sheet.getColumnView(i);
                cell.setAutosize(true);
                sheet.setColumnView(i, cell);
            }

            sheet.addCell(new Label(20, 1, "  "));


            sheet.addCell(new Label(19, 2, "Revision No"));
            sheet.addCell(new Label(20, 2, "  "));

            sheet.addCell(new Label(19, 3, "Effective Date"));
            sheet.addCell(new Label(20, 3, "  "));


            WritableFont arial10pt = new WritableFont(WritableFont.ARIAL, 9);
            arial10pt.setBoldStyle(WritableFont.BOLD);
            WritableCellFormat cellFormat = new WritableCellFormat(arial10pt);

            sheet.mergeCells(0,4,6,4);
            sheet.addCell(new Label(0, 4, "Frequency: Once In a Shift", cellFormat));

            sheet.mergeCells(7,4,10,4);
            sheet.addCell(new Label(7, 4, "PACKING QUALITY", cellFormat));

            sheet.mergeCells(11,4,15,4);
            sheet.addCell(new Label(11, 4, "PRODUCT QUALITY", cellFormat));

            sheet.mergeCells(16,4,17,4);
            sheet.addCell(new Label(16, 4, "Diameter", cellFormat));

            sheet.addCell(new Label(0, 5, "Date", cellFormat));

            sheet.addCell(new Label(1, 5, "Shift", cellFormat));

            sheet.addCell(new Label(2, 5, "Time", cellFormat));

            sheet.addCell(new Label(3, 5, "Pkt Time", cellFormat));

            sheet.addCell(new Label(4, 5, "Module", cellFormat));

            sheet.addCell(new Label(5, 5, "SKU", cellFormat));

            sheet.addCell(new Label(6, 5, "Stack", cellFormat));

            sheet.addCell(new Label(7, 5, "Coding", cellFormat));

            sheet.addCell(new Label(8, 5, "Loose Packet", cellFormat));

            sheet.addCell(new Label(9, 5, "Wrinkles", cellFormat));

            sheet.addCell(new Label(10, 5, "Sealing Quality", cellFormat));

            sheet.addCell(new Label(11, 5, "Broken/Chipped", cellFormat));

            sheet.addCell(new Label(12, 5, "Colour", cellFormat));

            sheet.addCell(new Label(13, 5, "Aroma", cellFormat));

            sheet.addCell(new Label(14, 5, "Foreign Matter", cellFormat));

            sheet.addCell(new Label(15, 5, "Other Defects", cellFormat));

            sheet.addCell(new Label(16, 5, "Value 1", cellFormat));

            sheet.addCell(new Label(17, 5, "Value 2", cellFormat));

            sheet.mergeCells(18,4,18,5);
            sheet.addCell(new Label(18, 4, "Checked By", cellFormat));

            sheet.mergeCells(19,4,19,5);
            sheet.addCell(new Label(19, 4, "Remarks", cellFormat));

            sheet.mergeCells(20,4,20,5);
            sheet.addCell(new Label(20, 4, "Verfied by", cellFormat));

            int row_count = 6;
            int current_verify_id = 0;
            int signature_from_row = 0;
            int signature_row_count = 0;
            int to_row;
            int flag = 1;
            int unverified_flag = 1;

            Cursor c = dbHelper.getHourlyPQISheetData(startDate, endDate);
            if (c.getCount() > 0) {
                Log.e("LogMsg", "Data vcount: " + c.getCount());
                while (c.moveToNext()) {
                    if (c.getInt(c.getColumnIndex("verify_status")) == 2) {
                        String sign = dbHelper.getVerifierSignature(c.getInt(c.getColumnIndex("verify_id")));
                        if (sign != null) {
                            byte[] signatureBytes = getByteFromFile(sign);
                            Log.e("LogMsg", "Hello1");
                            WritableImage signature = new WritableImage(20, row_count, 1, 1, signatureBytes);
                            sheet.addImage(signature);
                        }
                    }
                    sheet.addCell(new Label(0, row_count, c.getString(c.getColumnIndex("date"))));
                    sheet.addCell(new Label(1, row_count, c.getString(c.getColumnIndex("shift"))));
                    sheet.addCell(new Label(2, row_count, c.getString(c.getColumnIndex("time"))));
                    sheet.addCell(new Label(3, row_count, c.getString(c.getColumnIndex("pkd_time"))));
                    sheet.addCell(new Label(4, row_count, c.getString(c.getColumnIndex("machine_no"))));
                    sheet.addCell(new Label(5, row_count, c.getString(c.getColumnIndex("sku"))));
                    sheet.addCell(new Label(6, row_count, c.getString(c.getColumnIndex("stack"))));
                    sheet.addCell(new Label(7, row_count, c.getString(c.getColumnIndex("coding"))));
                    sheet.addCell(new Label(8, row_count, c.getString(c.getColumnIndex("loose_pkd"))));
                    sheet.addCell(new Label(9, row_count, c.getString(c.getColumnIndex("wrinkles"))));
                    sheet.addCell(new Label(10, row_count, c.getString(c.getColumnIndex("sealing_quality"))));
                    sheet.addCell(new Label(11, row_count, c.getString(c.getColumnIndex("broken_chipped"))));
                    sheet.addCell(new Label(12, row_count, c.getString(c.getColumnIndex("color"))));
                    sheet.addCell(new Label(13, row_count, c.getString(c.getColumnIndex("aroma"))));
                    sheet.addCell(new Label(14, row_count, c.getString(c.getColumnIndex("foreign_matter"))));
                    sheet.addCell(new Label(15, row_count, c.getString(c.getColumnIndex("other_defects"))));
                    sheet.addCell(new Label(16, row_count, c.getString(c.getColumnIndex("diameter1"))));
                    sheet.addCell(new Label(17, row_count, c.getString(c.getColumnIndex("diameter2"))));
                    sheet.addCell(new Label(18, row_count, c.getString(c.getColumnIndex("u_name"))));
                    sheet.addCell(new Label(19 , row_count, c.getString(c.getColumnIndex("remarks"))));
                    row_count++;
                }
            }


            workbook.write();
            workbook.close();
            Toast.makeText(this,
                    "Data Exported in a Excel Sheet", Toast.LENGTH_SHORT).show();
        } catch(Exception e){
            Log.e("LogMsg", "Workbook error" + e.getMessage());
            e.printStackTrace();
        }
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @OnClick(R.id.img_view_calendar_start_date) void onCalendarStartDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        startDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        startDate = Helper.changeDateFormat(startDate);
                        txtViewStartDate.setText(startDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.img_view_calendar_end_date) void onCalendarEndDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        endDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        endDate = Helper.changeDateFormat(endDate);
                        txtViewEndDate.setText(endDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.btn_export) void onExportClicked() {
        saveData();
    }
    @OnClick(R.id.btn_search) void onSearchClicked() {
        if (startDate == null || endDate == null){
            Toasty.warning(this, "Please enter both dates", Toast.LENGTH_SHORT).show();
        } else {
            cursor = dbHelper.getHourlyPQISheetData(startDate, endDate);
            Log.e("LogMsg", "Cursor Count: " + cursor.getCount());
            setRecyclerView(cursor);
            if (cursor.getCount() == 0) {
                Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setRecyclerView(Cursor c) {
        if (c.getCount() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            ArrayList<HourlyPQISheet> hourlyPQISheets = new ArrayList<>();
            while (c.moveToNext()) {
                HourlyPQISheet hourlyPQISheet = new HourlyPQISheet(
                        c.getInt(c.getColumnIndex("id")),
                        c.getString(c.getColumnIndex("date")),
                        c.getString(c.getColumnIndex("time")),
                        c.getString(c.getColumnIndex("shift")),
                        c.getString(c.getColumnIndex("pkd_time")),
                        c.getString(c.getColumnIndex("machine_no")),
                        c.getString(c.getColumnIndex("sku")),
                        c.getString(c.getColumnIndex("coding")),
                        c.getString(c.getColumnIndex("loose_pkd")),
                        c.getString(c.getColumnIndex("wrinkles")),
                        c.getString(c.getColumnIndex("stack")),
                        c.getString(c.getColumnIndex("diameter1")),
                        c.getString(c.getColumnIndex("diameter2")),
                        c.getString(c.getColumnIndex("sealing_quality")),
                        c.getString(c.getColumnIndex("broken_chipped")),
                        c.getString(c.getColumnIndex("color")),
                        c.getString(c.getColumnIndex("aroma")),
                        c.getString(c.getColumnIndex("foreign_matter")),
                        c.getString(c.getColumnIndex("other_defects")),
                        c.getString(c.getColumnIndex("remarks")),
                        c.getInt(c.getColumnIndex("user_id")),
                        c.getString(c.getColumnIndex("u_name")),
                        c.getInt(c.getColumnIndex("verify_status")),
                        c.getInt(c.getColumnIndex("verify_id"))
                );
                hourlyPQISheets.add(hourlyPQISheet);
            }
            HourlyPQISheetDataAdapter adapter = new HourlyPQISheetDataAdapter(this, hourlyPQISheets, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);
            DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
            recyclerView.addItemDecoration(itemDecor);
        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }

    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy_MM_dd_hh_mm_a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    public void onEditClicked(int id) {
        hourlyPQIEditDialog.setData(dbHelper.getSingleHourlyPQISheetData(id));
        hourlyPQIEditDialog.showDialog();
    }

    @Override
    public void onDeleteClicked(int id) {
        if (dbHelper.deleteSingleHourlyPQISheetData(id)) {
            Toasty.success(this, "Deleted Successfully", Toast.LENGTH_SHORT, true).show();
            onSearchClicked();
        } else {
            Toasty.error(this, "Unable To Delete", Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void onUpdateSucces() {
        onSearchClicked();
    }
}
