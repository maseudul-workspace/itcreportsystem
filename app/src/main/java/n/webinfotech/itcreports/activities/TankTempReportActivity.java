package n.webinfotech.itcreports.activities;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.TankTempDataAdapter;
import n.webinfotech.itcreports.adapters.TankTempVerifyAdapter;
import n.webinfotech.itcreports.dialogs.TankTempEditDialog;
import n.webinfotech.itcreports.models.TankTemp;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.Helper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;
import static n.webinfotech.itcreports.util.Helper.getByteFromFile;

public class TankTempReportActivity extends AppCompatActivity implements TankTempEditDialog.Callback, TankTempDataAdapter.Callback {

    DBHelper dbHelper;
    String startDate;
    String endDate;
    @BindView(R.id.txt_view_start_date)
    TextView txtViewStartDate;
    @BindView(R.id.txt_view_end_date)
    TextView txtViewEndDate;
    @BindView(R.id.recycler_view_tank_temp_data)
    RecyclerView recyclerView;
    Cursor cursor;
    TankTempEditDialog tankTempEditDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tank_temp_report);
        ButterKnife.bind(this);
        initializeDBHelper();
        startDate = Helper.getSearchCurrentDate();
        endDate = Helper.getSearchNextDate();
        cursor = dbHelper.getTankTempData(startDate, endDate);
        setRecyclerView(cursor);
        txtViewStartDate.setText(startDate);
        txtViewEndDate.setText(endDate);
        if (cursor.getCount() == 0) {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        }
        setUpDialog();
    }

    public void setUpDialog() {
        tankTempEditDialog = new TankTempEditDialog(this, this, this);
        tankTempEditDialog.setUpDialog();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void saveData() {

        File sd = Environment.getExternalStorageDirectory();
        String csvFile = getDate() + "_tank_temperature.xls";

        File directory = new File(sd.getAbsolutePath()+"/ITCReports/TankTemperature");
        //create directory if not exist
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
        //create directory if not exist
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
        try {

            //file path
            File file = new File(directory, csvFile);
            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setLocale(new Locale("en", "EN"));
            WritableWorkbook workbook;
            workbook = Workbook.createWorkbook(file, wbSettings);
            //Excel sheet name. 0 represents first sheet
            WritableSheet sheet = workbook.createSheet("Tank Temperature Record", 0);


//            Excel Header
//            Add logo
            byte[] logoBytes = Helper.getByteFromDrawable(getDrawable(R.drawable.itc_logo));
            WritableImage writableImageLogo = new WritableImage(0, 0, 2,4, logoBytes);
            sheet.addImage(writableImageLogo);

//            Add header
            byte[] headerBytes = Helper.getByteFromDrawable(getDrawable(R.drawable.tanktemp));
            WritableImage headerLogo = new WritableImage(2, 0, 5,4, headerBytes);
            sheet.addImage(headerLogo);

            sheet.addCell(new Label(7, 1, "Document No"));

            for(int i = 2; i < 9; i++) {
                CellView cell=sheet.getColumnView(i);
                cell.setAutosize(true);
                sheet.setColumnView(i, cell);
            }

            sheet.addCell(new Label(8, 1, "  "));


            sheet.addCell(new Label(7, 2, "Revision No"));
            sheet.addCell(new Label(8, 2, "  "));

            sheet.addCell(new Label(7, 3, "Effective Date"));
            sheet.addCell(new Label(8, 3, "  "));


            WritableFont arial10pt = new WritableFont(WritableFont.ARIAL, 9);
            arial10pt.setBoldStyle(WritableFont.BOLD);
            WritableCellFormat cellFormat = new WritableCellFormat(arial10pt);
            sheet.mergeCells(0,4,8,4);
            sheet.addCell(new Label(0, 4, "Frequency: Once In a Shift", cellFormat));

            sheet.addCell(new Label(0, 5, "Date", cellFormat));

            sheet.addCell(new Label(1, 5, "Shift", cellFormat));

            sheet.addCell(new Label(2, 5, "Time", cellFormat));

            sheet.addCell(new Label(3, 5, " Tank ", cellFormat));

            sheet.addCell(new Label(4, 5, " Set Temp(Deg C) ", cellFormat));

            sheet.addCell(new Label(5, 5, " Act Temp(Deg C) ", cellFormat));

            sheet.addCell(new Label(6, 5, "  Checked By  ", cellFormat));

            sheet.addCell(new Label(7, 5, "  Remarks  ", cellFormat));

            sheet.addCell(new Label(8, 5, "  Verified By  ", cellFormat));

            int row_count = 6;

            Cursor c = dbHelper.getTankTempData(startDate, endDate);
            if (c.getCount() > 0) {
                while (c.moveToNext()) {
                    if (c.getInt(c.getColumnIndex("verify_status")) == 2) {
                        String sign = dbHelper.getVerifierSignature(c.getInt(c.getColumnIndex("verify_id")));
                        if (sign != null) {
                            byte[] signatureBytes = getByteFromFile(sign);
                            WritableImage signature = new WritableImage(8, row_count, 1, 1, signatureBytes);
                            sheet.addImage(signature);
                        }
                    }
                    sheet.addCell(new Label(0, row_count, c.getString(c.getColumnIndex("date"))));
                    sheet.addCell(new Label(1, row_count, c.getString(c.getColumnIndex("shift"))));
                    sheet.addCell(new Label(2, row_count, c.getString(c.getColumnIndex("time"))));
                    sheet.addCell(new Label(3, row_count, c.getString(c.getColumnIndex("tank"))));
                    sheet.addCell(new Label(4, row_count, c.getString(c.getColumnIndex("set_temp"))));
                    sheet.addCell(new Label(5, row_count, c.getString(c.getColumnIndex("act_temp"))));
                    sheet.addCell(new Label(6, row_count, c.getString(c.getColumnIndex("u_name"))));
                    sheet.addCell(new Label(7, row_count, c.getString(c.getColumnIndex("remarks"))));
                    row_count++;
                }
            }


            workbook.write();
            workbook.close();
            Toast.makeText(this,
                    "Data Exported in a Excel Sheet", Toast.LENGTH_SHORT).show();
        } catch(Exception e){
            Log.e("LogMsg", "Workbook error" + e.getMessage());
            e.printStackTrace();
        }
    }

    @OnClick(R.id.img_view_calendar_start_date) void onCalendarStartDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        startDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        startDate = Helper.changeDateFormat(startDate);
                        txtViewStartDate.setText(startDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.img_view_calendar_end_date) void onCalendarEndDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        endDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        endDate = Helper.changeDateFormat(endDate);
                        txtViewEndDate.setText(endDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.btn_export) void onExportClicked() {
        saveData();
    }
    @OnClick(R.id.btn_search) void onSearchClicked() {
        if (startDate == null || endDate == null){
            Toasty.warning(this, "Please enter both dates", Toast.LENGTH_SHORT).show();
        } else {
            cursor = dbHelper.getTankTempData(startDate, endDate);
            setRecyclerView(cursor);
            if (cursor.getCount() == 0) {
                Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy_MM_dd_hh_mm_a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void setRecyclerView(Cursor c) {
        if (c.getCount() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            ArrayList<TankTemp> tankTemps = new ArrayList<>();
            while (c.moveToNext()) {
                TankTemp tankTemp = new TankTemp(
                        c.getInt(c.getColumnIndex("id")),
                        c.getString(c.getColumnIndex("date")),
                        c.getString(c.getColumnIndex("time")),
                        c.getString(c.getColumnIndex("shift")),
                        c.getString(c.getColumnIndex("tank")),
                        c.getString(c.getColumnIndex("act_temp")),
                        c.getString(c.getColumnIndex("set_temp")),
                        c.getString(c.getColumnIndex("remarks")),
                        c.getInt(c.getColumnIndex("user_id")),
                        c.getString(c.getColumnIndex("u_name")),
                        c.getInt(c.getColumnIndex("verify_status")),
                        c.getInt(c.getColumnIndex("verify_id"))
                );
                tankTemps.add(tankTemp);
            }
            TankTempDataAdapter adapter = new TankTempDataAdapter(this, tankTemps,this);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
            recyclerView.addItemDecoration(itemDecor);
        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onEditClicked(int id) {
        tankTempEditDialog.setData(dbHelper.getSingleTankTempData(id));
        tankTempEditDialog.showDialog();
    }

    @Override
    public void onDeleteClicked(int id) {
        if (dbHelper.deleteTankTempVerification(id)) {
            Toasty.success(this, "Deleted Successfully", Toast.LENGTH_SHORT, true).show();
            onSearchClicked();
        } else {
            Toasty.error(this, "Unable to delete", Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void onUpdateSucces() {
        onSearchClicked();
    }
}
