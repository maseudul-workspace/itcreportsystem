package n.webinfotech.itcreports.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.util.DBHelper;

public class AddUserActivity extends AppCompatActivity {

    @BindView(R.id.edit_text_username)
    EditText editTextUsername;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_repeat_password)
    EditText editTextRepeatPassword;
    DBHelper dbHelper;
    @BindView(R.id.radio_group_gender)
    RadioGroup radioGroupGender;
    @BindView(R.id.radio_group_user_type)
    RadioGroup radioGroupUserType;
    String gender = "Male";
    int userType = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        ButterKnife.bind(this);
        initializeDBHelper();
        setRadioButtons();
    }

    public void setRadioButtons() {
        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                gender = checkedRadioButton.getText().toString();
            }
        });
        radioGroupUserType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton)group.findViewById(checkedId);
                if (checkedRadioButton.getText().toString().equals("Employee")) {
                    userType = 2;
                } else {
                    userType = 3;
                }
            }
        });
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (editTextUsername.getText().toString().trim().isEmpty() || editTextName.getText().toString().trim().isEmpty() || editTextPassword.getText().toString().trim().isEmpty() || editTextRepeatPassword.getText().toString().trim().isEmpty() || userType == 0) {
            Toasty.error(this, "Fields missing", Toast.LENGTH_SHORT, true).show();
        } else if (!editTextPassword.getText().toString().equals(editTextRepeatPassword.getText().toString())) {
            Toasty.error(this, "Password Mismatch", Toast.LENGTH_SHORT, true).show();
        } else {

            if(dbHelper.createUser(
                    editTextUsername.getText().toString(),
                    editTextName.getText().toString(),
                    editTextPassword.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextPhone.getText().toString(),
                    gender,
                    userType
            )) {
                Toasty.success(this, "User created successfully", Toast.LENGTH_SHORT, true).show();
                editTextUsername.setText("");
                editTextPassword.setText("");
                editTextRepeatPassword.setText("");
                editTextName.setText("");
                editTextEmail.setText("");
                editTextPhone.setText("");
            } else {
                Toasty.error(this, "Something went wrong or User already exist", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

}
