package n.webinfotech.itcreports.activities;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.MachineListAdapter;
import n.webinfotech.itcreports.dialogs.EditMachineDialog;
import n.webinfotech.itcreports.dialogs.QRCodeDialog;
import n.webinfotech.itcreports.models.Machines;
import n.webinfotech.itcreports.models.UserList;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.QRCodeHelper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;

public class MachineListActivity extends AppCompatActivity implements MachineListAdapter.Callback, EditMachineDialog.Callback {

    @BindView(R.id.recycler_view_machine_list)
    RecyclerView recyclerViewMachineList;
    DBHelper dbHelper;
    ArrayList<Machines> machinesList;
    MachineListAdapter machineListAdapter;
    EditMachineDialog editMachineDialog;
    QRCodeDialog qrCodeDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_machine_list);
        ButterKnife.bind(this);
        initializeDBHelper();
        initialiseQRCodeDialog();
        initialiseEditMachineDialog();
        editMachineDialog.setUpDialog();
        qrCodeDialog.setUpDialog();
        machinesList = new ArrayList<>();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void initialiseEditMachineDialog() {
        editMachineDialog = new EditMachineDialog(this, this, this);
    }

    public void initialiseQRCodeDialog() {
        qrCodeDialog = new QRCodeDialog(this, this);
    }

    public void getMachineList() {
        machinesList.clear();
        Cursor c = dbHelper.getMachines();
        if (c.getCount() > 0) {
            Log.e("LogMsg", "Machine count list: " + c.getCount());
            int i = 0;
            while (c.moveToNext()) {
                i++;
                Log.e("LogMsg", "Index: " + i);
                Machines machines = new Machines(
                        c.getInt(c.getColumnIndex("id")),
                        c.getString(c.getColumnIndex("machine_name")),
                        c.getString(c.getColumnIndex("machine_number")),
                        c.getInt(c.getColumnIndex("status")),
                        c.getString(c.getColumnIndex("qr_text")),
                        c.getString(c.getColumnIndex("f_name"))
                );
                machinesList.add(machines);
            }
            machineListAdapter = new MachineListAdapter(this, machinesList, this);
            recyclerViewMachineList.setAdapter(machineListAdapter);
            recyclerViewMachineList.setLayoutManager(new LinearLayoutManager(this));
            DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
            recyclerViewMachineList.addItemDecoration(itemDecor);
        }
    }

    @Override
    public void onEditClicked(int id) {
        for(int i = 0; i < machinesList.size(); i++) {
            if (machinesList.get(i).id == id) {
                editMachineDialog.setEditTexts(machinesList.get(i).machineName, machinesList.get(i).machineNo, id);
                editMachineDialog.showDialog();
                break;
            }
        }
    }

    @Override
    public void changeMachineActivation(int id, int status) {
        if (dbHelper.changeMachineActivation(status, id)) {
            Toasty.success(this, "Machine changed successfully", Toast.LENGTH_SHORT, true).show();
            getMachineList();
        } else {
            Toasty.error(this, "Something went wrong", Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void onShowQRcodeClicked(int id, String qrText) {
        Bitmap bitmap = QRCodeHelper
                .newInstance(this)
                .setContent(qrText)
                .setErrorCorrectionLevel(ErrorCorrectionLevel.Q)
                .setMargin(2)
                .getQRCOde();
        qrCodeDialog.setImageView(bitmap);
        qrCodeDialog.showDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        getMachineList();
    }

    @Override
    public void updateMachine(String machineName, String machineNo, int id) {
        if (dbHelper.updateMachine(machineName, machineNo, id)) {
            Toasty.success(this, "Machine edited successfully", Toast.LENGTH_SHORT, true).show();
            editMachineDialog.hideDialog();
            getMachineList();
        } else {
            Toasty.error(this, "Something went wrong", Toast.LENGTH_SHORT, true).show();
        }
    }
}
