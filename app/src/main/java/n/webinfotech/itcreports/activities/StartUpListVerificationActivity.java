package n.webinfotech.itcreports.activities;

import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.StartUpCheckListAdapter;
import n.webinfotech.itcreports.dialogs.StartUpCheckListDataDialog;
import n.webinfotech.itcreports.models.StartCheckList;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;
import static android.graphics.drawable.ClipDrawable.VERTICAL;

public class StartUpListVerificationActivity extends AppCompatActivity implements StartUpCheckListAdapter.Callback {

    @BindView(R.id.recycler_view_start_up_check_list_verification)
    RecyclerView recyclerView;
    DBHelper dbHelper;
    StartUpCheckListDataDialog startUpCheckListDataDialog;
    ArrayList<Integer> checkListIds;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_up_list_verification);
        ButterKnife.bind(this);
        initializeDBHelper();
        setRecyclerView();
        initialiseDialog();
        checkListIds = new ArrayList<>();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void initialiseDialog() {
        startUpCheckListDataDialog = new StartUpCheckListDataDialog(this, this);
        startUpCheckListDataDialog.setUpDialog();
    }

    public void setRecyclerView() {
        Cursor c = dbHelper.getStartUpCheckListUnverified();
        ArrayList<StartCheckList> checkLists = new ArrayList<>();
        while (c.moveToNext()) {
            StartCheckList startCheckList = new StartCheckList(
                    c.getInt(c.getColumnIndex("id")),
                    c.getString(c.getColumnIndex("date_time")),
                    c.getString(c.getColumnIndex("username")),
                    c.getString(c.getColumnIndex("quality_manager_sign")),
                    c.getInt(c.getColumnIndex("verify_status")),
                    c.getInt(c.getColumnIndex("verify_id")),
                    c.getInt(c.getColumnIndex("seive1")),
                    c.getInt(c.getColumnIndex("seive2")),
                    c.getInt(c.getColumnIndex("seive3")),
                    c.getInt(c.getColumnIndex("seive4")),
                    c.getInt(c.getColumnIndex("packing1")),
                    c.getInt(c.getColumnIndex("packing2")),
                    c.getInt(c.getColumnIndex("packing3")),
                    c.getInt(c.getColumnIndex("packing4")),
                    c.getInt(c.getColumnIndex("packing5")),
                    c.getInt(c.getColumnIndex("packing6")),
                    c.getInt(c.getColumnIndex("packing7")),
                    c.getInt(c.getColumnIndex("packing8")),
                    c.getInt(c.getColumnIndex("packing9")),
                    c.getInt(c.getColumnIndex("packing10")),
                    c.getInt(c.getColumnIndex("packing11")),
                    c.getInt(c.getColumnIndex("packing12")),
                    c.getInt(c.getColumnIndex("packing13")),
                    c.getInt(c.getColumnIndex("packing14")),
                    c.getInt(c.getColumnIndex("packing15")),
                    c.getInt(c.getColumnIndex("packing16")),
                    c.getInt(c.getColumnIndex("packing17")),
                    c.getInt(c.getColumnIndex("packing18")),
                    c.getInt(c.getColumnIndex("packing19")),
                    c.getInt(c.getColumnIndex("packing20")),
                    c.getInt(c.getColumnIndex("packing21")),
                    c.getInt(c.getColumnIndex("packing22")),
                    c.getInt(c.getColumnIndex("packing23")),
                    c.getInt(c.getColumnIndex("packing24")),
                    c.getInt(c.getColumnIndex("oven1")),
                    c.getInt(c.getColumnIndex("oven2")),
                    c.getInt(c.getColumnIndex("oven3")),
                    c.getInt(c.getColumnIndex("oven4")),
                    c.getInt(c.getColumnIndex("oven5")),
                    c.getInt(c.getColumnIndex("vp1")),
                    c.getInt(c.getColumnIndex("vp2")),
                    c.getInt(c.getColumnIndex("vp3")),
                    c.getInt(c.getColumnIndex("vp4")),
                    c.getInt(c.getColumnIndex("vp5")),
                    c.getInt(c.getColumnIndex("vp6")),
                    c.getInt(c.getColumnIndex("vp7"))
                    );
            checkLists.add(startCheckList);
        }

        StartUpCheckListAdapter adapter = new StartUpCheckListAdapter(this, checkLists, this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecor);

    }

    @Override
    public void onViewClicked(int id, String title) {
        Cursor c = dbHelper.getStartUpCheckListData(id);
        startUpCheckListDataDialog.setData(c, title);
        startUpCheckListDataDialog.showDialog();
    }

    @Override
    public void insertToArray(int id) {
        checkListIds.add(id);
    }

    @Override
    public void removeFromArray(int id) {
        for (int i = 0; i < checkListIds.size(); i++) {
            if (checkListIds.get(i) == id) {
                checkListIds.remove(i);
                break;
            }
        }
    }

    @OnClick(R.id.btn_verify) void onVerificationClicked() {
        if (checkListIds.size() > 0 ) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Are you sure !");
            builder.setMessage("You are about to verify " + checkListIds.size() +" records. Do you really want to proceed ?");
            builder.setCancelable(false);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    androidApplication = (AndroidApplication) getApplicationContext();
                    User user = androidApplication.getUser(getApplicationContext());
                    for (int  i = 0; i < checkListIds.size(); i++) {
                        dbHelper.updateStartUpCheckListVerification(checkListIds.get(i), user.id);
                    }
                    checkListIds.clear();
                    setRecyclerView();
                    Toasty.success(getApplicationContext(), "Successfully verified", Toast.LENGTH_SHORT, true).show();
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            builder.show();
        } else {
            Toasty.warning(this, "No data selected", Toast.LENGTH_SHORT, true).show();
        }
    }

}
