package n.webinfotech.itcreports.activities;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.BatchNoinputAdapter;
import n.webinfotech.itcreports.models.BatchNo;
import n.webinfotech.itcreports.models.LastBatch;
import n.webinfotech.itcreports.util.DBHelper;

public class OnlineSensoryBatchActivity extends AppCompatActivity implements BatchNoinputAdapter.Callback {

    @BindView(R.id.recycler_view_input_batch_no)
    RecyclerView recyclerView;
    BatchNoinputAdapter adapter;
    ArrayList<Integer> batchNos;
    @BindView(R.id.txt_view_last_checked_date)
    TextView txtViewLastCheckedDate;
    @BindView(R.id.layout_last_batches)
    LinearLayout layoutBatches;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_sensory_batch);
        ButterKnife.bind(this);
        batchNos = new ArrayList<>();
        setRecyclerView();
        initializeDBHelper();

    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setLastBatches() {
        LastBatch lastBatch = dbHelper.getLastBatches();
        if (lastBatch != null) {
            txtViewLastCheckedDate.setText(lastBatch.dateTime);
            Cursor c = lastBatch.c;
            while (c.moveToNext()) {
                View view = this.getLayoutInflater().inflate(R.layout.layout_batch_no, layoutBatches, false);
                TextView txtViewBatch = (TextView) view.findViewById(R.id.txt_view_batch_no);
                txtViewBatch.setText(Integer.toString(c.getInt(c.getColumnIndex("batch_no"))));
                layoutBatches.addView(view);
            }
        }
    }

    public void setRecyclerView(){
        ArrayList<BatchNo> batchNos = new ArrayList<>();

        BatchNo batchNo1 = new BatchNo(1);
        BatchNo batchNo2 = new BatchNo(2);
        BatchNo batchNo3 = new BatchNo(3);
        BatchNo batchNo4 = new BatchNo(4);
        BatchNo batchNo5 = new BatchNo(5);
        BatchNo batchNo6 = new BatchNo(6);
        BatchNo batchNo7 = new BatchNo(7);
        BatchNo batchNo8 = new BatchNo(8);
        BatchNo batchNo9 = new BatchNo(9);
        BatchNo batchNo10 = new BatchNo(10);
        BatchNo batchNo11 = new BatchNo(11);
        BatchNo batchNo12 = new BatchNo(12);

        batchNos.add(batchNo1);
        batchNos.add(batchNo2);
        batchNos.add(batchNo3);
        batchNos.add(batchNo4);
        batchNos.add(batchNo5);
        batchNos.add(batchNo6);
        batchNos.add(batchNo7);
        batchNos.add(batchNo8);
        batchNos.add(batchNo9);
        batchNos.add(batchNo10);
        batchNos.add(batchNo11);
        batchNos.add(batchNo12);

        adapter = new BatchNoinputAdapter(this, batchNos, this);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onChecked(int number) {
        batchNos.add(number);
    }

    @Override
    public void onUnchecked(int number) {
        for (int i = 0; i < batchNos.size(); i++) {
            if (batchNos.get(i) == number) {
                batchNos.remove(i);
                break;
            }
        }
    }

    @OnClick(R.id.btn_next) void onNextClicked() {
        Intent intent = new Intent(this, OnlineSensoryEvaluationFormActivity.class);
        Collections.sort(batchNos);
        intent.putExtra("batchNos", batchNos);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        layoutBatches.removeAllViews();
        setLastBatches();
        super.onResume();
    }

}
