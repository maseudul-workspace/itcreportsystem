package n.webinfotech.itcreports.activities;

import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.MDRecordsAdapter;
import n.webinfotech.itcreports.dialogs.VerificationDialog;
import n.webinfotech.itcreports.models.MDRecord;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.SaveSignatureHelper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;

public class MDRecordsVerificationActivity extends AppCompatActivity implements MDRecordsAdapter.Callback {

    DBHelper dbHelper;
    @BindView(R.id.recycler_view_md_records_verification)
    RecyclerView recyclerViewMDRecords;
    ArrayList<Integer> mdRecordIds;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mdrecords_verification);
        ButterKnife.bind(this);
        mdRecordIds = new ArrayList<>();
        initializeDBHelper();
        setRecyclerView();

    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    public void setRecyclerView() {
        Cursor c = dbHelper.getMDRecordsUnverified();
        if (c.getCount() > 0) {
            ArrayList<MDRecord> mdRecords = new ArrayList<>();
            while (c.moveToNext()) {
                MDRecord mdRecord = new MDRecord(
                        c.getInt(c.getColumnIndex("id")),
                        c.getString(c.getColumnIndex("date")),
                        c.getString(c.getColumnIndex("time")),
                        c.getString(c.getColumnIndex("shift")),
                        c.getString(c.getColumnIndex("md_no")),
                        c.getString(c.getColumnIndex("prove_fe")),
                        c.getString(c.getColumnIndex("prove_nfe")),
                        c.getString(c.getColumnIndex("prove_ss")),
                        c.getString(c.getColumnIndex("inter_lock")),
                        c.getString(c.getColumnIndex("sensitivity")),
                        c.getString(c.getColumnIndex("remarks")),
                        c.getInt(c.getColumnIndex("user_id")),
                        c.getString(c.getColumnIndex("username")),
                        c.getInt(c.getColumnIndex("verify_status")),
                        c.getInt(c.getColumnIndex("verify_id"))
                );
                mdRecords.add(mdRecord);
            }
            recyclerViewMDRecords.setLayoutManager(new LinearLayoutManager(this));
            MDRecordsAdapter adapter = new MDRecordsAdapter(this, mdRecords, this);
            recyclerViewMDRecords.setAdapter(adapter);
            DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
            recyclerViewMDRecords.addItemDecoration(itemDecor);
        } else {
            recyclerViewMDRecords.setVisibility(View.GONE);
        }
    }

    @Override
    public void insertToArray(int id) {
        mdRecordIds.add(id);

    }

    @Override
    public void removeFromArray(int id) {
        for (int i = 0; i < mdRecordIds.size(); i++) {
            if (mdRecordIds.get(i) == id) {
                mdRecordIds.remove(i);
                break;
            }
        }
    }

    @OnClick(R.id.btn_verify) void onVerificationClicked() {
        if (mdRecordIds.size() > 0 ) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Are you sure !");
            builder.setMessage("You are about to verify " + mdRecordIds.size() + " records. Do you really want to proceed ?");
            builder.setCancelable(false);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    androidApplication = (AndroidApplication) getApplicationContext();
                    User user = androidApplication.getUser(getApplicationContext());
                    for (int  i = 0; i < mdRecordIds.size(); i++) {
                        dbHelper.updateMDRecordsVerification(mdRecordIds.get(i), user.id);
                    }
                    mdRecordIds.clear();
                    setRecyclerView();
                    Toasty.success(getApplicationContext(), "Successfully verified", Toast.LENGTH_SHORT, true).show();
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            builder.show();
        } else {
            Toasty.warning(this, "No data selected", Toast.LENGTH_SHORT, true).show();
        }
    }


    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy_MM_dd_hh_mm_a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

}
