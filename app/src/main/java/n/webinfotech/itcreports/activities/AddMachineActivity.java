package n.webinfotech.itcreports.activities;

import android.database.Cursor;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.util.DBHelper;

public class AddMachineActivity extends AppCompatActivity {

    @BindView(R.id.edit_text_machine_name)
    EditText editTextMachineName;
    @BindView(R.id.spinner_form_names)
    Spinner spinnerFormNames;
    DBHelper dbHelper;
    int formId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_machine);
        ButterKnife.bind(this);
        initializeDBHelper();
        getFormNames();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void getFormNames() {
        final List<String> formNames = new ArrayList<>();
        final List<Integer> formIds = new ArrayList<>();
        formNames.add("Choose Form");
        formIds.add(0);
        Cursor c = dbHelper.getFormNames();
        if (c.getCount() > 0) {
            while (c.moveToNext()) {
                formNames.add(c.getString(c.getColumnIndex("form_name")));
                formIds.add(c.getInt(c.getColumnIndex("id")));
            }

            final ArrayAdapter<String> formNamesAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, formNames){
                @Override
                public boolean isEnabled(int position) {
                    if(position == 0)
                    {
                        // Disable the first item from Spinner
                        // First item will be use for hint
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }

                @Override
                public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view;
                    if(position == 0){
                        // Set the hint text color gray
                        tv.setTextColor(Color.GRAY);
                    }
                    else {
                        tv.setTextColor(Color.BLACK);
                    }
                    return view;            }
            };

            formNamesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerFormNames.setAdapter(formNamesAdapter);
            spinnerFormNames.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    formId = formIds.get(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (editTextMachineName.getText().toString().trim().isEmpty() || formId == 0) {
            Toasty.warning(this, "Some fields are empty", Toast.LENGTH_SHORT, true).show();
        } else {
            if (dbHelper.addMachine(
                    editTextMachineName.getText().toString(),
                    "",
                    randomString(),
                    formId
            )) {
                Toasty.success(this, "Machine added successfully", Toast.LENGTH_SHORT, true).show();
                editTextMachineName.setText("");
            } else {
                Toasty.error(this, "Something went wrong", Toast.LENGTH_SHORT, true).show();
            }
        }
    }

    public String randomString() {

        String DATA = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Random RANDOM = new Random();
        StringBuilder sb = new StringBuilder(7);

        for (int i = 0; i < 7; i++) {
            sb.append(DATA.charAt(RANDOM.nextInt(DATA.length())));
        }

        return sb.toString();
    }

}
