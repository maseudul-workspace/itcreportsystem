package n.webinfotech.itcreports.activities;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.UserListAdapter;
import n.webinfotech.itcreports.dialogs.EditUserDialog;
import n.webinfotech.itcreports.models.Module;
import n.webinfotech.itcreports.models.UserList;
import n.webinfotech.itcreports.util.DBHelper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;

public class UserListActivity extends AdminBaseActivity implements UserListAdapter.Callback, EditUserDialog.Callback {

    DBHelper dbHelper;
    UserListAdapter userListAdapter;
    ArrayList<UserList> userLists;
    @BindView(R.id.recycler_view_user_list)
    RecyclerView recyclerViewUserList;
    EditUserDialog editUserDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_user_list);
        ButterKnife.bind(this);
        initializeDBHelper();
        initialiseUserEditDialog();
        editUserDialog.setUpDialog();
        userLists = new ArrayList<>();
    }

    public void initialiseUserEditDialog() {
        editUserDialog = new EditUserDialog(this, this, this);
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void getUsers() {
        userLists.clear();
        Cursor c = dbHelper.getUsers();
        Log.e("LogMsg", "User count: " + c.getCount());
        if (c.getCount() > 0) {
            while (c.moveToNext()) {
                UserList userList = new UserList(c.getInt(c.getColumnIndex("id")), c.getString(c.getColumnIndex("user_name")), c.getString(c.getColumnIndex("name")), c.getString(c.getColumnIndex("mobile")), c.getString(c.getColumnIndex("email")), c.getString(c.getColumnIndex("gender")), c.getInt(c.getColumnIndex("status")), c.getString(c.getColumnIndex("password")), c.getInt(c.getColumnIndex("user_type")));
                userLists.add(userList);
            }
            userListAdapter = new UserListAdapter(this, userLists, this);
            recyclerViewUserList.setAdapter(userListAdapter);
            recyclerViewUserList.setLayoutManager(new LinearLayoutManager(this));
            recyclerViewUserList.setNestedScrollingEnabled(false);
            DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
            recyclerViewUserList.addItemDecoration(itemDecor);
        } else {

        }
    }

    @Override
    public void onEditClicked(int id) {
        for (int i = 0; i < userLists.size(); i++) {
            if (userLists.get(i).id == id) {
                editUserDialog.setUpEditTexts(
                        userLists.get(i).name,
                        userLists.get(i).password,
                        userLists.get(i).email,
                        userLists.get(i).phone,
                        userLists.get(i).gender,
                        id
                );
                editUserDialog.showDialog();
                break;
            }
        }
    }

    @Override
    public void changeUserActivation(int id, int status) {
        if (dbHelper.changeUserActivation(status, id)) {
            Toasty.success(this, "User changed successfully", Toast.LENGTH_SHORT, true).show();
            getUsers();
        } else {
            Toasty.error(this, "Something went wrong", Toast.LENGTH_SHORT, true).show();
        }
    }

    @Override
    public void updateUser(String name, String password, String email, String phone, String gender, int id) {
        if (dbHelper.updateUser(name, password, email, phone, gender, id)) {
            Toasty.success(this, "User edited successfully", Toast.LENGTH_SHORT, true).show();
            editUserDialog.hideDialog();
            getUsers();
        } else {
            Toasty.error(this, "Something went wrong", Toast.LENGTH_SHORT, true).show();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        getUsers();
    }
}
