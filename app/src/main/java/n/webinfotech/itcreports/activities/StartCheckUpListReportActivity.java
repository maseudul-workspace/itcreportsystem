package n.webinfotech.itcreports.activities;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.StartUpCheckListAdapter;
import n.webinfotech.itcreports.adapters.StartUpCheckListDataAdapter;
import n.webinfotech.itcreports.dialogs.SelectModuleDialog;
import n.webinfotech.itcreports.dialogs.StartUpCheckListDataDialog;
import n.webinfotech.itcreports.models.StartCheckList;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.Helper;

import static n.webinfotech.itcreports.util.Helper.getByteFromDrawable;
import static n.webinfotech.itcreports.util.Helper.getByteFromFile;
import static n.webinfotech.itcreports.util.Helper.getDate;

public class StartCheckUpListReportActivity extends AppCompatActivity implements  SelectModuleDialog.Callback, StartUpCheckListDataAdapter.Callback {

    DBHelper dbHelper;
    String startDate;
    String endDate;
    @BindView(R.id.txt_view_start_date)
    TextView txtViewStartDate;
    @BindView(R.id.txt_view_end_date)
    TextView txtViewEndDate;
    @BindView(R.id.recycler_view_start_up_check_list_data)
    RecyclerView recyclerView;
    SelectModuleDialog selectModuleDialog;
    String module;
    Cursor cursor;
    StartUpCheckListDataDialog startUpCheckListDataDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_check_up_list_report);
        ButterKnife.bind(this);
        initializeDBHelper();
        setUpInitialDates();
        setSelectModuleDialog();
        selectModuleDialog.showDialog();
        initialiseDialog();
    }

    public void setUpInitialDates() {
        startDate = Helper.getSearchCurrentDate();
        endDate = Helper.getSearchNextDate();
        txtViewStartDate.setText(startDate);
        txtViewEndDate.setText(endDate);
    }

    public void initialiseDialog() {
        startUpCheckListDataDialog = new StartUpCheckListDataDialog(this, this);
        startUpCheckListDataDialog.setUpDialog();
    }

    @Override
    public void onModuleSelected(int moduleId) {
        if (moduleId == 1) {
            module = "M - 1";
        } else {
            module = "M - 2";
        }
        setData();
        selectModuleDialog.hideDialog();
    }

    public void setData() {
        cursor = dbHelper.getStartChecklistData(startDate, endDate, module);
        if (cursor.getCount() == 0) {
            Toast.makeText(this, "No data found", Toast.LENGTH_SHORT).show();
        } else {
            setRecyclerView(cursor);
        }
    }

    public void setRecyclerView(Cursor c) {
        ArrayList<StartCheckList> checkLists = new ArrayList<>();
        while (c.moveToNext()) {
            StartCheckList startCheckList = new StartCheckList(
                    c.getInt(c.getColumnIndex("id")),
                    c.getString(c.getColumnIndex("date_time")),
                    c.getString(c.getColumnIndex("username")),
                    c.getString(c.getColumnIndex("quality_manager_sign")),
                    c.getInt(c.getColumnIndex("verify_status")),
                    c.getInt(c.getColumnIndex("verify_id")),
                    c.getInt(c.getColumnIndex("seive1")),
                    c.getInt(c.getColumnIndex("seive2")),
                    c.getInt(c.getColumnIndex("seive3")),
                    c.getInt(c.getColumnIndex("seive4")),
                    c.getInt(c.getColumnIndex("packing1")),
                    c.getInt(c.getColumnIndex("packing2")),
                    c.getInt(c.getColumnIndex("packing3")),
                    c.getInt(c.getColumnIndex("packing4")),
                    c.getInt(c.getColumnIndex("packing5")),
                    c.getInt(c.getColumnIndex("packing6")),
                    c.getInt(c.getColumnIndex("packing7")),
                    c.getInt(c.getColumnIndex("packing8")),
                    c.getInt(c.getColumnIndex("packing9")),
                    c.getInt(c.getColumnIndex("packing10")),
                    c.getInt(c.getColumnIndex("packing11")),
                    c.getInt(c.getColumnIndex("packing12")),
                    c.getInt(c.getColumnIndex("packing13")),
                    c.getInt(c.getColumnIndex("packing14")),
                    c.getInt(c.getColumnIndex("packing15")),
                    c.getInt(c.getColumnIndex("packing16")),
                    c.getInt(c.getColumnIndex("packing17")),
                    c.getInt(c.getColumnIndex("packing18")),
                    c.getInt(c.getColumnIndex("packing19")),
                    c.getInt(c.getColumnIndex("packing20")),
                    c.getInt(c.getColumnIndex("packing21")),
                    c.getInt(c.getColumnIndex("packing22")),
                    c.getInt(c.getColumnIndex("packing23")),
                    c.getInt(c.getColumnIndex("packing24")),
                    c.getInt(c.getColumnIndex("oven1")),
                    c.getInt(c.getColumnIndex("oven2")),
                    c.getInt(c.getColumnIndex("oven3")),
                    c.getInt(c.getColumnIndex("oven4")),
                    c.getInt(c.getColumnIndex("oven5")),
                    c.getInt(c.getColumnIndex("vp1")),
                    c.getInt(c.getColumnIndex("vp2")),
                    c.getInt(c.getColumnIndex("vp3")),
                    c.getInt(c.getColumnIndex("vp4")),
                    c.getInt(c.getColumnIndex("vp5")),
                    c.getInt(c.getColumnIndex("vp6")),
                    c.getInt(c.getColumnIndex("vp7"))
            );
            checkLists.add(startCheckList);
        }

        StartUpCheckListDataAdapter adapter = new StartUpCheckListDataAdapter(this, checkLists, this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecor);
        recyclerView.setVisibility(View.VISIBLE);
    }

    public void setSelectModuleDialog() {
        selectModuleDialog = new SelectModuleDialog(this, this, this);
        selectModuleDialog.setUpDialog();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @OnClick(R.id.img_view_calendar_start_date) void onCalendarStartDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        startDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        startDate = Helper.changeDateFormat(startDate);
                        txtViewStartDate.setText(startDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.img_view_calendar_end_date) void onCalendarEndDate() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        endDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        endDate = Helper.changeDateFormat(endDate);
                        txtViewEndDate.setText(endDate);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }


    @Override
    public void onViewClicked(int id, String title) {
        Cursor c = dbHelper.getStartUpCheckListData(id);
        startUpCheckListDataDialog.setData(c, title);
        startUpCheckListDataDialog.showDialog();
    }

    @OnClick(R.id.btn_search) void onSearchClicked() {
        if (startDate == null || endDate == null){
            Toasty.warning(this, "Please enter both dates", Toast.LENGTH_SHORT).show();
        } else {
            selectModuleDialog.showDialog();
        }
    }

    @Override
    public void onDeleteClicked(int id) {
        if (dbHelper.deleteStartUpCheckListData(id)) {
            Toasty.success(this, "Deleted successfully", Toast.LENGTH_SHORT, true).show();
            recyclerView.setVisibility(View.GONE);
            setData();
        } else {
            Toasty.error(this, "Failed to delete data", Toast.LENGTH_SHORT, true).show();
        }
    }

    @OnClick(R.id.btn_export) void onExportClicked() {
        cursor = dbHelper.getStartChecklistData(startDate, endDate, module);
        if (cursor != null && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                File sd = Environment.getExternalStorageDirectory();
                String csvFile = cursor.getString(cursor.getColumnIndex("date_time")) + "_startup_checklist.xls";

                File directory = new File(sd.getAbsolutePath()+"/ITCReports/StartUpCheckList");
                //create directory if not exist
                if (!directory.isDirectory()) {
                    directory.mkdirs();
                }
                try {

                    //file path
                    File file = new File(directory, csvFile);
                    WorkbookSettings wbSettings = new WorkbookSettings();
                    wbSettings.setLocale(new Locale("en", "EN"));
                    WritableWorkbook workbook;
                    workbook = Workbook.createWorkbook(file, wbSettings);
                    //Excel sheet name. 0 represents first sheet
                    WritableSheet sheet = workbook.createSheet("Startup Check List", 0);


//            Excel Header
//            Add logo
                    byte[] logoBytes = getByteFromDrawable(getDrawable(R.drawable.itc_logo));
                    WritableImage writableImageLogo = new WritableImage(0, 0, 1, 4, logoBytes);
                    sheet.addImage(writableImageLogo);

//            Add header
                    byte[] headerBytes = getByteFromDrawable(getDrawable(R.drawable.onlinesensory));
                    WritableImage headerLogo = new WritableImage(1, 0, 3, 4, headerBytes);
                    sheet.addImage(headerLogo);

                    for (int i = 1; i < 6; i++) {
                        CellView cell = sheet.getColumnView(i);
                        cell.setAutosize(true);
                        sheet.setColumnView(i, cell);
                    }

                    WritableFont arial10pt = new WritableFont(WritableFont.ARIAL, 9);
                    arial10pt.setBoldStyle(WritableFont.BOLD);
                    WritableCellFormat cellFormat = new WritableCellFormat(arial10pt);


                    sheet.addCell(new Label(4, 0, "Date"));

                    sheet.addCell(new Label(5, 0, "  "));

                    sheet.addCell(new Label(4, 1, "Document No"));

                    sheet.addCell(new Label(5, 1, "  "));

                    sheet.addCell(new Label(4, 2, "Revision No"));
                    sheet.addCell(new Label(5, 2, "  "));

                    sheet.addCell(new Label(4, 3, "Effective Date"));
                    sheet.addCell(new Label(5, 3, "  "));

                    sheet.addCell(new Label(0, 4, " SL no ", cellFormat));
                    sheet.addCell(new Label(1, 4, " Department ", cellFormat));
                    sheet.addCell(new Label(2, 4, " OK/Not Ok ", cellFormat));
                    sheet.addCell(new Label(3, 4, " Remarks ", cellFormat));
                    sheet.addCell(new Label(4, 4, " Action Taken ", cellFormat));
                    sheet.addCell(new Label(5, 4, " Final Status ", cellFormat));

                    sheet.mergeCells(0,5,0,9);
                    sheet.addCell(new Label(0, 5, "1"));

                    sheet.mergeCells(0,10,0,34);
                    sheet.addCell(new Label(0, 10, "2"));

                    sheet.mergeCells(0,35,0,40);
                    sheet.addCell(new Label(0, 35, "3"));

                    sheet.mergeCells(0,41,0,48);
                    sheet.addCell(new Label(0, 41, "4"));

//            Seiving
                    sheet.addCell(new Label(1, 5, "Seiving", cellFormat));
                    sheet.addCell(new Label(1, 6, "Check the condition of  seives and magnets"));
                    sheet.addCell(new Label(1, 7, "All materials are stored on pallets "));
                    sheet.addCell(new Label(1, 8, "Floor Cleaning "));
                    sheet.addCell(new Label(1, 9, "Condition of PVC Stripes "));

//            Packing and manufacturing
                    sheet.addCell(new Label(1, 10, "Packing and Manufacturing", cellFormat));
                    sheet.addCell(new Label(1, 11, "Availability of IPA and Lint Roller "));
                    sheet.addCell(new Label(1, 12, "Entrance Pathway Should be Cleaned and Cleared from any stored materials "));
                    sheet.addCell(new Label(1, 13, "Cleanliness of process washroom "));
                    sheet.addCell(new Label(1, 14, "Availability of Hot Water in Washroom "));
                    sheet.addCell(new Label(1, 15, "All persons working are with Lindstorm Uniform "));
                    sheet.addCell(new Label(1, 16, "Cleanliness of freezer "));
                    sheet.addCell(new Label(1, 17, "Cleanliness of Scapper/Beater/Mixer bowl and overall mixing machine "));
                    sheet.addCell(new Label(1, 18, "Cleanliness of Weighing balance and Table "));
                    sheet.addCell(new Label(1, 19, "Cleanliness of all SS Containers/LIDS/Trolleys and Scoops "));
                    sheet.addCell(new Label(1, 20, "Cleanliness of Dough and Cream Hopper "));
                    sheet.addCell(new Label(1, 21, "Cleanliness of Feeding chute and flow controller of cream and dough "));
                    sheet.addCell(new Label(1, 22, "Cleanliness of compound nozzles and encruster "));
                    sheet.addCell(new Label(1, 23, "Cleanliness of panning belt/pressure roller and overall panning section "));
                    sheet.addCell(new Label(1, 24, "Cleanliness of SS trays used "));
                    sheet.addCell(new Label(1, 25, "No accumulation of empty Choco Cream Container "));
                    sheet.addCell(new Label(1, 26, "No containers / trays / crates are stored on floor "));
                    sheet.addCell(new Label(1, 27, "Ensure the instructor working "));
                    sheet.addCell(new Label(1, 28, "Floor Cleaning "));
                    sheet.addCell(new Label(1, 29, "Only Green Crates to be used "));
                    sheet.addCell(new Label(1, 30, "No unwanted substances like loose nuts / plastics etc above pannels and all other area "));
                    sheet.addCell(new Label(1, 31, "Cleanliness of taking conveyors "));
                    sheet.addCell(new Label(1, 32, "Cleanliness of MD Rejection Bean "));
                    sheet.addCell(new Label(1, 33, "Cleanliness of knives of all conveyors "));
                    sheet.addCell(new Label(1, 34, "Cleanliness of crates trays "));

//            Oven Section DH Room
                    sheet.addCell(new Label(1, 35, "Oven Section DH room", cellFormat));
                    sheet.addCell(new Label(1, 36, "Floor Cleaning "));
                    sheet.addCell(new Label(1, 37, "Cleanliness of moisture analyser and weighing balance: "));
                    sheet.addCell(new Label(1, 38, "Cleanliness of oven "));
                    sheet.addCell(new Label(1, 39, "Ensure the instructor working "));
                    sheet.addCell(new Label(1, 40, "RH and DH room is maintained "));

//            VP Section
                    sheet.addCell(new Label(1, 41, "VP Section", cellFormat));
                    sheet.addCell(new Label(1, 42, "Empty monocartons should be stored in trays / box container "));
                    sheet.addCell(new Label(1, 43, "Only green crates to be used "));
                    sheet.addCell(new Label(1, 44, "No unwanted material is stored above machines "));
                    sheet.addCell(new Label(1, 45, "Cleanliness of glowing unit "));
                    sheet.addCell(new Label(1, 46, "Ensure the instructor working "));
                    sheet.addCell(new Label(1, 47, "Checkweigher rejection bin is under lock and key "));
                    sheet.addCell(new Label(1, 48, "Floor Cleaning "));


                    Cursor sieve1Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("seive1")));
                    sieve1Cursor.moveToNext();
                    sheet.addCell(new Label(2, 6, sieve1Cursor.getString(sieve1Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 6, sieve1Cursor.getString(sieve1Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 6, sieve1Cursor.getString(sieve1Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 6, sieve1Cursor.getString(sieve1Cursor.getColumnIndex("final_status"))));

                    Cursor sieve2Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("seive2")));
                    sieve2Cursor.moveToNext();
                    sheet.addCell(new Label(2, 7, sieve2Cursor.getString(sieve2Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 7, sieve2Cursor.getString(sieve2Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 7, sieve2Cursor.getString(sieve2Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 7, sieve2Cursor.getString(sieve2Cursor.getColumnIndex("final_status"))));

                    Cursor sieve3Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("seive3")));
                    sieve3Cursor.moveToNext();
                    sheet.addCell(new Label(2, 8, sieve3Cursor.getString(sieve3Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 8, sieve3Cursor.getString(sieve3Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 8, sieve3Cursor.getString(sieve3Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 8, sieve3Cursor.getString(sieve3Cursor.getColumnIndex("final_status"))));

                    Cursor sieve4Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("seive4")));
                    sieve4Cursor.moveToNext();
                    sheet.addCell(new Label(2, 9, sieve4Cursor.getString(sieve4Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 9, sieve4Cursor.getString(sieve4Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 9, sieve4Cursor.getString(sieve4Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 9, sieve4Cursor.getString(sieve4Cursor.getColumnIndex("final_status"))));

                    Log.e("LogMsg", "Hello1");

                    Cursor packing1Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing1")));
                    packing1Cursor.moveToNext();
                    sheet.addCell(new Label(2, 11, packing1Cursor.getString(packing1Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 11, packing1Cursor.getString(packing1Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 11, packing1Cursor.getString(packing1Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 11, packing1Cursor.getString(packing1Cursor.getColumnIndex("final_status"))));

                    Cursor packing2Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing2")));
                    packing2Cursor.moveToNext();
                    sheet.addCell(new Label(2, 12, packing2Cursor.getString(packing2Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 12, packing2Cursor.getString(packing2Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 12, packing2Cursor.getString(packing2Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 12, packing2Cursor.getString(packing2Cursor.getColumnIndex("final_status"))));

                    Cursor packing3Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing3")));
                    packing3Cursor.moveToNext();
                    sheet.addCell(new Label(2, 13, packing3Cursor.getString(packing3Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 13, packing3Cursor.getString(packing3Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 13, packing3Cursor.getString(packing3Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 13, packing3Cursor.getString(packing3Cursor.getColumnIndex("final_status"))));

                    Cursor packing4Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing4")));
                    packing4Cursor.moveToNext();
                    sheet.addCell(new Label(2, 14, packing4Cursor.getString(packing4Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 14, packing4Cursor.getString(packing4Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 14, packing4Cursor.getString(packing4Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 14, packing4Cursor.getString(packing4Cursor.getColumnIndex("final_status"))));

                    Cursor packing5Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing5")));
                    packing5Cursor.moveToNext();
                    sheet.addCell(new Label(2, 15, packing5Cursor.getString(packing5Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 15, packing5Cursor.getString(packing5Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 15, packing5Cursor.getString(packing5Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 15, packing5Cursor.getString(packing5Cursor.getColumnIndex("final_status"))));

                    Cursor packing6Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing6")));
                    packing6Cursor.moveToNext();
                    sheet.addCell(new Label(2, 16, packing6Cursor.getString(packing6Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 16, packing6Cursor.getString(packing6Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 16, packing6Cursor.getString(packing6Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 16, packing6Cursor.getString(packing6Cursor.getColumnIndex("final_status"))));

                    Cursor packing7Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing7")));
                    packing7Cursor.moveToNext();
                    sheet.addCell(new Label(2, 17, packing7Cursor.getString(packing7Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 17, packing7Cursor.getString(packing7Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 17, packing7Cursor.getString(packing7Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 17, packing7Cursor.getString(packing7Cursor.getColumnIndex("final_status"))));

                    Cursor packing8Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing8")));
                    packing8Cursor.moveToNext();
                    sheet.addCell(new Label(2, 18, packing8Cursor.getString(packing8Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 18, packing8Cursor.getString(packing8Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 18, packing8Cursor.getString(packing8Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 18, packing8Cursor.getString(packing8Cursor.getColumnIndex("final_status"))));

                    Cursor packing9Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing9")));
                    packing9Cursor.moveToNext();
                    sheet.addCell(new Label(2, 19, packing9Cursor.getString(packing9Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 19, packing9Cursor.getString(packing9Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 19, packing9Cursor.getString(packing9Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 19, packing9Cursor.getString(packing9Cursor.getColumnIndex("final_status"))));

                    Cursor packing10Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing10")));
                    packing10Cursor.moveToNext();
                    sheet.addCell(new Label(2, 20, packing10Cursor.getString(packing10Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 20, packing10Cursor.getString(packing10Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 20, packing10Cursor.getString(packing10Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 20, packing10Cursor.getString(packing10Cursor.getColumnIndex("final_status"))));


                    Cursor packing11Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing11")));
                    packing11Cursor.moveToNext();
                    sheet.addCell(new Label(2, 21, packing11Cursor.getString(packing11Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 21, packing11Cursor.getString(packing11Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 21, packing11Cursor.getString(packing11Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 21, packing11Cursor.getString(packing11Cursor.getColumnIndex("final_status"))));


                    Cursor packing12Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing12")));
                    packing12Cursor.moveToNext();
                    sheet.addCell(new Label(2, 22, packing12Cursor.getString(packing12Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 22, packing12Cursor.getString(packing12Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 22, packing12Cursor.getString(packing12Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 22, packing12Cursor.getString(packing12Cursor.getColumnIndex("final_status"))));

                    Cursor packing13Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing13")));
                    packing13Cursor.moveToNext();
                    sheet.addCell(new Label(2, 23, packing13Cursor.getString(packing13Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 23, packing13Cursor.getString(packing13Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 23, packing13Cursor.getString(packing13Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 23, packing13Cursor.getString(packing13Cursor.getColumnIndex("final_status"))));


                    Cursor packing14Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing14")));
                    packing14Cursor.moveToNext();
                    sheet.addCell(new Label(2, 24, packing14Cursor.getString(packing14Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 24, packing14Cursor.getString(packing14Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 24, packing14Cursor.getString(packing14Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 24, packing14Cursor.getString(packing14Cursor.getColumnIndex("final_status"))));


                    Cursor packing15Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing15")));
                    packing15Cursor.moveToNext();
                    sheet.addCell(new Label(2, 25, packing15Cursor.getString(packing15Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 25, packing15Cursor.getString(packing15Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 25, packing15Cursor.getString(packing15Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 25, packing15Cursor.getString(packing15Cursor.getColumnIndex("final_status"))));


                    Cursor packing16Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing16")));
                    packing16Cursor.moveToNext();
                    sheet.addCell(new Label(2, 26, packing16Cursor.getString(packing16Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 26, packing16Cursor.getString(packing16Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 26, packing16Cursor.getString(packing16Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 26, packing16Cursor.getString(packing16Cursor.getColumnIndex("final_status"))));


                    Cursor packing17Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing17")));
                    packing17Cursor.moveToNext();
                    sheet.addCell(new Label(2, 27, packing17Cursor.getString(packing17Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 27, packing17Cursor.getString(packing17Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 27, packing17Cursor.getString(packing17Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 27, packing17Cursor.getString(packing17Cursor.getColumnIndex("final_status"))));


                    Cursor packing18Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing18")));
                    packing18Cursor.moveToNext();
                    sheet.addCell(new Label(2, 28, packing18Cursor.getString(packing18Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 28, packing18Cursor.getString(packing18Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 28, packing18Cursor.getString(packing18Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 28, packing18Cursor.getString(packing18Cursor.getColumnIndex("final_status"))));


                    Cursor packing19Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing19")));
                    packing19Cursor.moveToNext();
                    sheet.addCell(new Label(2, 29, packing19Cursor.getString(packing19Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 29, packing19Cursor.getString(packing19Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 29, packing19Cursor.getString(packing19Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 29, packing19Cursor.getString(packing19Cursor.getColumnIndex("final_status"))));


                    Cursor packing20Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing20")));
                    packing20Cursor.moveToNext();
                    sheet.addCell(new Label(2, 30, packing20Cursor.getString(packing20Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 30, packing20Cursor.getString(packing20Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 30, packing20Cursor.getString(packing20Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 30, packing20Cursor.getString(packing20Cursor.getColumnIndex("final_status"))));

                    Cursor packing21Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing21")));
                    packing21Cursor.moveToNext();
                    sheet.addCell(new Label(2, 31, packing21Cursor.getString(packing21Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 31, packing21Cursor.getString(packing21Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 31, packing21Cursor.getString(packing21Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 31, packing21Cursor.getString(packing21Cursor.getColumnIndex("final_status"))));

                    Cursor packing22Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing22")));
                    packing22Cursor.moveToNext();
                    sheet.addCell(new Label(2, 32, packing22Cursor.getString(packing22Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 32, packing22Cursor.getString(packing22Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 32, packing22Cursor.getString(packing22Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 32, packing22Cursor.getString(packing22Cursor.getColumnIndex("final_status"))));

                    Cursor packing23Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing23")));
                    packing23Cursor.moveToNext();
                    sheet.addCell(new Label(2, 33, packing23Cursor.getString(packing23Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 33, packing23Cursor.getString(packing23Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 33, packing23Cursor.getString(packing23Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 33, packing23Cursor.getString(packing23Cursor.getColumnIndex("final_status"))));

                    Cursor packing24Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("packing24")));
                    packing24Cursor.moveToNext();
                    sheet.addCell(new Label(2, 34, packing24Cursor.getString(packing24Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 34, packing24Cursor.getString(packing24Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 34, packing24Cursor.getString(packing24Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 34, packing24Cursor.getString(packing24Cursor.getColumnIndex("final_status"))));

                    Cursor oven1Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("oven1")));
                    oven1Cursor.moveToNext();
                    sheet.addCell(new Label(2, 36, oven1Cursor.getString(oven1Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 36, oven1Cursor.getString(oven1Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 36, oven1Cursor.getString(oven1Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 36, oven1Cursor.getString(oven1Cursor.getColumnIndex("final_status"))));

                    Cursor oven2Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("oven2")));
                    oven2Cursor.moveToNext();
                    sheet.addCell(new Label(2, 37, oven2Cursor.getString(oven2Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 37, oven2Cursor.getString(oven2Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 37, oven2Cursor.getString(oven2Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 37, oven2Cursor.getString(oven2Cursor.getColumnIndex("final_status"))));

                    Cursor oven3Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("oven3")));
                    oven3Cursor.moveToNext();
                    sheet.addCell(new Label(2, 38, oven3Cursor.getString(oven3Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 38, oven3Cursor.getString(oven3Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 38, oven3Cursor.getString(oven3Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 38, oven3Cursor.getString(oven3Cursor.getColumnIndex("final_status"))));

                    Cursor oven4Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("oven4")));
                    oven4Cursor.moveToNext();
                    sheet.addCell(new Label(2, 39, oven4Cursor.getString(oven4Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 39, oven4Cursor.getString(oven4Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 39, oven4Cursor.getString(oven4Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 39, oven4Cursor.getString(oven4Cursor.getColumnIndex("final_status"))));

                    Cursor oven5Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("oven5")));
                    oven5Cursor.moveToNext();
                    sheet.addCell(new Label(2, 40, oven5Cursor.getString(oven5Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 40, oven5Cursor.getString(oven5Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 40, oven5Cursor.getString(oven5Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 40, oven5Cursor.getString(oven5Cursor.getColumnIndex("final_status"))));

                    Cursor vp1Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("vp1")));
                    vp1Cursor.moveToNext();
                    sheet.addCell(new Label(2, 42, vp1Cursor.getString(vp1Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 42, vp1Cursor.getString(vp1Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 42, vp1Cursor.getString(vp1Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 42, vp1Cursor.getString(vp1Cursor.getColumnIndex("final_status"))));

                    Cursor vp2Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("vp2")));
                    vp2Cursor.moveToNext();
                    sheet.addCell(new Label(2, 43, vp2Cursor.getString(vp2Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 43, vp2Cursor.getString(vp2Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 43, vp2Cursor.getString(vp2Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 43, vp2Cursor.getString(vp2Cursor.getColumnIndex("final_status"))));

                    Cursor vp3Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("vp3")));
                    vp3Cursor.moveToNext();
                    sheet.addCell(new Label(2, 44, vp3Cursor.getString(vp3Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 44, vp3Cursor.getString(vp3Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 44, vp3Cursor.getString(vp3Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 44, vp3Cursor.getString(vp3Cursor.getColumnIndex("final_status"))));

                    Cursor vp4Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("vp4")));
                    vp4Cursor.moveToNext();
                    sheet.addCell(new Label(2, 45, vp4Cursor.getString(vp4Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 45, vp4Cursor.getString(vp4Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 45, vp4Cursor.getString(vp4Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 45, vp4Cursor.getString(vp4Cursor.getColumnIndex("final_status"))));

                    Cursor vp5Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("vp5")));
                    vp5Cursor.moveToNext();
                    sheet.addCell(new Label(2, 46, vp5Cursor.getString(vp5Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 46, vp5Cursor.getString(vp5Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 46, vp5Cursor.getString(vp5Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 46, vp5Cursor.getString(vp5Cursor.getColumnIndex("final_status"))));

                    Cursor vp6Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("vp6")));
                    vp6Cursor.moveToNext();
                    sheet.addCell(new Label(2, 47, vp6Cursor.getString(vp6Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 47, vp6Cursor.getString(vp6Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 47, vp6Cursor.getString(vp6Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 47, vp6Cursor.getString(vp6Cursor.getColumnIndex("final_status"))));

                    Cursor vp7Cursor = dbHelper.getStartUpCheckListData(cursor.getInt(cursor.getColumnIndex("vp7")));
                    vp7Cursor.moveToNext();
                    sheet.addCell(new Label(2, 48, vp7Cursor.getString(vp7Cursor.getColumnIndex("status"))));
                    sheet.addCell(new Label(3, 48, vp7Cursor.getString(vp7Cursor.getColumnIndex("remarks"))));
                    sheet.addCell(new Label(4, 48, vp7Cursor.getString(vp7Cursor.getColumnIndex("action_taken"))));
                    sheet.addCell(new Label(5, 48, vp7Cursor.getString(vp7Cursor.getColumnIndex("final_status"))));

                    sheet.addCell(new Label(1, 50, "Quality Manager"));

                    String qualityManagerSign = cursor.getString(cursor.getColumnIndex("quality_manager_sign"));
                    if (qualityManagerSign != null) {
                        byte[] signatureBytes = getByteFromFile(qualityManagerSign);
                        WritableImage signature = new WritableImage(2, 50, 1, 1, signatureBytes);
                        sheet.addImage(signature);
                    }

                    sheet.addCell(new Label(1, 52, "Verifier Signature"));

                    if (cursor.getInt(cursor.getColumnIndex("verify_status")) == 2) {
                        String sign = dbHelper.getVerifierSignature(cursor.getInt(cursor.getColumnIndex("verify_id")));
                        if (sign != null) {
                            byte[] signatureBytes = getByteFromFile(sign);
                            WritableImage signature = new WritableImage(2, 52, 1, 1, signatureBytes);
                            sheet.addImage(signature);
                        }
                    }

                    workbook.write();
                    workbook.close();
                    Toast.makeText(this,
                            "Data Exported in a Excel Sheet", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("LogMsg", "Error" + e.getMessage());
                }

            }
        }

    }

}
