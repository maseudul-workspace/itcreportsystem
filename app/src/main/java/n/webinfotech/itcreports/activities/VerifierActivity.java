package n.webinfotech.itcreports.activities;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.Forms;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;

public class VerifierActivity extends AppCompatActivity {

    @BindView(R.id.txt_view_username)
    TextView txtViewUsername;
    @BindView(R.id.btn_hourly_pqi_sheet)
    Button btnHourlyPQI;
    @BindView(R.id.txt_view_hourly_count)
    TextView txtViewHourlyCount;
    @BindView(R.id.btn_verify_md_records)
    Button btnMDrecords;
    @BindView(R.id.txt_view_md_count)
    TextView txtViewMDCount;
    @BindView(R.id.btn_baking_record_verify)
    Button btnBaking;
    @BindView(R.id.txt_view_baking_count)
    TextView txtViewBakingCount;
    @BindView(R.id.btn_cfc_check_verify)
    Button btnCFC;
    @BindView(R.id.txt_view_cfc_count)
    TextView txtViewCFCCount;
    @BindView(R.id.btn_dh_room_verification)
    Button btnDH;
    @BindView(R.id.txt_view_dh_count)
    TextView txtViewDHCount;
    @BindView(R.id.btn_sieve_magnet_verify)
    Button btnSieve;
    @BindView(R.id.txt_view_sieve_count)
    TextView txtViewSieveCount;
    @BindView(R.id.btn_tank_temp)
    Button btnTank;
    @BindView(R.id.txt_view_tank_count)
    TextView txtViewTankCount;
    @BindView(R.id.btn_check_weighe_verify)
    Button btnWeigh;
    @BindView(R.id.txt_view_check_count)
    TextView txtViewCheckCount;
    @BindView(R.id.txt_view_start_up_count)
    TextView txtViewStartUpCheckListCount;
    @BindView(R.id.btn_online_sensory_verify)
    Button btnOnline;
    @BindView(R.id.txt_view_sensory_count)
    TextView txtViewSensoryCount;
    @BindView(R.id.btn_start_up_checklist_verify)
    Button btnStartUpChecklist;
    DBHelper dbHelper;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifier);
        ButterKnife.bind(this);
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        user = androidApplication.getUser(this);
        txtViewUsername.setText("Welcome " + user.name);
        initializeDBHelper();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void checkFormsVerfication() {
        ArrayList<Forms> arrayList = dbHelper.getFormsVerficationStatus();
        for (int i = 0; i < arrayList.size(); i++) {
            switch (arrayList.get(i).id) {
                case 2:
                    if(arrayList.get(i).verified) {
                        btnTank.setVisibility(View.GONE);
                    } else {
                        txtViewTankCount.setText(arrayList.get(i).unverifyCount + " unverified records");
                    }
                    break;
                case 3:
                    if(arrayList.get(i).verified) {
                        btnDH.setVisibility(View.GONE);
                    }
                    else {
                        txtViewDHCount.setText(arrayList.get(i).unverifyCount + " unverified records");
                    }
                    break;
                case 4:
                    if(arrayList.get(i).verified) {
                        btnCFC.setVisibility(View.GONE);
                    } else {
                        txtViewCFCCount.setText(arrayList.get(i).unverifyCount + " unverified records");
                    }
                    break;
                case 5:
                    if(arrayList.get(i).verified) {
                        btnMDrecords.setVisibility(View.GONE);
                    } else {
                        txtViewMDCount.setText(arrayList.get(i).unverifyCount + " unverified records");
                    }
                    break;
                case 6:
                    if(arrayList.get(i).verified) {
                        btnSieve.setVisibility(View.GONE);
                    } else {
                        txtViewSieveCount.setText(arrayList.get(i).unverifyCount + " unverified records");
                    }
                    break;
                case 8:
                    if(arrayList.get(i).verified) {
                        btnBaking.setVisibility(View.GONE);
                    } else {
                        txtViewBakingCount.setText(arrayList.get(i).unverifyCount + " unverified records");
                    }
                    break;
                case 9:
                    if(arrayList.get(i).verified) {
                        btnWeigh.setVisibility(View.GONE);
                    } else {
                        txtViewCheckCount.setText(arrayList.get(i).unverifyCount + " unverified records");
                    }
                    break;
                case 10:
                    if(arrayList.get(i).verified) {
                        btnOnline.setVisibility(View.GONE);
                    } else {
                        txtViewSensoryCount.setText(arrayList.get(i).unverifyCount + " unverified records");
                    }
                    break;
                case 11:
                    if(arrayList.get(i).verified) {
                        btnHourlyPQI.setVisibility(View.GONE);
                    } else {
                        txtViewHourlyCount.setText(arrayList.get(i).unverifyCount + " unverified records");
                    }
                    break;

            }
        }
    }

    @OnClick(R.id.btn_hourly_pqi_sheet) void onHourlyPQISheetClicked() {
        if (dbHelper.getVerifierSignature(user.id) == null) {
            Toasty.warning(this, "Please add signature", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(this, HourlyPQISheetVerificationActivity.class);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_verify_md_records) void onMDRecordVerifyClicked() {
        if (dbHelper.getVerifierSignature(user.id) == null) {
            Toasty.warning(this, "Please add signature", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(this, MDRecordsVerificationActivity.class);
            startActivity(intent);
        }

    }

    @OnClick(R.id.btn_baking_record_verify) void onBakingRecordClicked() {
        if (dbHelper.getVerifierSignature(user.id) == null) {
            Toasty.warning(this, "Please add signature", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(this, BakingTimeRecordsVerificationActivity.class);
            startActivity(intent);
        }

    }



    @OnClick(R.id.btn_cfc_check_verify) void onCFCVerifyClicked() {
        if (dbHelper.getVerifierSignature(user.id) == null) {
            Toasty.warning(this, "Please add signature", Toast.LENGTH_SHORT).show();
        } else {
            Intent cfcVerifyIntent = new Intent(this, CFCCheckingSheetVerificationActivity.class);
            startActivity(cfcVerifyIntent);
        }
    }

    @OnClick(R.id.btn_dh_room_verification) void onDHRoomVerificationClicked() {
        if (dbHelper.getVerifierSignature(user.id) == null) {
            Toasty.warning(this, "Please add signature", Toast.LENGTH_SHORT).show();
        } else {
            Intent dhRoomVerifyIntent = new Intent(this, DHRoomRecordsVerificationActivity.class);
            startActivity(dhRoomVerifyIntent);
        }

    }

    @OnClick(R.id.btn_sieve_magnet_verify) void onSieveMagnetVerifyClicked() {
        if (dbHelper.getVerifierSignature(user.id) == null) {
            Toasty.warning(this, "Please add signature", Toast.LENGTH_SHORT).show();
        } else {
            Intent sieveMagnetIntent = new Intent(this, SieveAndMagnetChecklistVerificationActivity.class);
            startActivity(sieveMagnetIntent);
        }

    }

    @OnClick(R.id.btn_tank_temp) void onTankTempClicked() {
        if (dbHelper.getVerifierSignature(user.id) == null) {
            Toasty.warning(this, "Please add signature", Toast.LENGTH_SHORT).show();
        } else {
            Intent tankTempIntent = new Intent(this, TankTempVerificationActivity.class);
            startActivity(tankTempIntent);
        }
    }

    @OnClick(R.id.btn_check_weighe_verify) void onCheckWeigherClicked() {
        if (dbHelper.getVerifierSignature(user.id) == null) {
            Toasty.warning(this, "Please add signature", Toast.LENGTH_SHORT).show();
        } else {
            Intent checkWeighIntent = new Intent(this, CheckWeigherVerificationActivity.class);
            startActivity(checkWeighIntent);
        }
    }

    @OnClick(R.id.btn_online_sensory_verify) void onOnlineSensoryVerifyClicked() {
        if (dbHelper.getVerifierSignature(user.id) == null) {
            Toasty.warning(this, "Please add signature", Toast.LENGTH_SHORT).show();
        } else {
            Intent onlineSensoryIntent = new Intent(this, OnlineSensoryVerificationActivity.class);
            startActivity(onlineSensoryIntent);
        }

    }

    @OnClick(R.id.btn_signature_view) void onSignatureClicked() {
        Intent signatureIntent = new Intent(this, AddSignatureActivity.class);
        startActivity(signatureIntent);
    }

    @OnClick(R.id.btn_start_up_checklist_verify) void onOnStartUpChecklistVerify() {
        Intent intent = new Intent(this, StartUpListVerificationActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_logout) void onLogoutClicked() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setUser(this, null);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.btn_hourly_pqi_sheet_view) void onHourlyPQISheetViewClicked() {
        Intent hourlyPQISheet = new Intent(getApplicationContext(), HourlyPQISheetReportActivity.class);
        startActivity(hourlyPQISheet);
    }

    @OnClick(R.id.btn_baking_record_view) void onBakingRecordViewClicked() {
        Intent bakingTimeRecordsReport = new Intent(getApplicationContext(), BakingTimeRecordReportActivity.class);
        startActivity(bakingTimeRecordsReport);
    }

    @OnClick(R.id.btn_cfc_check_view) void onCFCCheckViewClicked() {
        Intent cfcSheet = new Intent(getApplicationContext(), CFCCheckingSheetReportActviity.class);
        startActivity(cfcSheet);
    }

    @OnClick(R.id.btn_check_weighe_view) void onBtnCheckWeigherView() {
        Intent checkWeigherIntent = new Intent(getApplicationContext(), CheckWeigherReportActivity.class);
        startActivity(checkWeigherIntent);
    }

    @OnClick(R.id.btn_dh_room_view) void onDHRoomViewClicked() {
        Intent dhRoomIntent = new Intent(getApplicationContext(), DHRoomRecordReportActivity.class);
        startActivity(dhRoomIntent);
    }

    @OnClick(R.id.btn_online_sensory_view) void onOnlineSensoryViewClicked() {
        Intent onlineSensoryIntent = new Intent(getApplicationContext(), OnlineSensoryEvaluationReportActivity.class);
        startActivity(onlineSensoryIntent);
    }

    @OnClick(R.id.btn_sieve_magnet_view) void onSieveMagnetView() {
        Intent sieveMagnetIntent = new Intent(getApplicationContext(), SieveAndMagnetChecklistReportActivity.class);
        startActivity(sieveMagnetIntent);
    }

    @OnClick(R.id.btn_start_up_checklist_view) void onStartUpChecklistViewClicked() {
        Intent startUpCheckListIntent = new Intent(getApplicationContext(), StartCheckUpListReportActivity.class);
        startActivity(startUpCheckListIntent);
    }

    @OnClick(R.id.btn_tank_temp_view) void onTankTempView() {
        Intent tabkTempIntent = new Intent(getApplicationContext(), TankTempReportActivity.class);
        startActivity(tabkTempIntent);
    }

    @OnClick(R.id.btn_view_md_records) void mdRecordsViewClicked() {
        Intent mdIntent = new Intent(getApplicationContext(), MDRecordReportActivity.class);
        startActivity(mdIntent);
    }

    @Override
    protected void onResume() {
        checkFormsVerfication();
        Cursor c = dbHelper.getStartUpCheckListUnverified();
        if (c.getCount() > 0) {
            txtViewStartUpCheckListCount.setText(c.getCount() + " unverified records");
            btnStartUpChecklist.setVisibility(View.VISIBLE);
        } else {
            txtViewStartUpCheckListCount.setText("");
            btnStartUpChecklist.setVisibility(View.GONE);
        }
        super.onResume();
    }

}
