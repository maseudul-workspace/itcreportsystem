package n.webinfotech.itcreports.activities;

import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.HourlyPQISheetVerficationAdapter;
import n.webinfotech.itcreports.dialogs.VerificationDialog;
import n.webinfotech.itcreports.models.HourlyPQISheet;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.SaveSignatureHelper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;

public class HourlyPQISheetVerificationActivity extends AppCompatActivity implements HourlyPQISheetVerficationAdapter.Callback {

    DBHelper dbHelper;
    @BindView(R.id.recycler_view_hourly_pqi_sheet_verification)
    RecyclerView recyclerView;
    ArrayList<Integer> hourlyPqiIds;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hourly_pqisheet_verification);
        ButterKnife.bind(this);
        initializeDBHelper();
        setRecyclerView();
        hourlyPqiIds = new ArrayList<>();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setRecyclerView() {
        Cursor c = dbHelper.getHourlyPQISheetUnverified();
        if (c.getCount() > 0) {
            ArrayList<HourlyPQISheet> hourlyPQISheets = new ArrayList<>();
            while (c.moveToNext()) {
                HourlyPQISheet hourlyPQISheet = new HourlyPQISheet(
                  c.getInt(c.getColumnIndex("id")),
                        c.getString(c.getColumnIndex("date")),
                        c.getString(c.getColumnIndex("time")),
                        c.getString(c.getColumnIndex("shift")),
                        c.getString(c.getColumnIndex("pkd_time")),
                        c.getString(c.getColumnIndex("machine_no")),
                        c.getString(c.getColumnIndex("sku")),
                        c.getString(c.getColumnIndex("coding")),
                        c.getString(c.getColumnIndex("loose_pkd")),
                        c.getString(c.getColumnIndex("wrinkles")),
                        c.getString(c.getColumnIndex("stack")),
                        c.getString(c.getColumnIndex("diameter1")),
                        c.getString(c.getColumnIndex("diameter2")),
                        c.getString(c.getColumnIndex("sealing_quality")),
                        c.getString(c.getColumnIndex("broken_chipped")),
                        c.getString(c.getColumnIndex("color")),
                        c.getString(c.getColumnIndex("aroma")),
                        c.getString(c.getColumnIndex("foreign_matter")),
                        c.getString(c.getColumnIndex("other_defects")),
                        c.getString(c.getColumnIndex("remarks")),
                        c.getInt(c.getColumnIndex("user_id")),
                        c.getString(c.getColumnIndex("username")),
                        c.getInt(c.getColumnIndex("verify_status")),
                        c.getInt(c.getColumnIndex("verify_id"))
                );
                hourlyPQISheets.add(hourlyPQISheet);
            }
            HourlyPQISheetVerficationAdapter adapter = new HourlyPQISheetVerficationAdapter(this, hourlyPQISheets, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);
            DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
            recyclerView.addItemDecoration(itemDecor);
        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void insertToArray(int id) {
        hourlyPqiIds.add(id);
    }

    @Override
    public void removeFromArray(int id) {
        for (int i = 0; i < hourlyPqiIds.size(); i++) {
            if (hourlyPqiIds.get(i) == id) {
                hourlyPqiIds.remove(i);
                break;
            }
        }
    }


    @OnClick(R.id.btn_verify) void onVerificationClicked() {
        if (hourlyPqiIds.size() > 0 ) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Are you sure !");
            builder.setMessage("You are about to verify " + hourlyPqiIds.size() + " records. Do you really want to proceed ?");
            builder.setCancelable(false);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    androidApplication = (AndroidApplication) getApplicationContext();
                    User user = androidApplication.getUser(getApplicationContext());
                    for (int  i = 0; i < hourlyPqiIds.size(); i++) {
                        dbHelper.updateHourlyPQISheetVerification(hourlyPqiIds.get(i), user.id);
                    }
                    hourlyPqiIds.clear();
                    setRecyclerView();
                    Toasty.success(getApplicationContext(), "Successfully verified", Toast.LENGTH_SHORT, true).show();
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            builder.show();
        } else {
            Toasty.warning(this, "No data selected", Toast.LENGTH_SHORT, true).show();
        }
    }

    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy_MM_dd_hh_mm_a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }


}
