package n.webinfotech.itcreports.activities;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;

public class LoginActivity extends AppCompatActivity {

    DBHelper dbHelper;
    @BindView(R.id.edit_text_username)
    EditText editTextUsername;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initializeDBHelper();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_login) void onLoginClicked() {

        if (editTextUsername.getText().toString().isEmpty() || editTextPassword.getText().toString().isEmpty()) {
            Toasty.error(this, "Fields missing", Toast.LENGTH_SHORT).show();
        } else {
            Cursor c = dbHelper.checkLogin(editTextUsername.getText().toString(), editTextPassword.getText().toString());
            if (c != null && c.getCount() > 0) {
                Toasty.success(this, "Login Successful", Toast.LENGTH_LONG).show();
                c.moveToFirst();
                int userId = c.getInt(c.getColumnIndex("id"));
                int userType = c.getInt(c.getColumnIndex("user_type"));
                String userName = c.getString(c.getColumnIndex("user_name"));
                String signature = c.getString(c.getColumnIndex("sign"));
                User user = new User(userId, userType, userName, signature);
                androidApplication = (AndroidApplication) getApplicationContext();
                androidApplication.setUser(this, user);
                Log.e("LogMsg", "User Id:" + userId);
                if (userType == 1) {
                    Intent adminIntent = new Intent(this, UserListActivity.class);
                    startActivity(adminIntent);
                } else if (userType == 2) {
                    Intent userIntent = new Intent(this, UserActivity.class);
                    startActivity(userIntent);
                } else {
                    Intent verifierIntent = new Intent(this, VerifierActivity.class);
                    startActivity(verifierIntent);
                }
                finish();
            } else {
                Toasty.error(this, "Login Failed", Toast.LENGTH_LONG).show();
            }
            if (!c.isClosed()) {
                c.close();
            }
        }
//        while (c.moveToNext()) {
//            Toast.makeText(this, "Name: " + c.getString(c.getColumnIndex("name")), Toast.LENGTH_LONG).show();
//        }
    }

}
