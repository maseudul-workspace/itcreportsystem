package n.webinfotech.itcreports.activities;

import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import n.webinfotech.itcreports.AndroidApplication;
import n.webinfotech.itcreports.R;
import n.webinfotech.itcreports.adapters.HourlyPQISheetVerficationAdapter;
import n.webinfotech.itcreports.adapters.OnlineSensoryVerifyAdapter;
import n.webinfotech.itcreports.dialogs.VerificationDialog;
import n.webinfotech.itcreports.models.HourlyPQISheet;
import n.webinfotech.itcreports.models.OnlineSensory;
import n.webinfotech.itcreports.models.User;
import n.webinfotech.itcreports.util.DBHelper;
import n.webinfotech.itcreports.util.SaveSignatureHelper;

import static android.graphics.drawable.ClipDrawable.HORIZONTAL;

public class OnlineSensoryVerificationActivity extends AppCompatActivity implements OnlineSensoryVerifyAdapter.Callback {

    DBHelper dbHelper;
    @BindView(R.id.recycler_view_online_sensory_verification)
    RecyclerView recyclerView;
    ArrayList<Integer> onlineSenIds;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_sensory_verification);
        ButterKnife.bind(this);
        onlineSenIds = new ArrayList<>();
        initializeDBHelper();
        setRecyclerView();
    }

    public void initializeDBHelper() {
        dbHelper = new DBHelper(this);
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void setRecyclerView() {
        Cursor c = dbHelper.getOnlineSensoryUnverified();
        if (c.getCount() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            ArrayList<OnlineSensory> onlineSensories = new ArrayList<>();
            while (c.moveToNext()) {
                OnlineSensory onlineSensory = new OnlineSensory(
                        c.getInt(c.getColumnIndex("id")),
                        c.getString(c.getColumnIndex("date")),
                        c.getString(c.getColumnIndex("time")),
                        c.getString(c.getColumnIndex("shift")),
                        c.getString(c.getColumnIndex("machine_no")),
                        c.getString(c.getColumnIndex("sku")),
                        c.getString(c.getColumnIndex("p1_sign")),
                        c.getString(c.getColumnIndex("p1_name")),
                        c.getString(c.getColumnIndex("p2_sign")),
                        c.getString(c.getColumnIndex("p2_name")),
                        c.getString(c.getColumnIndex("p3_sign")),
                        c.getString(c.getColumnIndex("p3_name")),
                        dbHelper.getOnlineSensoryBatchesData(c.getInt(c.getColumnIndex("id")), 1),
                        dbHelper.getOnlineSensoryBatchesData(c.getInt(c.getColumnIndex("id")), 2),
                        dbHelper.getOnlineSensoryBatchesData(c.getInt(c.getColumnIndex("id")), 3),
                        c.getInt(c.getColumnIndex("verify_status")),
                        c.getInt(c.getColumnIndex("verify_id"))
                );
                onlineSensories.add(onlineSensory);
            }
            OnlineSensoryVerifyAdapter adapter = new OnlineSensoryVerifyAdapter(this, this, onlineSensories, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);
            DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
            recyclerView.addItemDecoration(itemDecor);
        } else {
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void insertToArray(int id) {
        onlineSenIds.add(id);
    }

    @Override
    public void removeFromArray(int id) {
        for (int i = 0; i < onlineSenIds.size(); i++) {
            if (onlineSenIds.get(i) == id) {
                onlineSenIds.remove(i);
                break;
            }
        }
    }

    @OnClick(R.id.btn_verify) void onVerificationClicked() {
        if (onlineSenIds.size() > 0 ) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Are you sure !");
            builder.setMessage("You are about to verify " + onlineSenIds.size() + " records. Do you really want to proceed ?");
            builder.setCancelable(false);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                androidApplication = (AndroidApplication) getApplicationContext();
                User user = androidApplication.getUser(getApplicationContext());
                for (int  i = 0; i < onlineSenIds.size(); i++) {
                    dbHelper.updateOnlineSansoryVerification(onlineSenIds.get(i), user.id);
                }
                onlineSenIds.clear();
                setRecyclerView();
                Toasty.success(getApplicationContext(), "Successfully verified", Toast.LENGTH_SHORT, true).show();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            builder.show();
        } else {
            Toasty.warning(this, "No data selected", Toast.LENGTH_SHORT, true).show();
        }
    }


    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy_MM_dd_hh_mm_a", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
}
